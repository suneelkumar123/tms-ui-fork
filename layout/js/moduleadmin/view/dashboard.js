(function (){
    var mid;
    window.dash_board = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    dash_board.Views.header = Backbone.View.extend({
        
        initialize: function () {
            _.bindAll(this,'render');
        },
        
        render: function (modulid) {
            mid = modulid;
            this.$el.empty().append(
            '<div class="row">'+
                '<div class="col-lg-12">'+
                    '<h1 class="page-header"> Dashboard </h1>'+
                '</div>'+
                '<!-- /.col-lg-12 -->'+
            '</div>');
            
        },
    });
})();