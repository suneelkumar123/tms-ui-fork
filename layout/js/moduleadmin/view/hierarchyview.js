(function () {
    

    var empId,empid;
    /* GLOBAL CALLING ALL BACKBONE MVC */
    window.Heirarchy_view = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };


    /*Creating view for LATM view */


      Heirarchy_view.Views.header = Backbone.View.extend({
        
        events: {
             'click .btndetails': 'sendLATMEmpCodeForFutureUse',

        },
     
        
        initialize: function () {
            _.bindAll(this, 'render');
        },
        
        render: function (id,modulid) {
            mid = modulid;
            empId=id;
            empid=localStorage.getItem("empId");
            $("#sidebar").empty().append(
                 '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="training_done" class="maind" data-toggle="tooltip tab" data-original-title="Training Details">&nbsp;&nbsp;<i class="fa fa-list-ol fa-fw"></i>' +
                                '<span class="content-hide">Dashboard</span>' +
                            '</li>' +
                            '<li id="trainerbatchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="attend_manage" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="manage_user" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                            '<span class="content-hide">User Management</span>' +
                        '</li>' +
                        '<li id="hierarchyview" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Hierarchy View">&nbsp;&nbsp;<i class="fa fa-camera-retro fa-fw"></i>' +
                            '<span class="content-hide">Hierarchy View</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            		
                 /*Making view for LATM */
               /* '<div class="row">' +
	                '<div class="col-xs-12">' +
	                    '<h1 class="page-header">Summary Of  Batches <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
	                '</div>' +
	                '<!-- /.col-xs-12 -->' +
                '</div>' +*/
            /*'<div id="batch_wrapper">' +
            		'<div class="col-xs-9">' +
                        '<span> <a href="excelreportforhier?empId='+empid+'" target="_blank"> Click Here</a> To download  batch summary report</span>'+
                    '</div>'+
            '</div>' +*/
            '<div class="row">' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="latmbatch" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<td><b>Employee Code</b></td>' +
                                                '<td><b>Name </b></td>' +
                                                '<td><b>Branch Code</b></td>' +
                                                '<td><b>Branch Name</b></td>' +
                                                '<td><b>No. of Batches</b></td>' +
                                                '<td><b>No. of Students</b></td>' +
                                                '<td><b>Candidates Appeared</b></td>'+
                                                '<td><b>Candidates Passed</b></td>'+
                                                '<td><b>Candidates Failed</b></td>' +
                                                '<td><b>Candidates Pending</b></td>'+
                                                '<td><b>Passing%</b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'  
    );
            /*if(localStorage.getItem("addStudentCheckValue") == 1){
        		$("#manage_user").show();
        	}*/
    this.sendBatchNameForFurtureUse();
   
     },
      createLatmTable: function (empId) {
//            selected=[];
    	  var parentlat = $("#latmbatch").parent().parent().parent().parent();
    	  $("#latmbatch").parent().parent().parent().remove();
    	  parentlat.append(
    			  	'<div class="row">' +
		                '<div class="col-xs-12">' +
		                    '<h1 class="page-header">Summary Of  Batches <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
		                '</div>' +
		                '<!-- /.col-xs-12 -->' +
	                '</div>' +
    			  '<div id="batch_wrapper">' +
          				'<div class="col-xs-9">' +
          					'<span> <a href="excelreportforhier?empId='+empid+'" target="_blank"> Click Here</a> To download  batch summary report</span>'+
          				'</div>'+
          			'</div>' +
    			  '<table id="latmbatch" class="table table-hover">' +
                  '<thead>' +
                  '<tr>' +
                      '<td><b>Employee Code</b></td>' +
                      '<td><b>Name </b></td>' +
                      '<td><b>Branch Code</b></td>' +
                      '<td><b>Branch Name</b></td>' +
                      '<td><b>No. of Batches</b></td>' +
                      '<td><b>No. of Students</b></td>' +
                      '<td><b>Candidates Appeared</b></td>'+
                      '<td><b>Candidates Passed</b></td>'+
                      '<td><b>Candidates Failed</b></td>' +
                      '<td><b>Candidates Pending</b></td>'+
                      '<td><b>Passing%</b></td>' +
                  '</tr>' +
              '</thead>' +
              '<tbody>' +
              '</tbody>' +
          '</table>');
            jQuery("#latmbatch").dataTable({ 
            	 "ajax": {
                     "url": "listhierarchyviewde?empId="+empId,
                     "dataSrc": "",
                     destroy: true, 
                 },
            
             "columnDefs": [{
                     'targets': 0,
                    'searchable':false,
                     'render': function (data, type, full, meta){
                    	 empid=full.empId;
                         return '<button class="btndetails" ><a>'+full.empId+'</a></button>';
                     }
                }
              ],
            "columns" : [
                           { "data": " "},
                           { "data": "name"},
                           { "data": "branchCode" },
                           { "data": "branchName" },
                           { "data": "noOfBatches" },                       
                           { "data": "noOfStudent" },
                           { "data": "candidatesAppeared"},
                           { "data" :"candidatesPassed"},
                           { "data" :"candidatesFailed"},
                           { "data" :"candidatesPending"},
                           { "data" :"passingPercentage"}

            ],
             'fnCreatedRow': function (nRow, data, iDataIndex) {
                     $(nRow).attr('id', 'batch' + data.empId); // or whatever you choose to set as the id
                 },
            });
        },

    sendBatchNameForFurtureUse: function(){
            $(".errorClss").hide();
            $("#pageloading").show();
            Backbone.ajax({
                url: "getdesig?empId=" +0,
                data: "",
                type: "GET",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data.managerId == 1){
                   heirarchyindex.createLatmTable("0");
                  /* landingroutes.navigate('batch/'+empId+'/nh/'+mid+'/0', {trigger: true});*/
                    }

                   /*else if((data=="latm")||(data== "LATM")){
                      landingroutes.navigate('batch/'+empId+'/latmtobtm/'+mid+'/0', {trigger: true});

                    } else if((data=="atm")||(data == "ATM")){
                        landingroutes.navigate('batch/'+empId+'/btmbatchview/'+mid+'/0', {trigger: true});
                    }*/else if(data.managerId==0){
                         landingroutes.navigate('hier/'+empId+'/batchlist/'+mid+'/0', {trigger: true});
                    }
                     else {
                        ("Please contact IT Admin");
                    }
                    
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function(data) {
                    $("#pageloading").hide();
                }
            });
        },

     sendLATMEmpCodeForFutureUse: function(e){
            $(".errorClss").hide();
            $("#pageloading").show();
             var empId = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children()[0]).children().children("a").text();
            Backbone.ajax({
                dataType: "json",
                url: "getdesig?empId="+empId,
                data: "",
                type: "GET",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    if(data.managerId == 0) { landingroutes.navigate('hier/'+empId+'/batchlist/'+mid+'/0', {trigger: true});
                    } else if(data.managerId == 1) {
                       
                    	 heirarchyindex.createLatmTable(empId);
                    } else {
                        ("Please contact IT Admin");
                    }
                    
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },


       
      });
    /*CREATING VIEW FOR HIERARCHY MANAGEMENT FOR ATM */
    /*  Heirarchy_view.Views.atmlist = Backbone.View.extend({
        
        events: {
             
             'click .btmlist': 'sendATMEmpCodeForFutureUse'
        },
     
        
        initialize: function () {
            _.bindAll(this, 'render');
        },
        
        render: function (modulid) {
            mid = modulid;
          //  baid = b;
            $("#sidebar").empty().append(
                 '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="training_done" class="maind" data-toggle="tooltip tab" data-original-title="Training Details">&nbsp;&nbsp;<i class="fa fa-list-ol fa-fw"></i>' +
                                '<span class="content-hide">Dashboard</span>' +
                            '</li>' +
                            '<li id="trainerbatchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="attend_manage" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="manage_user" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                            '<span class="content-hide">User Management</span>' +
                        '</li>' +
                          '<li id="hierarchyview" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Hierarchy View">&nbsp;&nbsp;<i class="fa fa-camera-retro fa-fw"></i>' +
                            '<span class="content-hide">Hierarchy View</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(

                *//*** Creating division for the ATM summary report*****//*
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header">Summary Of ATM Batches <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div id="batch_wrapper">' +
            '<div class="col-xs-9">' +
                        '<span> <a href="excelreportforatm?empId='+localStorage.getItem("empId")+'" target="_blank"> Click Here</a> To download ATM batch summary report</span>'+
                    '</div>'+
            '</div>' +
            '<div class="row">' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="batchtodisplay" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                
                                                '<td><b>Employee Code</b></td>' +
                                                '<td><b>ATM Name </b></td>' +
                                                '<td><b>Branch Code</b></td>' +
                                                '<td><b>Branch Name</b></td>' +
                                                '<td><b>No. of Batches</b></td>' +
                                                '<td><b>No. of Students</b></td>' +
                                                '<td><b>Candidates Appeared</b></td>'+
                                                '<td><b>Candidates Passed</b></td>'+
                                                '<td><b>Candidates Failed</b></td>' +
                                                '<td><b>Candidates Pending</b></td>'+
                                                '<td><b>Passing%</b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'  
    );
            if(localStorage.getItem("addStudentCheckValue") == 1){
        		$("#manage_user").show();
        	}
            this.CreatebatchTable();
        },
        
       
        
    Fetch The batch From The Table
        CreatebatchTable: function (e) {
            selected=[];
            jQuery("#batchtodisplay").dataTable({
            "ajax": {
                      "url": "atmhierarchyview?empId="+localStorage.getItem("empId"),
                      "dataSrc": ""
                  },

             "columnDefs": [{
                     'targets': 0,
                    'searchable':false,
                     'render': function (data, type, full, meta){
                         return '<button class="btmlist"><a>'+full.empId+'</a></button>';
                     }
                }
              ],
            "columns" : [
                           { "data": " "},
                           { "data": "atmName"},
                           { "data": "branchCode" },
                           { "data": "branchName" },
                           { "data": "noOfBatches" },                       
                           { "data": "noOfStudent" },
                           { "data": "candidatesAppeared"},
                           { "data" :"candidatesPassed"},
                           { "data" :"candidatesFailed"},
                           { "data" :"candidatesPending"},
                           { "data" :"passingPercentage"}

            ],
//             'fnCreatedRow': function (nRow, data, iDataIndex) {
//                     $(nRow).attr('id', 'batch' + data.empId); // or whatever you choose to set as the id
//                 },
            });
        },
 
        sendATMEmpCodeForFutureUse: function(e){
            $(".errorClss").hide();
             $("#pageloading").show();
             var empId = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children()[0]).children().children("a").text();
                Backbone.ajax({
                dataType: "json",
                url: "btmhierarchymid?empId="+localStorage.getItem("empId"),
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0) {
                        $(".errorBatchName").show();
                    } else if(data == 1) {
                        landingroutes.navigate('batch/'+empId+'/btmbatchview/'+mid+'/0', {trigger: true});
                    } else {
                        ("Please contact IT Admin");
                    }
                    
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },


 
  });


*//***Heirarchy Of btmlist views*//*
   Heirarchy_view.Views.btmlist =Backbone.View.extend({

      events:{

           'click .batchlilst': 'sendBTMEmpCodeForFutureUse'
      },

       initialize: function () {
            _.bindAll(this, 'render');
        },

         render: function (modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                 '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="training_done" class="maind" data-toggle="tooltip tab" data-original-title="Training Details">&nbsp;&nbsp;<i class="fa fa-list-ol fa-fw"></i>' +
                                '<span class="content-hide">Dashboard</span>' +
                            '</li>' +
                            '<li id="trainerbatchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="attend_manage" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="manage_user" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                            '<span class="content-hide">User Management</span>' +
                        '</li>' +
                        '<li id="hierarchyview" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Hierarchy View">&nbsp;&nbsp;<i class="fa fa-camera-retro fa-fw"></i>' +
                            '<span class="content-hide">Hierarchy View</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
               *//***** Appending the BTM VIEW *//*
                 '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header">Summary Of BTM  <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div id="batch_wrapper">' +
            '<div class="col-xs-9">' +
                        '<span> <a href="excelreportforbtm?empId='+localStorage.getItem("empId")+'" target="_blank"> Click Here</a> To download BTM batch summary report</span>'+
                    '</div>'+
            '</div>' +
            '<div class="row">' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="batch" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                
                                                '<td><b>Employee Code</b></td>' +
                                                '<td><b>BTM Name </b></td>' +
                                                '<td><b>Branch Code</b></td>' +
                                                '<td><b>Branch Name</b></td>' +
                                                '<td><b>No.of Batches</b></td>' +
                                                '<td><b>No.of Students</b></td>' +
                                                '<td><b>Candidates Appeared</b></td>'+
                                                '<td><b>Candidates Passed</b></td>'+
                                                '<td><b>Candidates Failed</b></td>' +
                                                '<td><b>Candidates Pending</b></td>'+
                                                '<td><b>Passing %</b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'  
             
        );
            if(localStorage.getItem("addStudentCheckValue") == 1){
        		$("#manage_user").show();
        	}
       this.createBTMtable();
       
     },
          batchHeading: function() {
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getvariable?key=empId",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data){
                    $(".bat_Detail").text(data);
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
           // this.createBTMtable();
        },

      

        createBTMtable : function (e) {
          //  selected=[];
            jQuery("#batch").dataTable({
            	   "ajax": {
                       "url": "btmhierarchyview?empId="+localStorage.getItem("empId"),
                       "dataSrc": ""
                   },

             "columnDefs": [{
                     'targets': 0,
                    'searchable':false,
                     'render': function (data, type, full, meta){
                         return '<button class="batchlilst"><a>'+full.empId+'</a></button>';
                     }
                },
              ],
            "columns" : [
                           { "data": " "},
                           { "data": "btmName"},
                           { "data": "branchCode" },
                           { "data": "branchName" },
                           { "data": "noOfBatches" },                       
                           { "data": "noOfStudent" },
                           { "data": "candidatesAppeared"},
                           { "data" :"candidatesPassed"},
                           { "data" :"candidatesFailed"},
                           { "data" :"candidatesPending"},
                           { "data" :"passingPercentage"}
            ],
             'fnCreatedRow': function (nRow, data, iDataIndex) {
                     $(nRow).attr('id', 'batch' + data.empId); // or whatever you choose to set as the id
                 },
            });

      },


 sendBTMEmpCodeForFutureUse: function(e){
            $(".errorClss").hide();
            $("#pageloading").show();
            var empId = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children()[0]).children().children("a").text();
                Backbone.ajax({
                dataType: "json",
                url: "hierarchymid?empId="+localStorage.getItem("empId"),
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0) {
                        $(".errorBatchName").show();
                    } else if(data == 1) {
                        landingroutes.navigate('batch/'+empId+'/batchlist/'+mid+'/0', {trigger: true});
                    } else {
                        ("Please contact IT Admin");
                    }
                    
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },

    });*/






Heirarchy_view.Views.batchlist =Backbone.View.extend({

      events:{

      },

       initialize: function () {
            _.bindAll(this, 'render');
        },

         render: function (eid) {
            empId = eid;
            $("#sidebar").empty().append(
                 '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="training_done" class="maind" data-toggle="tooltip tab" data-original-title="Training Details">&nbsp;&nbsp;<i class="fa fa-list-ol fa-fw"></i>' +
                                '<span class="content-hide">Dashboard</span>' +
                            '</li>' +
                            '<li id="trainerbatchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="attend_manage" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="manage_user" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                            '<span class="content-hide">User Management</span>' +
                        '</li>' +
                        '<li id="hierarchyview" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Hierarchy View">&nbsp;&nbsp;<i class="fa fa-camera-retro fa-fw"></i>' +
                            '<span class="content-hide">Hierarchy View</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
               /***** Appending the BTM VIEW */
                 '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header"> List of Batches <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div id="batch_wrapper">' +
            '<div class="col-xs-9">' +
                        '<span> <a href="excelreportforbatch?empId='+empId+'" target="_blank"> Click Here</a> To download batchlist summary report</span>'+
                    '</div>'+
            '</div>' +
            '<div class="row">' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="batch" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<td><b>Batch Name </b></td>' +
                                                '<td><b>StartDate</b></td>' +
                                                '<td><b>EndDate</b></td>' +
                                                '<td><b>Lunch Expenses</b></td>' +
                                                '<td><b>Miscallaneous Expenses</b></td>' +
                                                '<td><b>Travelling Expenses</b></td>' +
                                                '<td><b>No of Students</b></td>' +
                                                '<td><b>Candidates Appeared</b></td>'+
                                                '<td><b>Candidates Passed</b></td>'+
                                                '<td><b>Candidates Failed</b></td>' +
                                                '<td><b>Candidates Pending</b></td>'+
                                                '<td><b>Passing %</b></td>' +
                                                '<td><b>Batch report</td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'  
             
        );
           /* if(localStorage.getItem("addStudentCheckValue") == 1){
        		$("#manage_user").show();
        	}*/
       this.createBTMtable(empId);
       
     },
/*          batchHeading: function() {
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getvariable?key=empId",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data){
                    $(".bat_Detail").text(data);
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
           // this.createBTMtable();
        },
*/
      

        createBTMtable : function (empId) {
          //  selected=[];
            jQuery("#batch").dataTable({
            
            "ajax": {
                "url": "getbtmbatchdetails?empId="+empId,
                "dataSrc": ""
            },

             "columnDefs": [ {
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<a href="excelreportbatchwise?batchId='+full.batchId+'" target="_blank"> Download</a>';
                    }
                },
              ],
            "columns" : [
                           
                          { "data": "batchName" },
                           { "data": "batchStartDate"},
                           { "data": "batchEndDate"},
                           { "data" : "lunchExpenses"},
                           { "data" : "miselExpense"},
                           { "data" : "travellingExpense"},                 
                           { "data": "numStudent" },
                           { "data": "candidateAppeared"},
                           { "data" :"candidatePassed"},
                           { "data" :"candidateFailed"},
                           { "data" :"candidatePending"},
                           { "data" :"passingPercentage"},
                           { "data" : " "}
                           
            ],
             'fnCreatedRow': function (nRow, data, iDataIndex) {
                     $(nRow).attr('id', 'batch' + data.empId); // or whatever you choose to set as the id
                 },
            });

      },


    });





 })();
        

    
        
    
    
