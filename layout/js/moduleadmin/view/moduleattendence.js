(function (){
    var mid, batchAttenElement;
    var checkjson = [];
    var selected = [];
    var selectedatt = [];
    var selectedremmark = [];
    
    var dateForBatch = [];
    var batchStudentCount;
   // var selected=[];
    window.admin_attendmark = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    admin_attendmark.Views.header = Backbone.View.extend({
        
        
        events:{
            'click .submit_Attedence': 'submitAttedanceMarked',
            'click .attCls': 'atteMarkToJson',
            'focusout .attRemCls': 'atteRemarkToJson',
//            'mouseover #display_batch':'mouseoverInBatchAttedance',
//            'mouseover #display_slot': 'mouseoverInSlotAttedance',
            //'mouseout #display_batch': 'mouseoutInBatchAttedance',
            //'mouseout #display_slot': 'mouseoutInSlotAttedance',
            'click .atteBatchCls': 'changeInBatchAttedance',
            'click .atteSlotCls': 'changeInSlotAttedance',
            'click .atteTrainerCls': 'changeInTrainerAttedance',
//            'mouseover #select_batch': 'selectInBatchAttedance',
//            'mouseout #select_batch': 'mouseoutInBatchAttedance',
            'click #session_no': 'selectInSlotAttedance',
            'click #trainer-name': 'selectInTrainerAttedance',
//            'mouseover #session_no': 'mouseOverInSlotAttedance',
//            'mouseout #session_no': 'mouseoutInSlotAttedance',
            'change .attendancefile': 'attedanceListUpload',
            'mousedown .attendancefile': 'beforeAttedUpload',
            'click #select_date': 'selectDateValidation',
            'click .dwbthstud': 'attendancefiledownloaddata',
            'keyup #select_batch':'filterFunctionType'
        },
        
        initialize: function () {
            _.bindAll(this,'render');
        },
        
        
        render: function(modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="config_module" class="maind" data-toggle="tooltip tab" data-original-title="Module Configuration">&nbsp;&nbsp;<i class="fa fa-file-excel-o fa-fw"></i>' +
                                '<span class="content-hide">Configuration</span>' +
                            '</li>' +
                            '<li id="user_manage" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                                '<span class="content-hide">User Management</span>' +
                            '</li>' +
                            '<li id="batchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="ques_master" class="maind" data-toggle="tooltip tab" data-original-title="Question Bank">&nbsp;&nbsp;<i class="fa fa-pencil fa-fw"></i>' +
                                '<span class="content-hide">Question Bank</span>' +
                            '</li>' +
                            '<li id="admin_attedance" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="paper_create" class="maind" data-toggle="tooltip tab" data-original-title="Test Management">&nbsp;&nbsp;<i class="fa fa-sitemap fa-fw"></i>' +
                                '<span class="content-hide">Test Management</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header"> Attendance Management <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div class="row">' +
                '<div class="col-xs-3">' +
                    '<label>Select batch</label>' +
                '</div>' +
                '<div class="col-xs-2">' +
                    '<label>Select date</label>' +
                '</div>' +
                '<div class="col-xs-2">' +
                    '<label>Select slot</label>' +
                '</div>' +
                '<div class="col-xs-2">' +
                    '<label>Select Trainer</label>' +
                '</div>' +
                '<div class="col-xs-1">' +
                    '<form method="post" enctype="multipart/form-data">'+
                        '<div class="input-group-btn">'+
                            '<div class="btn btn-outline btn-primary btn-file" style="cursor:pointer; border-radius: 0;" tabindex="2">'+
                                '<i class="glyphicon glyphicon-folder-open">'+
                                '</i> &nbsp;Upload  <input name="filename" type="file" class="attendancefile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px;"> '+
                            '</div>'+
                        '</div>'+
                    '</form>'+
                '</div>'+
            '</div>' +
            '<div class="row">' +
                '<div class="col-xs-3" id="mydropdown">' +
                    '<input id="select_batch" type="text" name="select-batches" class="dropdown-toggle form-control" placeholder="Select Batch"  data-toggle="dropdown" tabindex="1">' +
                    '<ul class="dropdown-menu drop-menu" id="display_batch" aria-labelledby="select_batch" style="text-align:justify;"></ul>' +
                '</div>' +
                '<div class="col-xs-2">' +
                    '<div id="attendance_date">' +
                        '<input id="select_date" type="text" name="select-date" class="form-control" placeholder="Select Date" readonly tabindex="1">' +
                    '</div>' +
                '</div>' +
                '<div class="col-xs-2">' +
                    '<input id="session_no" type="text" name="session-no" class="dropdown-toggle form-control" placeholder="Select Slot" data-toggle="dropdown" readonly tabindex="1">' +
                    '<ul class="dropdown-menu drop-menu" id="display_slot" aria-labelledby="session_no" style="text-align:justify;"></ul>' +
                '</div>' +
                '<div class="col-xs-2">' +
                    '<input id="trainer-name" type="text" name="trainer-name" class="dropdown-toggle form-control" placeholder="Select Trainer" data-toggle="dropdown" readonly tabindex="1">' +
                    '<ul class="dropdown-menu drop-menu" id="display_trainers" aria-labelledby="trainer-name" style="text-align:justify;"></ul>' +
                '</div>' +
                '<div class="col-xs-2" style="margin-top: 1%;">' +
                        '<span><a tabindex="1" target="_blank" class="dwbthstud">Download</a> template</span>' +
                '</div>' +
            '</div>' +
            '<div id="mark_attedance" class="row">' +
            '</div>'
            );
            $("#mark_attedance").hide();
            this.attendanceBatchCreate();
        },
        
        mouseoverInBatchAttedance: function(){
            $("#display_batch").show();
        },

        mouseoverInSlotAttedance: function(){
            $("#display_slot").show();
        },

        mouseoutInBatchAttedance: function(){
            $("#display_batch").hide();
        },

        mouseoutInSlotAttedance: function(){
            $("#display_slot").hide();
        },
        
        attendanceBatchCreate: function () {
            
            Backbone.$("#display_batch").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url:"listbatchbymid?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0) {
                        
                    } else {
                        for (var i=0; i< data.length; i++){
                        	Backbone.$("#display_batch").append(
                                '<li id="bth'+data[i].batchId+'" class="atteBatchCls"  tabindex="-1">'+data[i].shortDesc+'</li>'
                            );
 dateForBatch.push({"batchId":data[i].batchId,"startDate":data[i].validFrom,"endDate":data[i].validTo});
                        }
                        
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
         filterFunctionType: function() {
            var input, filter, ul, li, a ;
            input = document.getElementById("select_batch");
            filter = input.value.toUpperCase();
            div = document.getElementById("mydropdown");
            a = div.getElementsByTagName("li");
            for (var i = 0; i < a.length; i++) {
                if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
        },
        
        loadTrainersList: function(e) {
            var baid = $("#select_batch").attr('value');
            Backbone.$("#display_trainers").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url:"listtrainerbybatchid?batchId="+baid+"&moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0) {
                        
                    } else {
                        for (var i=0; i< data.length; i++){
                            Backbone.$("#display_trainers").append(
                                '<li id="trin'+data[i].uniId+'" class="atteTrainerCls"  tabindex="-1">'+data[i].name+'</li>'
                            );
                        }
                        
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        loadSlotToCreate: function () {
            
            Backbone.$("#display_slot").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getslotlist?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0) {
                        
                    } else {
                        for (var i=0; i< data.length; i++){
                            Backbone.$("#display_slot").append(
                                '<li id="slot'+data[i].uniId+'" class="atteSlotCls"  tabindex="-1">'+data[i].slotName+'</li>'
                            );
                        }
                        
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
        loadStudentsOfBatch: function(batid, sltid, trnid) {
            
            Backbone.$("#mark_attedance").empty().append('<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="marking_Attedance" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                /*'<td class="col-xs-1"><b>Sl No</b></td>' +
                                                */'<td class="col-xs-1"><b>URN Id</b></td>' +
                                                '<td class="col-xs-5"><b>Student Name</b></td>' +
                                                '<td class="col-xs-1"><b>Present</b></td>' +
                                                '<td class="col-xs-1"><b>Absent</b></td>' +
                                                '<td class="col-xs-3"><b>Remark</b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                        '<tfoot>' +
                                            '<tr>' +
                                                '<td colspan="5"> <div class="attDateError errorClss">Date should be with in valid date of batch</div><div class="attMarkError errorClss">Mark all the attendance before submit</div>' +
                                                '</td>' +
                                            '</tr>' +
                                        '</tfoot>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                            '<div class="panel-footer">' +
                                '<div class="row">' +
                                    '<div class="col-xs-10">' +
                                        '<div class="errorAttMarkSave errorClss">Failed to Save, Try Again..!</div>' +
                                    '</div>' +
                                    '<div class="col-xs-2">' +
                                        '<div class="btn btn-outline btn-primary submit_Attedence"> Save</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' );
            
            
            /*Backbone.$("#marking_Attedance").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"liststudentbybatchid?batchId="+batid+"&slotId="+sltid+"&trainerId="+trnid,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    batchStudentCount = data.length;
                    if(data == 0) {
                        $("#mark_attedance").show();
                        
                        Backbone.$("#marking_Attedance").children("tbody").append('<tr><td colspan="6">NO DATA AVAILABLE</td></tr>');
                    } else {
                        checkjson = [];
                        for (var i=0; i< data.length; i++){
                            checkjson.push({uniId:data[i].uniId,"status":"","remarks":""});
                            Backbone.$("#marking_Attedance").children("tbody").append(
                                '<tr id="std'+data[i].uniId+'">' +
                                    '<td>'+(i+1)+'</td>' +
                                    '<td>'+data[i].employeeId+'</td>' +
                                    '<td>'+data[i].name+'</td>' +
                                    '<td class="prCls">' +
                                        '<input type="radio" id="pre'+(i+1)+'" class="attCls" name="options'+i+'" value="1">' +
                                    '</td>' +
                                    '<td class="abCls">' +
                                        '<input type="radio" id="abs'+(i+1)+'" class="attCls" name="options'+i+'" value="2">' +
                                    '</td>' +
                                    '<td>' +
                                        '<input type="text" class="attRemCls" name="atted_remark'+i+'">' +
                                    '</td>' +
                                '</tr>'
                            );
                        }
                        $("#mark_attedance").show();
                        
                        adminattendancemarkindex.preLoadIfPresent(batid, sltid);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
            adminattendancemarkindex.preLoadIfPresent(batid, sltid);
        },
        
        preLoadIfPresent: function(batid, sltid){
            
            
        	
        	
        	var date = $("#select_date").val();
        	selected=[];
            selectedatt=[];
            selectedremmark=[];
            
        	var urls="getpaginstudentattedance?batchId="+batid+"&slotId="+sltid+"&date="+date;
        	
        	var studentTable;
       	 
        	studentTable = jQuery("#marking_Attedance").dataTable({
        	 
        	"iDisplayLength": 10,
        	"bProcessing" : true,
        	"bServerSide" : true,
        	"sAjaxSource" : urls,
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bSort" : false,
        	/*"sAjaxDataProp":"",*/
        	 "columnDefs": [ 
								{
								    'targets': -3,
								    'searchable':false,
								    'orderable':false,
								    'render': function (data, type, full, meta){
								        return '<input id="pre_'+full.uniId+'" type="radio" class="attCls" value="1" name="radio_'+full.uniId+'" > ';
								    }
								},
								{
								    'targets': -2,
								    'searchable':false,
								    'orderable':false,
								    'render': function (data, type, full, meta){
								        return '<input id="abs_'+full.uniId+'" type="radio" class="attCls" value="2" name="radio_'+full.uniId+'">';
								    }
								},
								{
								    'targets': -1,
								    'searchable':false,
								    'orderable':false,
								    'render': function (data, type, full, meta){
								        return '<input id="atted_remark_'+full.uniId+'"  name="atted_remark_'+full.uniId+'"  type="text" class="attRemCls" value="'+full.remarks+'">';
								    }
								},


        	 
        	 ],
        	 
        	"aoColumns" : [
        	               { "mData": "urnId" },
        	               { "mData": "studentName" },
        	               { "mData": "" },
        	               { "mData": "" },
        	               { "mData": "" }
        	],
        	'fnServerParams':function(aoData){
        		selected = [];
				selectedatt = [];
				selectedremmark = [];
        	},
        	'fnPreDrawCallback': function (oSettings) {
        		
				
        	},
            'fnCreatedRow': function (nRow, data, iDataIndex) {
				
                    $(nRow).attr('id', 'module' + data.uniId); // or whatever you choose to set as the id
                    if(data.status=="1"){
                    	$(nRow).children().children("#pre_"+data.uniId).prop("checked", true);
                    	selected.push(data.uniId);
                    	selectedatt.push(data.status);
                    	selectedremmark.push(data.remarks);
                    	
                    }else  if(data.status=="2"){
                    	$(nRow).children().children("#abs_"+data.uniId).prop("checked", true);
                    	selected.push(data.uniId);
                    	selectedatt.push(data.status);
                    	selectedremmark.push(data.remarks);
                    }
                    if(data.status!="")
                    	adminattendancemarkindex.attendentUpdateSave(data.uniId,data.status,data.remarks);
                },
                "oLanguage": {
                    "sProcessing": "DataTables is currently busy"
                },"drawCallback": function( oSettings ) {
    				
                	for (var int = 0; int < selected.length; int++) {
                		if(selectedatt[int]=="1"){
                			$("#pre_"+selected[int]).prop("checked", true);
                        	//$(nRow).children().children("#pre_"+data.uniId).prop("checked", true);
                        }else  if(selectedatt[int]=="2"){
                        	$("#abs_"+selected[int]).prop("checked", true);
                        	//$(nRow).children().children("#abs_"+data.uniId).prop("checked", true);
                        }
					}
                	
                	 $('#marking_Attedance tbody tr td').on('click', 'input[type="radio"]', function (e) {
                		 var dd=this.id.split("_");
                		 
                         var id = dd[1];
                         if(dd[0]=="pre"){
                        	 adminattendancemarkindex.attendentUpdateSave(id,"1",$("#atted_remark_"+id).val());
                         }else if(dd[0]=="abs"){
                        	 adminattendancemarkindex.attendentUpdateSave(id,"2",$("#atted_remark_"+id).val());
                         }
                	 });
                	 
                	 
                	 $('#marking_Attedance tbody tr td').on('change', 'input[type="text"]', function (e) {
                		 var dd=this.id.split("_");
                		 
                         var id = dd[2];
                         
                        	 adminattendancemarkindex.attendentUpdateSave(id,$("input[name='radio_"+id+"']:checked").val(),$("#atted_remark_"+id).val());
                        
                	 });
                	 
                	 
                	 
                }
        	});
        	
        	 jQuery("#mark_attedance").show();
        	 
        	 
        	 
        	 
        	 
        	// adminattendancemarkindex.attendentUpdateSave(data.uniId,data.remarks,data.status);
        	
            /*$("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"getstudentattedance?batchId="+batid+"&slotId="+sltid+"&date="+date,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    
                    for (var i=0; i< data.length; i++){
                        for( var j = 0; j < checkjson.length; j++) {
                            if(checkjson[j].uniId == data[i].uniId){
                                checkjson[j].status = data[i].status;
                                checkjson[j].remarks = data[i].remarks;
                            }
                        }
                        $("#std"+data[i].uniId+"").children().children('.attRemCls').val(data[i].remarks);
                        if( data[i].status == 1 ){
                           $("#std"+data[i].uniId+"").children(".prCls").children('.attCls').prop('checked', true);
                        } else if( data[i].status == 2 ){
                            $("#std"+data[i].uniId+"").children(".abCls").children('.attCls').prop('checked', true);
                        }
                    }
                    $("#marking_Attedance").dataTable({
                        "ordering": false
                    });
                    var reLoadVal = $($("#marking_Attedance").children("tbody").children("tr")[0]).prop('id').slice(3);
                    for( var x = 0; x < checkjson.length; x++) {
                            if(reLoadVal == checkjson[x].uniId){
                                if( checkjson[x].status == 1 ){
                                    $("#std"+reLoadVal+"").children(".prCls").children('.attCls').prop('checked', true);
                                } else if( checkjson[x].status == 2 ){
                                    $("#std"+reLoadVal+"").children(".abCls").children('.attCls').prop('checked', true);
                                } 
                            }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
        
        changeInBatchAttedance: function(e) {
            //$(".drop-menu").hide();
            var bhname = $("#"+e.currentTarget.id+"").text();
            var bhid = e.currentTarget.id.slice(3);
            for ( var i=0; i< dateForBatch.length; i++ ){
                if( dateForBatch[i].batchId == bhid){
                    $("#attendance_date").empty().append(
                        '<input id="select_date" type="text" name="select-date" class="form-control" placeholder="Select Date" readonly>'
                    );
                    var mida = dateForBatch[i].startDate;
                    var mxda = dateForBatch[i].endDate;
                    $("#select_date").val(mida);
                    $("#select_date").datepicker({
                        dateFormat: "yy-mm-dd",
                        minDate: mida,
                        maxDate: mxda
                    });
                }
            }
            $("#select_batch").attr('value', bhid);
            $("#select_batch").val(bhname);
            if($("#session_no").val() != 0){
                var slid =  $("#session_no").attr('value');
                if($("#trainer-name").val() != 0){
                    var trid = $("#trainer-name").attr('value');
                    adminattendancemarkindex.loadStudentsOfBatch(bhid, slid, trid);
                }
            }
        },
        
        changeInSlotAttedance: function(e){
            //$(".drop-menu").hide();
            var slname = $("#"+e.currentTarget.id+"").text();
            var slid = e.currentTarget.id.slice(4);
            $("#session_no").attr('value', slid);
            $("#session_no").val(slname);
            if($("#select_batch").val() != 0){
                var bhid = $("#select_batch").attr('value');
                if($("#trainer-name").val() != 0){
                    var trid = $("#trainer-name").attr('value');
                    adminattendancemarkindex.loadStudentsOfBatch(bhid, slid, trid);
                }
            }
        },
        
        changeInTrainerAttedance: function(e){
            //$(".drop-menu").hide();
            var trname = $("#"+e.currentTarget.id+"").text();
            var trid = e.currentTarget.id.slice(4);
            $("#trainer-name").attr('value', trid);
            $("#trainer-name").val(trname);
            if($("#select_batch").val() != 0){
                var bhid = $("#select_batch").attr('value');
                if($("#session_no").val() != 0){
                    var slid = $("#session_no").attr('value');
                    adminattendancemarkindex.loadStudentsOfBatch(bhid, slid, trid);
                }
            }
        },
        
        
        selectInBatchAttedance: function(e) {
            $(".drop-menu").hide();
            $("#display_batch").show();
        },
        
        selectInTrainerAttedance: function(e){
            $(".errorClss").hide();
            $(".errorAdmAttBat").remove();
            if( $("#select_batch").val() == 0 ){
                $(e.currentTarget.parentElement).append('<div class="errorClss errorAdmAttBat">Select Batch First</div>');
                $(".errorAdmAttBat").show();
                $("#display_trainers").val("");
            } else {
                adminattendancemarkindex.loadTrainersList();
            }
        },
        
        selectInSlotAttedance: function(e){
            $(".errorClss").hide();
            $(".errorAdmAttDat").remove();
            if($("#select_date").val() != 0){
                adminattendancemarkindex.loadSlotToCreate();
                //$(".drop-menu").hide();
                //$("#display_slot").show();
            } else{
                $(e.currentTarget.parentElement).append('<div class="errorClss errorAdmAttDat">Select Date First</div>');
                $(".errorAdmAttDat").show();
            }
        },
        
        mouseOverInSlotAttedance: function(e){
            if($("#select_date").val() != 0){
                $(".drop-menu").hide();
                $("#display_slot").show();
            }
        },
        
        atteMarkToJson: function(e) {
            var uid = e.currentTarget.parentElement.parentElement.id.slice(3);
            var attedancemark = $("#"+e.currentTarget.id+"").attr('value');
            for( var i = 0; i < checkjson.length; i++){
                if(checkjson[i].uniId == uid){
                    checkjson[i].status = attedancemark;
                }
            }
        },
        
        atteRemarkToJson: function(e){
            var uid = e.currentTarget.parentElement.parentElement.id.slice(3);
            var remark = $("#"+e.currentTarget.parentElement.parentElement.id+" > td:nth-child(6) > input").val();
            for( var i = 0; i < checkjson.length; i++){
                if(checkjson[i].uniId == uid){
                    checkjson[i].remarks = remark;
                }
            }
        },
        
        
        submitAttedanceMarked: function(){
            $(".errorClss").hide();
            var date = $("#select_date").val();
            var slid = $("#session_no").attr('value');
            var bhid = $("#select_batch").attr('value');
            var trid = $("#trainer-name").attr('value');
            var json = [];
            if(selectedatt.length!=$(".attRemCls").length){
            	
            	$(".attMarkError").show();
            	return;
            }
            
            for( var i = 0; i < selected.length; i++){
                if(selectedatt[i]== ""){
                    $(".attMarkError").show();
                    break;
                } else if( i < selected.length){
                   
                    json.push({"uniId":selected[i],"status":selectedatt[i],"remarks":selectedremmark[i]});
                }
            }  
            	$("#pageloading").show();
            	Backbone.ajax({
                url: "studentattedancesubmit?batchId="+bhid+"&slotId="+slid+"&date="+date+"&trainerId="+trid,
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    if(data == 1){
                        sweetAlert("Attendance is submitted");
                        //adminattendancemarkindex.loadStudentsOfBatch(bhid, slid, trid);
                    } else if(data == 2){
                        $(".errorAttMarkSave").show();
                    }
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
        complete: function() {
            $("#pageloading").hide();
        }
            });
        },
        
        beforeAttedUpload: function(e) {
            $(".errorClss").hide();
            if(($("#select_batch").val() == 0)){
                sweetAlert("Please select \"Batch\" before upload")
            }
            
            var dt = parseInt($("#select_date").val().replace(/\D+/g, ''));
            var bhid = $("#select_batch").attr('value');
            for( var i=0; i< dateForBatch.length; i++){
                if(dateForBatch[i].batchId == bhid){
                    var sd = parseInt(dateForBatch[i].startDate.replace(/\D+/g, ''));
                    var ed = parseInt(dateForBatch[i].endDate.replace(/\D+/g, ''));
                    if( !((sd <= dt) && (dt <= ed)) ){
                        $(".attDateError").show();
                    }
                }
            }
        },
        
        selectDateValidation: function(e) {
            $(".errorClss").hide();
            $(".attError").remove();
            var dt = parseInt($("#select_date").val().replace(/\D+/g, ''));
            var bhid = $("#select_batch").attr('value');
            if( $("#select_batch").val() == 0 ){
                $(e.currentTarget.parentElement).append('<div class="errorClss attError">Select Batch First</div>');
                $(".attError").show();
                $("#select_date").val("");
            }
        },
        
        
        attendancefiledownloaddata: function(e) {
        	
        	 var date = $("#select_date").val();
             var slid = $("#session_no").attr('value');
             var bhid = $("#select_batch").attr('value');
             var trid = $("#trainer-name").attr('value');
             if(slid==null)
            	 slid="";
             if(date==null)
            	 date="";
             if(trid==null)
            	 trid="";
             if(bhid==null)
            	 bhid="";
        	
        	var urld = "attedanceuploadtemplate?batchId="+bhid+"&slotId="+slid+"&date="+date+"&trainerId="+trid;
        	$(e.currentTarget).prop('href', urld);
        },
        attendentRemove:function(unid){
       	 var index = $.inArray(unid, selected);
            
            if ( index > -1 ) {
           	 selected.splice( index, 1 );
           	 selected.selectedatt( index, 1 );
           	 selected.selectedremmark( index, 1 );
            }
       },
       attendentUpdateSave:function(unid,status,remarks){
    	 var index = $.inArray(unid, selected);
         
         if ( index > -1 ) {
        	 selected.splice( index, 1 );
        	 selectedatt.splice( index, 1 );
        	 selectedremmark.splice( index, 1 );
         }
         if(status==null){
        	 status="";
         }
         selected.push(unid );
      	 selectedatt.push( status);
      	selectedremmark.push(remarks);
             
             
        },
        
        attedanceListUpload: function(e) {
            var slid = $("#session_no").attr('value');
            var bhid = $("#select_batch").attr('value');
            var date = $("#select_date").val();
            var trid = $("#trainer-name").attr('value');
            var urlv = "studentattedanceupload?"+csrf_url+"="+token+"&batchId="+bhid;
            $("#pageloading").show();
            $(e.currentTarget).parent().parent().parent().attr("action",urlv).ajaxForm({
                success : function(data) {
                    
                    var pf = $(".attendancefile").parent();
                    $(".attendancefile").remove();
                    $(pf).append('<input name="filename" type="file" class="attendancefile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px;">')
                    if(data=="1"){
                        sweetAlert("Attendance Uploaded Successfully");
                    } else if(data=="2"){
                        sweetAlert("Attendance Upload failed");
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    window.location.reload();
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
        },
    });
})();