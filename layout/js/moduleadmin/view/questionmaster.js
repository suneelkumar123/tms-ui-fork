(function (){
    
    var languageElement, topicElement, questionsl, sectionElement, mid,quid, dataTableInitializeFlag;
    window.question_master = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    question_master.Views.header = Backbone.View.extend({
        
        events:{
        	 'change #topic_select': 'topicSelctedForUpload',
             'change #language_select': 'languageSelecteForUpload',
             'change .questionfile': 'questionsListUpload',
             'mousedown .questionfile': 'beforeQuesUpload',
             'click #addNewLanguage': 'newLanguageForModule',
             'click .newlanguage_submit': 'submitNewLangForModule',
             'click .dwQueMas': 'beforeQuestionMasterDownload',
             'change #question_select' :'questionSelectForUpload',
             'change #language_select' :'changeToReset',
             'change #question_select' :'questionSelectForUpload',
             'mousedown .questionfile': 'disablepload',	 

        },
        
        initialize: function () {
            _.bindAll(this,'render');
            dataTableInitializeFlag = true;
        },
        
        render: function(modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="config_module" class="maind" data-toggle="tooltip tab" data-original-title="Module Configuration">&nbsp;&nbsp;<i class="fa fa-file-excel-o fa-fw"></i>' +
                                '<span class="content-hide">Configuration</span>' +
                            '</li>' +
                            '<li id="user_manage" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                                '<span class="content-hide">User Management</span>' +
                            '</li>' +
                            '<li id="batchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="ques_master" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Question Bank">&nbsp;&nbsp;<i class="fa fa-pencil fa-fw"></i>' +
                                '<span class="content-hide">Question Bank</span>' +
                            '</li>' +
                            '<li id="admin_attedance" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="paper_create" class="maind" data-toggle="tooltip tab" data-original-title="Test Management">&nbsp;&nbsp;<i class="fa fa-sitemap fa-fw"></i>' +
                                '<span class="content-hide">Test Management</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<h1 class="page-header"> Question Bank <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-2">' +
                        '<label>Select Language</label>' +
                    '</div>' +

                    '<div class="col-xs-2">' +
                         '<label> Select Question Bank Type</label>' +
                    '</div>'+
                    '<div class="col-xs-2">' +
                        '<label>Select Topic</label>' +
                    '</div>'+
                 /*   '<div class="col-xs-1">' +
                    '</div>'+*/
                    '<div class="col-xs-2">' +
                        '<label class="control-label"> Upload Question Bank </label>' +
                    '</div>'+
                    '<div class="col-xs-3">' +
                        '<form method="post" enctype="multipart/form-data">'+
                            '<div class="input-group-btn">'+
                                '<div class="btn btn-outline btn-primary btn-file" tabindex="1">'+
                                    '<i class="glyphicon glyphicon-folder-open">'+
                                    '</i> &nbsp;Browse  <input name="filename" type="file" class="questionfile"  id="questionuploadcheck" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px;" >'+
                                '</div>'+
                            '</div>'+
                        '</form>'+
                        '<span class="errorClss errorQueUploFailed"><br>Questions Uploading Failed. Try Again..! </span>' +
                    '</div>'+
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-2">' +
                        '<select id="language_select" type="text" name="language-select" class="form-control" placeholder="Select Language" readonly></select>' +
                   '</div>' +
                    //Creating the questions Sets for selecting
                    '<div class="col-xs-2">' +
                         '<select id="question_select" type="text" name="question-select" class="form-control" placeholder="Select Question" style="cursor: pointer;" readonly></select>' +
                         '<span class="errorClss errorQueSelectFailed"><br>Select Question Bank Set</span>' +
                    '</div>' +
                     /*
                    '<div class="col-xs-2">' +
                        '<button id="addNewLanguage" class="btn btn-default">add Language</button>' +
                    '</div>' +*/
                    '<div class="col-xs-2">' +
                        '<select id="topic_select" type="text" name="topic-select" class="form-control" placeholder="Select Topic" readonly></select>' +
                    '</div>' +
                    '<div class="col-xs-1">' +
                    '</div>' +
                    '<div class="col-xs-5" style="line-height: 30px;">' +
                        '<span><a class="dwQueMas" tabindex="1">Click Here </a></span><label>Download Question format</label>' +
                    '</div>'+
                '</div>'+
                '<div id="question_Bank" class="row">' +
                '</div>' +
            '</div>'
            );
            questionmasterindex.DisplayLanguageToSelect();
        },
        
        submitNewLangForModule: function (e) {
            var json = [];
            var radios = $(".checklanguage"); 
            var count = 0;
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) {
                    json.push({"langId":radios[i].parentElement.parentElement.id.slice(5)});   
                }
            }
            if(json.length >= 1){
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addmodlang?moduleId="+mid,
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (data, textStatus, jqXHR) {
                        if(data == 1){
                            $("#createnewlanguage").modal('hide');
                            $("#createnewlanguage").on('hidden.bs.modal', function(){
                                questionmasterindex.DisplayLanguageToSelect(mid);
                            });
                        } else {
                            
                        }
                    },
                    error: function (res, ioArgs) {
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            } else {
                $("#createnewlanguage").modal('hide');
                swal({
                    title:"Please select at least one language",
                },
                     function(){
                    $("#createnewlanguage").modal('show');
                    $("#createnewlanguage").modal({ keyboard: false });
                });
            }
        },
    
        newLanguageForModule: function() {
            $("#createnewlanguage").modal({ keyboard: false });
            $("#createnewlanguage").modal('show');
            $("#language_table").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url: "maslanglist",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                    } else {
                        for( var i=0; i< data.length; i++ ){
                            $("#language_table").children("tbody").append(
                                '<tr id="lapre'+data[i].langId+'"><td>'+data[i].shortDesc+'</td><td><input type=checkbox class="checklanguage"></td></tr>'
                            );
                            $("#language_table").dataTable({
                                "ordering": false
                            });
                        }
                        $("#pageloading").show();
                        Backbone.ajax({
                            dataType: "json",
                            url: "modlanglist?moduleId="+mid+"",
                            data: "",
                            type: "POST",
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader(header, token);
                            },
                            success: function (data, textStatus, jqXHR) {
                                if(data == 0){
                                    
                                } else{
                                    for( var i=0; i< data.length; i++ ){
                                        $("#lapre"+data[i].langId+"").children().children(".checklanguage").attr('checked', true)
                                    }
                                }
                            },
                            error: function (res, ioArgs) {
                                
                                if (res.status === 440) {
                                    window.location.reload();
                                }
                            },
                complete: function() {
                    $("#pageloading").hide();
                }
                        });
                    }
                },
                error: function (res, ioArgs) {
                    if(res.status == 440){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    
    /*LOAD QUESTION ON BASIS OF TOPIC AND LANGUAGES*/
        loadQuestionAndOption: function(mid,lnid, tpid,quid){
        	 $("#pageloading").hide();
            $("#question_Bank").empty().append('<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="question_table" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<th style="width:30%;"> Question</th>' +
                                                '<th> Option_1</th>' +
                                                '<th> Option_2</th>' +
                                                '<th> Option_3</th>' +
                                                '<th> Option_4</th>' +
                                                '<th> Option_5</th>' +
                                                '<th> CorrectAns</th>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>');

            jQuery("#question_table").dataTable({
            	"bJQueryUI" : true,
            	"sPaginationType" : "full_numbers",
                "iDisplayLength": 10,
               // "bSort" :false,
                "bProcessing" : true,
                "bServerSide" : true,
                "bSort" : false,
                "sAjaxSource" : "paginlistquestions?moduleId="+mid+"&langId="+lnid+"&topicId="+tpid+ "&qbsId="+quid,
                "sServerMethod": "POST",
                "aoColumns" : [
                               { "mData": "question" },
                               { "mData": "opt1" },
                               { "mData": "opt2" },
                               { "mData": "opt3" },
                               { "mData": "opt4" },
                               { "mData": "opt5" },
                               { "mData": "correctAnswer" }
                ]
            });
            /*questionsl = $("#question_table").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url: "listquestions?moduleId="+mid+"&langId="+lnid+"&topicId="+tpid,
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                        questionsl.append('<tr><td colspan="7">NO DATA AVAILABLE</td></tr>');
                    } else {
                        questionmasterindex.generateQuestionTable(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if(res.status == 440){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
    
    /*GENERATE QUESTION TABLE */
        /*generateQuestionTable: function(data) {
            
            for( var i=0; i< data.length; i++ ){
                questionsl.append("<tr><td>"+data[i].question+"</td><td>"+data[i].opt1+"</td><td>"+data[i].opt2+"</td><td>"+data[i].opt3+"</td><td>"+data[i].opt4+"</td><td class='option5Cls'>"+data[i].opt5+"</td><td>"+data[i].correctAnswer+"</td></tr>");
                if(data[i].opt5 == null){
                    $("#questionid"+data[i].questionId+"").children(".option5Cls").text("");
                } else if(data[i].opt5 == 0){
                    $("#questionid"+data[i].questionId+"").children(".option5Cls").text("");
                }
            }
            
            $("#question_table").dataTable({
                "ordering": false,
                "info": true
            });
        },*/
    
        /*** GENERATE THE QUESTION LIST ALREADY IN ***/
        DisplayLanguageToSelect: function() {
            
            var lanid;
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "modlanglist?moduleId="+mid+"",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    languageElement = Backbone.$("#language_select").empty();
                    if(data == 0){
                        
                    } else{
                        for (var i=0; i< data.length; i++){
                            languageElement.append(
                                '<option value='+data[i].langId+' class="languageCls">'+data[i].shortDesc+'</option>'
                            );
                            if( i === 0 ){
                                lanid = data[i].langId;
                                lngeId = data[i].langId;
                            }
                        }
                    }
                },
                error: function (res, ioArgs) {
                        
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    questionmasterindex.displayQuestionToSelect(lngeId);
                    /*$("#question_table").empty();*/
                }
            });
        },


       disablepload:function(){
            $(".errorClss").hide();
            if(($("#question_select").val() == 0)){
                sweetAlert("Please select \"Question Bank Type\" before upload")
            }
        },
        
        
        

        /****Displaying the questions present in the list*/

        displayQuestionToSelect : function(lngid)
        {
            questionElement = Backbone.$("#question_select").empty();
             $("#pageloading").show();
             Backbone.ajax({
                dataType: "json",
                url: "listqbsbymodid?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                    } else{
                        questionmasterindex.generateQuestionList(data,lngid);
                    }
                },
                 error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    questionmasterindex.DisplayTopicToSelect(lngid,quid);
                  /*  $("#question_table").empty();*/

                }
            });
        },


    
        
        /*Generating the question list */

        generateQuestionList: function(data, lngid) {
            for (var i=0; i< data.length; i++){
                if(i == 0){
                    questionElement.append(
                        '<option value="0">Select</option>'
                    );
                }
                questionElement.append(
                    '<option value='+data[i].qbsId+' class="questionCls">' +data[i].shortDesc+'</option>'
                );
                
            }
            
        },
        
        
        /*****Changing to reset values ******/
        changeToReset : function(e){
             $('#question_select').prop('selectedIndex',0);
         },

         /*** GENERATE THE TOPIC LIST ALREADY IN ***/
         DisplayTopicToSelect: function(lngid,quid) {
             
             topicElement = Backbone.$("#topic_select").empty();
             $("#pageloading").show();
             Backbone.ajax({
                 dataType: "json",
                 url: "listtopic?moduleId="+mid,
                 data: "",
                 type: "POST",
                 beforeSend: function(xhr) {
                     xhr.setRequestHeader(header, token);
                 },
                 success: function (data, textStatus, jqXHR) {
                    // TopicElement = Backbone.$("#topic_select").empty();
                     if(data == 0){
                         
                     } 
                         else{
                              questionmasterindex.generateTopicList(data, lngid);
                              
                     }
                 
                 },
                 error: function (res, ioArgs) {
                     
                     if (res.status === 440) {
                         window.location.reload();
                     }
                 },
                 complete: function() {
                    $("#pageloading").hide();

                    
                 }
             });
         },



    /*** GENERATE THE TOPIC LIST ON ARRAY OR PANEL***/
        generateTopicList: function(data, lngid) {
            for (var i=0; i< data.length; i++){
                if(i == 0){
                    topicElement.append(
                        '<option value="0">Select</option>'
                    );
                }
                topicElement.append(
                    '<option value='+data[i].uniId+' class="topiccls">' +data[i].shortDesc+'</option>'
                );
                
            }
            
        },
        
        topicSelctedForUpload: function(e) {
            $(".errorClss").hide();
           var tpid = $("#topic_select").val();
           var lanid = $("#language_select").val();
           var quid = $("#question_select").val ();
              if(quid==0)
           {
               $(".errorQueSelectFailed").show();
                return false;
           }
           questionmasterindex.loadQuestionAndOption(mid,lanid,tpid,quid);
       },


       languageSelecteForUpload: function(e) {
           $(".errorClss").hide();
           var lnid = $("#language_select").val();
           var tpid = $("#topic_select").val();
           var quid=  $("#question_select").val ();
           if(quid==0)
           {
               $(".errorQueSelectFailed").show();
               return false;
           }
          questionmasterindex.loadQuestionAndOption(mid,lanid,tpid,quid);
       },
        
       questionSelectForUpload :function(e) {
           $(".errorClss").hide();
          var lanid = $("#language_select").val();
          var quid = $("#question_select").val ();
          var tpid =$("#topic_select").val();
            if(quid==0)
          {
              $(".errorQueSelectFailed").show();
               return false;
          }
          questionmasterindex.loadQuestionAndOption(mid,lanid,tpid,quid);
      },
        
        beforeQuestionMasterDownload: function(e) {
        	  $(".errorClss").hide();
              var quid = $("#question_select").val ();
              var tpid =$("#topic_select").val();
                 if(quid==0)
            {
                $(".errorQueSelectFailed").show();
                 return false;
            }
            if( ($("#language_select").val() == 0)){
                sweetAlert( "please select language before downloading");
            } else {
                var lanid = $("#language_select").val();
                var urld = "questionuploadtemplate?langId="+lanid+"&qbsId="+quid+"&topicid="+tpid;
                $(e.currentTarget).prop('href', urld);
            }
        },
        
    /*** QUESTION UPLOAD ***/
        questionsListUpload: function(e) {
            
            $(".errorClss").hide();
            var tpid =$("#topic_select").val();
            var quid = $("#question_select").val ();
               if(quid==0)
          {
              $(".errorQueSelectFailed").show();
               return false;
          }
            var lanid = $("#language_select").val();
            var urlv = "questionupload?"+csrf_url+"="+token+"&moduleId="+mid+"&langId="+lanid+"&qbsId="+quid
            $("#pageloading").show();
            $(e.currentTarget).parent().parent().parent().attr("action",urlv).ajaxForm({
                success : function(data) {
                    
                    var pf = $(".questionfile").parent();
                    $(".questionfile").remove();
                    $(pf).append('<input name="filename" type="file" class="questionfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px;">');
                    if(data=="1"){
                        sweetAlert("Questions Uploaded Successfully");
                        questionmasterindex.loadQuestionAndOption(mid,lanid,tpid,quid);
                    } else if(data=="2"){
                        $(".errorQueUploFailed").show();
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
        },
        
        beforeQuesUpload: function(e){
            if(($("#language_select").val() == null)){
                sweetAlert("Please select \"Language\" before upload")
            }
            var quid = $("#question_select").val ();
            if(quid==0)
           {
               $(".errorQueSelectFailed").show();
                return false;
           }
        },
        
    });
})();
