(function (){

    var moduleElement, employeeElement, editModuleFlag, mid;
    
    window.program_module = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    program_module.Views.header = Backbone.View.extend({
        
        events: {
            'click .modulecancel': 'moduleOnCancel',
            'click .createmodule': 'createNewModule',
            'click .modulesubmit': 'submitModule',
            'click .editmodule': 'editPModule',
            'click #addgrouptoadmin': 'addGroupToAdminsList',
            //'focusout #groupId': 'addGroupToAdminsList',
            'click .editmodulesubmit': 'beforeSubmittingEditDetails',
            'click .removeSelectAdmin': 'remoVeSelectedModuleAdmin',
            /*Language adding events*/
            'click .applanguagesubmit': 'submitNewLangugeToApp',
            'click .applangeditsubmit': 'subnitEditLanguage',
            'click .new_language': 'addANewLanguageToApp',
            'keyup #lang_id': 'specialCharValidate',
            'click .editlanguage': 'langEditForMod',
            /*Slot Adding Events*/
            'click .new_slot': 'addNewSlotToApp',
            'change .brachfile': 'uploadBranchesList'
        },
        
        initialize: function () {
            _.bindAll(this,'render');
            editModuleFlag = false;
        },
        
        render: function (modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="program_modal" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Program Modules"> &nbsp;&nbsp;' +
                                '<i class="fa fa-indent fa-fw"></i>' +
                                '<span class="content-hide">Program Modules</span>' +
                            '</li>' +
                            '<li id="language_add" class="maind" data-toggle="tooltip tab" data-original-title="languages"> &nbsp;&nbsp;' +
                                '<i class="fa fa-list-alt fa-fw"></i>' +
                                '<span class="content-hide">Languages</span>' +
                            '</li>' +
                            '<li id="slot_add" class="maind" data-toggle="tooltip tab" data-original-title="Branches"> &nbsp;&nbsp;' +
                                '<i class="fa fa-database fa-fw"></i>' +
                                '<span class="content-hide">Branches</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">'+
                '<div class="col-xs-12">'+
                    '<h1 class="page-header"> Program Modules <div class="btn goToLastPage"><i class="fa fa-reply-all"></i>Back</div></h1>'+
                '</div>'+
                '<!-- /.col-xs-12 -->'+
            '</div>' +
            '<div id="module_wrapper">' +
                '<div class="row">' +
                    '<div class="col-xs-3">' +
                        '<button class="createmodule btn btn-default" type="button">' +
                            '<i class="fa fa-indent"></i>  &nbsp;Create Module' +
                        '</button>' +
                    '</div>' +
                    '<div class="col-xs-9 pull-right" style="text-align: center;">' +
                        '<div class="errorClss errorModuleSubmit">Some problem in creating the module. Try later!</div>' +
                        '<div class="errorClss errorModuleEditSubmit">Error On Submitting. Try later!</div>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="moduletable" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<th> Module Name</b></th>' +
                                                '<th> Status</b></th>' +
                                                '<th> Internet</b></th>' +
                                                '<th> </th>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="modal fade" id="createnewmodule" tabindex="-1" role="dialog" aria-labelledby="createnewmodule" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Module Details </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<form role="form" id="moduleform">' +
                                    '<div class="row beforeAdminDetails">' +
                                        '<div class="form-group col-xs-11">' +
                                            '<label for="modulename" class="control-label">Title</label><span class="clsRed">*</span>' +
                                            '<input id="modulename" type="text" name="module-name" class="form-control">' +
                                            '<div class="errorClss errorManModuName"> Module Name Required </div>' +
                                        '</div>' +
                                    '</div>' +
                                    /*'<div class="row beforeAdminDetails">' +
                                        '<div class="form-group col-xs-6">' +
                                            '<label for="modulevalidfrom" class="control-label">Valid From</label><span class="clsRed">*</span>' +
                                            '<input id="modulevalidfrom" type="text" name="module-validfrom" class="form-control" readonly>' +
                                        '</div>' +
                                        '<div class="form-group col-xs-6">' +
                                            '<label for="modulevalidto" class="control-label">Valid To</label><span class="clsRed">*</span>' +
                                            '<input id="modulevalidto" type="text" name="module-validto" class="form-control" readonly>' +
                                        '</div>' +
                                    '</div>' +*/
                                    '<div class="row beforeAdminDetails">' +
                                        '<div class="col-xs-4 form-group" style="padding: 12px">' +
                                            '<label for="modulecheck" class="control-label" style="vertical-align: middle;">Module activate : </label>' +
                                        '</div>' +
                                        '<div class="col-xs-6 form-group">' +
                                            '<div id="yes-button">' +
                                                '<label>' +
                                                    '<input type="radio" class="modulecheck" name="module-status" value="1">' +
                                                    '<span>YES</span>' +
                                                '</label>' +
                                            '</div>' +
                                            '<div id="no-button">' +
                                                '<label>' +
                                                    '<input type="radio" class="modulecheck" name="module-status" value="0">' +
                                                    '<span>NO</span>' +
                                                '</label>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row beforeAdminDetails">' +
                                        '<div class="col-xs-4 form-group" style="padding: 12px">' +
                                            '<label for="internetcheck" class="control-label" style="vertical-align: middle;">Available on internet : </label>' +
                                        '</div>' +
                                        '<div class="col-xs-6 form-group">' +
                                            '<div id="yes-button">' +
                                                '<label>' +
                                                    '<input type="radio" class="internetcheck" name="internet-status" value="1">' +
                                                    '<span>YES</span>' +
                                                '</label>' +
                                            '</div>' +
                                            '<div id="no-button">' +
                                                '<label>' +
                                                    '<input type="radio" class="internetcheck" name="internet-status" value="0">' +
                                                    '<span>NO</span>' +
                                                '</label>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="form-group col-xs-8">' +
                                            '<label for="groupId" class="control-label">Admin Group Id</label><span class="clsRed">*</span>' +
                                            '<input id="groupId" type="text" name="module-Admins" class="form-control">' +
                                            '<ul class="dropdown-menu" id="mastergrouplistid" style="text-align:justify;">' +
                                            '</ul>' +
                                        '</div>' +
                                        '<div class="form-group col-xs-2">' +
                                            '<div class="btn btn-default" id="addgrouptoadmin">Add group</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="form-group col-xs-12" style="margin-bottom: 0;">' +
                                        '<div class="errorClss errModAdminGroup"> At Least One Admin Required </div>' +
                                        '<div class="errorClss errModAdmGrpId"> Please Add Group Id </div>' +
                                        '<div class="panel-body">' +
                                            '<div class="table-responsive">' +
                                                '<table id="selecedAdminTable" class="table table-hover">' +
                                                    '<tbody>' +
                                                    '</tbody>' +
                                                '</table>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</form>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="modulecancel btn btn-default"> Cancel </div>' +
                            '<div class="modulesubmit btn btn-default"> Submit </div>' +
                            '<div class="editmodulesubmit btn btn-default"> Submit </div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
            );
            
            this.moduleTableCreate();
            this.dateValidation();
        },
    /* DATE PICKER CALLING*/
        dateValidation: function() {
            $("#modulevalidfrom").datepicker({
                dateFormat: "yy-m-dd"
            });
            $('#modulevalidto').datepicker({
                dateFormat: "yy-m-dd"
            });
        },
        
    /*FETCH THE DETAILS OF module ALREADY PRESENT AND DISPLAY IN TABLE*/
        moduleTableCreate: function (e) {
            
            moduleElement = Backbone.$("#moduletable").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listmodule",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                        swal({
                            title: "No Module is created yet, You want to create new Module",
                            showCancelButton: true,
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Yes",
                            cancelButtonText: "No"
                        },
                             function(isConfirm){
                            if (isConfirm) {
                                programmoduleindex.createNewModule();
                            }
                        });
                    } else {
                        programmoduleindex.generatemoduleTable(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },

    /*~~~~ GENERATE TABLE ELEMENTS OF moduleS FROM JSON moduleLIST~~~~*/
        generatemoduleTable: function (data) {
            //get table id from jquery
            for (var i=0; i< data.length; i++){
                moduleElement.append(
                '<tr id="module'+data[i].moduleId+'" value="'+data[i].active+'">' +
                    '<td class="moduledesc">'+data[i].shortDesc+'</td>' +
                    '<td class="modstats" value="'+data[i].active+'"></td>' +
                    '<td class="internetcheckcls" value="'+data[i].internetStatus+'"></td>' +
                    '<td><div class="editmodule btn btn-default"><a><i class="glyphicon glyphicon-edit"></i> Edit </a></div></td>' +
                '</tr>');
                
                if(data[i].active == 1){
                    $("#module"+data[i].moduleId+"").children(".modstats").text("Enabled");
                } else {
                    $("#module"+data[i].moduleId+"").children(".modstats").text("Disabled");
                }
                if(data[i].internetStatus == 1){
                    $("#module"+data[i].moduleId+"").children(".internetcheckcls").text("Enabled");
                } else {
                    $("#module"+data[i].moduleId+"").children(".internetcheckcls").text("Disabled");
                }
            }
            $("#moduletable").dataTable({
                "ordering": false
            });
            
        },
        
    /* CREATE NEW module */    
        createNewModule: function() {
            $(".beforeAdminDetails").show();
            editModuleFlag = false;
            $("#createnewmodule").modal({ keyboard: false });
            $('#modulename').val("");
            $("#createnewmodule").modal('show');
            $("#yes-button label .modulecheck").prop('checked', true);
            $("#no-button label .internetcheck").prop('checked', true);
            $(".modulesubmit").css("display","inline-block");
            $(".editmodulesubmit").hide();
            $(".editAdminPrivilegModule").hide();
            $("#modulename").val("");
            $("#groupId").val("")
            $("#selecedAdminTable").children("tbody").empty();
        },
    
        addGroupToAdminsList: function(e){
            $(".errorClss").hide();
            var gpid = $("#groupId").val();
            if( gpid == 0){
                $(".errModAdmGrpId").show();
            } else {
                var bnm = $("#selecedAdminTable").children("tbody").children("tr").length;
                $("#selecedAdminTable").children("tbody").append(
                    '<tr id="adminadd'+(bnm+1)+'">' +
                        '<td>' +gpid+'</td>' +
                        '<td><div class="removeSelectAdmin btn btn-default"><a> Remove </a></div></td>' +
                    '</tr>');
                $("#groupId").val("");
            }
        },
    
    /*** SUBMITTING DETAILS FOR module CREATION ****/
        submitModule: function(e) {
            $(".errorClss").hide();
            var urlv;
            if($('#modulename').val() == 0) {
                $(".errorManModuName").show();    
            } else if ($('#selecedAdminTable').children('tbody').children('tr').length == 0) {
                $(".errModAdminGroup").show();
            } else {
                if(editModuleFlag){
                    programmoduleindex.editingModuleDetails();
                } else {
                    programmoduleindex.creatingModuleFinalDetails();
                }
            }
        },
        
    /*EDITING MODULE DETAILS LIST */
        editingModuleDetails: function(urlv) {
            $(".errorClss").hide();
            var employlength = $('#selecedAdminTable').children('tbody').children('tr').length;
            var ins;
            var mid = $("#modulename").attr('value');
            var activ;
            if($('.modulecheck').is(':checked')){
                for( var i = 0; i < $('.modulecheck').length; i++ ){
                    if( $('.modulecheck')[i].checked){
                        activ = $($('.modulecheck')[i]).val();
                    }
                }
            }
            
            if($('.internetcheck').is(':checked')){
                for( var i = 0; i < $('.internetcheck').length; i++ ){
                    if( $('.internetcheck')[i].checked){
                        ins = $($('.internetcheck')[i]).val();
                    }
                }
            }
            
            var json;
            var eidjson = [];
            try{
                for (var i=0; i < employlength; i++){
                    eidjson.push({"groupId": $($($('#selecedAdminTable').children('tbody').children('tr')[i]).children('td')[0]).text()});
                }
            }catch(e){}
            
            try{
                json={"shortDesc":$('#modulename').val(),"active":activ,"group":eidjson,"internetStatus":ins,"moduleId":mid};
            }catch(e){}
            $("#pageloading").show();
            Backbone.ajax({
                url: "editmodule",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    
                    $("#createnewmodule").modal('hide');
                    if(data == 1){
                        $("#createnewmodule").on('hidden.bs.modal', function(){
                            window.location.reload();
                        });
                    } else if(data == 2){
                        $(".errorModuleEditSubmit").show();
                    } else {
                        alert("Contact IT team");
                    }
                    },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
    },
        
    /*FINAL SUBMITTING OF DETAILS FOR MODULE CREATION */
        creatingModuleFinalDetails: function(urlv) {
            $(".errorClss").hide();
            var employlength = $('#selecedAdminTable').children('tbody').children('tr').length;
            var ins;
            
            var activ;
            if($('.modulecheck').is(':checked')){
                for( var i = 0; i < $('.modulecheck').length; i++ ){
                    if( $('.modulecheck')[i].checked){
                        activ = $($('.modulecheck')[i]).val();
                    }
                }
            }
            
            if($('.internetcheck').is(':checked')){
                for( var i = 0; i < $('.internetcheck').length; i++ ){
                    if( $('.internetcheck')[i].checked){
                        ins = $($('.internetcheck')[i]).val();
                    }
                }
            }
            var json;
            var eidjson = [];
            try{
                for (var i=0; i < employlength; i++){
                    eidjson.push({"groupId": $($($('#selecedAdminTable').children('tbody').children('tr')[i]).children('td')[0]).text()});
                }
            }catch(e){}
            
            try{
                json={"shortDesc":$('#modulename').val(),"active":activ,"group":eidjson,"internetStatus":ins};
            }catch(e){}
            $("#pageloading").show();
            Backbone.ajax({
                url: "createmodule",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    
                    $("#createnewmodule").modal('hide');
                    if(data == 1){
                        $("#createnewmodule").on('hidden.bs.modal', function(){
                            window.location.reload();
                        });
                    } else if(data == 2){
                        $(".errorModuleSubmit").show();
                    } else {
                        alert("Contact IT team");
                    }
                    },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
    },
        
    /*EDITING THE MODULE NAME*/
        
        editPModule: function(e) {
            $(".beforeAdminDetails").show();
            $("#groupId").val("");
            var mid = e.currentTarget.parentElement.parentElement.id.slice(6);
            var mn = $('#'+e.currentTarget.parentElement.parentElement.id+'').children(".moduledesc").text();
            if($('#'+e.currentTarget.parentElement.parentElement.id+'').attr('value') == 1){
                $("#yes-button label .modulecheck").prop('checked', true);
            } else {
                $("#no-button label .modulecheck").prop('checked', true);
            }
            if($('#'+e.currentTarget.parentElement.parentElement.id+'').children(".internetcheckcls").attr('value') == 1){
                $("#yes-button label .internetcheck").prop('checked', true);
            } else {
                $("#no-button label .internetcheck").prop('checked', true);
            }
            $("#createnewmodule").modal({ keyboard: false });
            $('#modulename').val(mn);
            $('#modulename').attr('value', mid);
            $("#createnewmodule").modal('show');
            $(".editmodulesubmit").css("display","inline-block");
            $(".modulesubmit").hide();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listmoduleadmin?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    var adminElemntsModule = $("#selecedAdminTable").children("tbody").empty();
                    if(data == 0){
                        adminElemntsModule.append(
                        '<tr>' +
                            '<td colspan="2">No Admin Assigned</td>' +
                        '</tr>');
                    } else {
                        for(var i=0; i<data.length; i++){
                            adminElemntsModule.append(
                            '<tr id="adminadd'+i+'">' +
                                '<td>' +data[i].groupId+'</td>' +
                                '<td><div class="removeSelectAdmin btn btn-default"><a> Remove </a></div></td>' +
                            '</tr>');
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    
    /* VALIDATING THE MODULE EDITING */
        beforeSubmittingEditDetails: function(e) {
            editModuleFlag = true;
            programmoduleindex.submitModule();
        },
        
        remoVeSelectedModuleAdmin: function(e) {
            $("#"+e.currentTarget.parentElement.parentElement.id+"").remove();
        },
        
        moduleOnCancel: function(e) {
            $("#createnewmodule").modal('hide');
            swal({
                title: "Do you want cancel module creation",
                showCancelButton: true,
                confirmButtonColor: "#F01E1E",
                confirmButtonText: "Yes",
                cancelButtonText: "No"
            },
                 function(isConfirm){
                if (isConfirm) {
                    $("#createnewmodule").modal('hide');
                } else {
                    $("#createnewmodule").modal({ keyboard: false });
                    $("#createnewmodule").modal('show');
                }
            });
        },
    
    /*LANGUAGES CREATION */
        languagesDisplay: function(modulid) {
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="program_modal" class="maind" data-toggle="tooltip tab" data-toggle="tooltip tab" data-original-title="Program Modules"> &nbsp;&nbsp;' +
                                '<i class="fa fa-indent fa-fw"></i>' +
                                '<span class="content-hide">Program Modules</span>' +
                            '</li>' +
                            '<li id="language_add" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="languages"> &nbsp;&nbsp;' +
                                '<i class="fa fa-list-alt fa-fw"></i>' +
                                '<span class="content-hide">Languages</span>' +
                            '</li>' +
                            '<li id="slot_add" class="maind" data-toggle="tooltip tab" data-original-title="Branches"> &nbsp;&nbsp;' +
                                '<i class="fa fa-database fa-fw"></i>' +
                                '<span class="content-hide">Branches</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">'+
                '<div class="col-xs-12">'+
                    '<h1 class="page-header"> Languages <div class="btn goToLastPage"><i class="fa fa-reply-all"></i>Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->'+
            '</div>' +
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<div class="row" style="margin-bottom: 10px;">' +
                        '<div class="col-xs-6">' +
                            '<div class="btn btn-outline btn-primary new_language"> New Language </div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="language_list" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<td><b>Language Code</b></td>' +
                                                '<td><b>Language Name</b></td>' +
                                                '<td><b></b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div class="modal fade" id="add_language" tabindex="-1" role="dialog" aria-labelledby="add_language" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Add Languages </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-6">' +
                                        '<div class="form-group">' +
                                            '<label for="lang_id" class="control-label">Language Code</label><span class="clsRed">*</span>' +
                                            '<input id="lang_id" type="text" name="language-code" class="form-control">' +
                                            '<div class="errorClss errorLangCode">Language Code is Mandatory</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="col-xs-6">' +
                                        '<div class="form-group">' +
                                            '<label for="lang_name" class="control-label">Language Name</label><span class="clsRed">*</span>' +
                                            '<input id="lang_name" type="text" name="language-name" class="form-control">' +
                                            '<div class="errorClss errorLangName">Language Name is Mandatory</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-xs-6">' +
                                        '<div class="form-group">' +
                                            '<div class="errorClss errorSpecialChar">No special characters allowed</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="errorClss errorOnLangSub pull-left">Language adding failed, Try Again</div>' +
                            '<div class="btn btn-default" align="center" data-dismiss="modal"> Cancel </div>' +
                            '<div class="applanguagesubmit btn btn-default"> Submit </div>' +
                            '<div class="applangeditsubmit btn btn-default"> Submit </div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
            );
            
            this.languageListInModule();
        },
        
        languageListInModule: function(){
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "maslanglist",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    Backbone.$("#language_list").children("tbody").empty();
                    if(data == 0){
                        Backbone.$("#language_list").children("tbody").append(
                            '<tr>' +
                                '<td colspan="3"> NO DATA FOUND </td>' +
                            '</tr>'
                            );
                    } else{
                        for (var i=0; i< data.length; i++){
                            Backbone.$("#language_list").children("tbody").append(
                            '<tr id="'+data[i].langId+'">' +
                                '<td>'+data[i].langId+'</td>' +
                                '<td>'+data[i].shortDesc+'</td>' +
                                '<td><button class="editlanguage btn btn-default"><a><i class="glyphicon glyphicon-edit"></i> Edit </a></button></td>' +
                            '</tr>'
                            );
                        }
                        $("#language_list").dataTable({
                            "ordering": false
                        });
                        
                        $("#language_list").show();
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        addANewLanguageToApp: function() {
            $(".applanguagesubmit").css({"display":"inline-block"});
            $(".applangeditsubmit").hide();
            $("#add_language").modal('show');
            $("#lang_name").val("");
            $("#lang_id").val("");
            $("#lang_id").prop('disabled', false);
        },
        
        subnitEditLanguage: function() {
            var urls = "editlang";
            programmoduleindex.submitLangDetailsForModule( urls );
        },
        
        submitNewLangugeToApp: function() {
            var urls = "addmaslang";
            programmoduleindex.submitLangDetailsForModule( urls );
        },
        
        submitLangDetailsForModule: function( urls ) {
            $("errorClss").hide();
            if ($("#lang_id").val() == 0) {
                $(".errorLangCode").show();
            } else if($("#lang_name").val() == 0) {
                $(".errorLangName").show();
            } else {
                var lid = $("#lang_id").val().toUpperCase();
                var lname = $("#lang_name").val();
                $("#pageloading").show();
                Backbone.ajax({
                    url: ""+urls+"?langCode="+lid+"&langName="+lname,
                    data: "",
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        if(data == 1){
                            $("#add_language").modal('hide');
                            $("#add_language").on('hidden.bs.modal', function(){
                                programmoduleindex.languagesDisplay();
                            });
                        } else if(data == 2){
                            $(".errorOnLangSub").show();
                        } 
                    },
                    error:function(res,ioArgs){
                        
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
        langEditForMod: function(e) {
            var laN = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children()[1]).text();
            var laI = e.currentTarget.parentElement.parentElement.id;
            $("#add_language").modal('show');
            $("#lang_name").val(laN);
            $("#lang_id").val(laI);
            $("#lang_id").prop('disabled', true);
            $(".applanguagesubmit").hide();
            $(".applangeditsubmit").css({"display":"inline-block"});
        },
        
        
    /*Slot CREATION */
        slotDisplay: function(modulid) {
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="program_modal" class="maind" data-toggle="tooltip tab" data-toggle="tooltip tab" data-original-title="Program Modules"> &nbsp;&nbsp;' +
                                '<i class="fa fa-indent fa-fw"></i>' +
                                '<span class="content-hide">Program Modules</span>' +
                            '</li>' +
                            '<li id="language_add" class="maind" data-toggle="tooltip tab" data-original-title="languages"> &nbsp;&nbsp;' +
                                '<i class="fa fa-list-alt fa-fw"></i>' +
                                '<span class="content-hide">Languages</span>' +
                            '</li>' +
                            '<li id="slot_add" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Branches"> &nbsp;&nbsp;' +
                                '<i class="fa fa-database fa-fw"></i>' +
                                '<span class="content-hide">Branches</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">'+
                '<div class="col-xs-12">'+
                    '<h1 class="page-header"> Branches <div class="btn goToLastPage"><i class="fa fa-reply-all"></i>Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->'+
            '</div>' +
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<div class="row" style="margin-top: 10px;">' +
                        '<div class="col-xs-6">' +
                            '<label class="control-label">Upload excel of branch list</label>' +
                            '<div class="col-xs-1" style="left:64%; top:-8px;">' +
                                '<form action="branchlistupload" method="post" enctype="multipart/form-data">'+
                                    '<div class="btn btn-outline btn-primary btn-file">'+
                                        '<i class="fa fa-upload">'+
                                        '</i> &nbsp;Upload <input name="filename" type="file" class="brachfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="left: 16px;width: 90px;">'+
                                    '</div>'+
                                '</form>'+
                            '</div>'+
                            '<span class="errorClss errorBrnchUploFailed"><br>Branch Uploading Failed. Try Again..! </span>' +
                        '</div>'+
                            '<div class="col-xs-5">' +
                                '<span><a href="branchlistuploadtemplate" target="_blank">Click Here </a>to download excel of branch list</span>' +
                            '</div>'+
                    '</div>' +
                    '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="branch_list" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<td><b>Branch Name</b></td>' +
                                                '<td><b>Branch Code</b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div class="modal fade" id="add_slot" tabindex="-1" role="dialog" aria-labelledby="add_slot" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Add slot to the application </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="form-group">' +
                                            '<label for="slot_name" class="control-label">Slot Name</label><span class="clsRed">*</span>' +
                                            '<input id="slot_name" type="text" name="slot-name" class="form-control">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="form-group col-xs-6">' +
                                        '<label for="slotvalidfrom" class="control-label">Slot start</label><span class="clsRed">*</span>' +
                                        '<input id="slotvalidfrom" type="text" name="slot-validfrom" class="form-control">' +
                                    '</div>' +
                                    '<div class="form-group col-xs-6">' +
                                        '<label for="slotvalidto" class="control-label">Slot end</label><span class="clsRed">*</span>' +
                                        '<input id="slotvalidto" type="text" name="slot-validto" class="form-control">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="btn btn-default" align="center" data-dismiss="modal"> Cancel </div>' +
                            '<div class="appSlotSubmit btn btn-default"> Submit </div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
            );
            $('#slotvalidfrom').timepicker();
            $('#slotvalidto').timepicker();
            programmoduleindex.loadAllBranches();
        },
        
        uploadBranchesList: function(e) {
            $("#pageloading").show();
            $(".errorClss").hide();
            $(e.currentTarget).parent().parent().ajaxForm({
                success : function(data) {
                    if(data == 1) {
                        swal({
                            title:"Branch List Uploaded Successfully",
                        },
                             function(){
                            window.location.reload();
                        });
                    } else if(data == 2){
                        $(".errorBrnchUploFailed").show();
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    $(".trainersfile").val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
            
        },
        
    /* ALL BRACHES DISPLAY */
        loadAllBranches: function(uid) {
            Backbone.$("#branch_list").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getallbranches",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        Backbone.$("#branch_list").children("tbody").append(
                            '<tr>' +
                                '<td colspan="2"> NO DATA FOUND </td>' +
                            '</tr>');
                    } else{
                        for (var i=0; i< data.length; i++){
                            Backbone.$("#branch_list").children("tbody").append(
                            '<tr>' +
                                '<td>'+data[i].branchName+'</td>' +
                                '<td>'+data[i].branchCode+'</td>' +
                            '</tr>');
                        }
                        Backbone.$("#branch_list").dataTable({
                            "ordering": false
                        });
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        addNewSlotToApp: function() {
            $("#add_slot").modal('show');
            $("#slot_name").val("")
            $("#slotvalidfrom").val("");
        },
        
        specialCharValidate: function(e) {
            $(".errorClss").hide();
            if($("#lang_id").val().length >= 1){
                var myStr = $("#lang_id").val();
                if(!(myStr.match(/^[a-zA-Z0-9]*$/))){
                    myStr = myStr.replace(/[^a-zA-Z0-9]/g,'');
                    $("#lang_id").val(myStr);
                    $(".errorSpecialChar").show();
                }
            }
        },
    });
})();