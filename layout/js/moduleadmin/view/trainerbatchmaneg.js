(function () {
    
    /* GLOBAL VARIABLES FOR THIS BATCH MANEGMENT */
    var batchElement, batchElementForDateSearch, listofTrainers, listofStudents, trainersIn, studentsIn, studentlistIn, baid, batchAttendenceFlag, batchStuAten, mid, presentCount, noOfDays, trainingsDetailsElement, studentScore;
    var json = [];
    var studentFlag = false;
    var reloadErrorFlag = false;
    var selected=[];
    var preDateArray = 0;
    
    /* GLOBAL CALLING ALL BACKBONE MVC */
    window.trainer_batch_manage = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    
    /*CREATING VIEW FOR BATCH MANAGEMENT */
    trainer_batch_manage.Views.header = Backbone.View.extend({
        
        events: {
            'click .addnewbatch': 'createNewBatch',
            'click .editbatch': 'editBatchDetails',
            'click .btdetails': 'batchDetails',
            'click .batchsubmit': 'newBatchSubmit',
            'click .editbatchsubmit': 'editBatchSubmit',
            'click .alertOk': 'clickAlertOk',
            'click #searchTraining': 'trainingDetailsList',
           /* 'click .circle-cls': 'openDashBoardDetailsModal',*/
            'change .pdefiletrainer': 'studentPdeListUploadTrainer',
            'change #batchvalidto': 'validateEndDate',
//            'click  #rulecheck':'expensesView',
            'keypress #allownumbers':'validateNumbers' ,
            'change .searchdatepic' :'getselecteddate'
        },
        
        initialize: function () {
            _.bindAll(this, 'render');
        },
        
        render: function (modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="training_done" class="maind" data-toggle="tooltip tab" data-original-title="Training Details">&nbsp;&nbsp;<i class="fa fa-list-ol fa-fw"></i>' +
                                '<span class="content-hide">Dashboard</span>' +
                            '</li>' +
                            '<li id="trainerbatchmanagement" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="attend_manage" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="manage_user" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                        	'<span class="content-hide">User Management</span>' +
	                        '</li>' +
	                        '<li id="hierarchyview" class="maind" data-toggle="tooltip tab" data-original-title="Hierarchy View">&nbsp;&nbsp;<i class="fa fa-camera-retro fa-fw"></i>' +
	                        '<span class="content-hide">Hierarchy View</span>' +
	                        '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header"> List of batches <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div id="batch_wrapper" class="row">' +
                '<div class="row">' +
                    '<div class="col-xs-3" style="margin-bottom: 20px;">' +
                        '<button class="addnewbatch btn btn-default" type="button">' +
                            '<i class="fa fa-calendar"></i>  &nbsp;Add New Batch' +
                        '</button>' +
                    '</div>' +
                    '<div class="col-xs-9">' +
                        '<div class="pull-right errorClss errorBatchName">Batch Selection Failed. Try Again..!</div>'+
                    '</div>'+
                    '<div class="col-xs-9">' +
                    '<span> <a href="tr-excelreportofallbatch" target="_blank"> Click Here</a> download module report batchwise</span>'+
                    '<div class="pull-right errorClss errorBatchName">Batch Selection Failed. Try Again..!</div>'+
                    '</div>'+
                    '<div class="batch_calendar">' +
                		'<span">Search by date:</span>'+
//                		'<input  readonly="readonly" size="8" type="text" class="searchdatepic fa fa-calendar">'+
                		'<input id="fromdate" readonly="readonly" size="8" type="text" class="datepicker searchdatepic fa fa-calendar" placeholder="From">'+
                		'<span class=" errorClss errorFromDate">Select From Date..</span>'+
                		'<input id="todate" readonly="readonly" size="8" type="text" class="datepicker searchdatepic fa fa-calendar" placeholder="To">'+
                	'<span class="errorClss errorToDate">Select To Date..</span>'+
                '</div>'+
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12" id="batchtablehide">' +
                        '<div class="panel">' +
                            '<div class="panel-body" >' +
                                '<div class="table-responsive">' +
                                    '<table id="batchtable" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                            '<td><b>Batch Name</b></td>' +
                                            '<td><b>Start Date</b></td>' +
                                            '<td><b>End Date</b></td>' +
                                            '<td><b>Lunch Expenses</b></td>' +
                                            '<td><b>Miscellaneous Expenses</b></td>' +
                                            '<td><b>Travelling Expenses</b></td>' +
                                            /*'<td><b>Status</b></td>' +*/
                                            '<td><b>Edit</b></td>' +
                                            '<td><b>No of Students</b></td>' +
                                            '<td><b>Candidates Appeared</b></td>' +
                                            '<td><b>Candidates Passed</b></td>' +
                                            '<td><b>Candidates Failed</b></td>' +
                                            '<td><b>Passing %</b></td>' +
                                            '<td><b>Candidates Pending</b></td>' +
                                            '<td><b>Batch Report</b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-xs-12" id="fromandenddate">' +
                    '<div class="panel panel-default", style="margin-top:30px">' +
                        '<div class="panel-heading">' + 
                            '<h4 style="margin-top:0; margin-bottom:0; text-align: center;" class="user_heading"></h4>' +
                        '</div>' +
                        '<div class="panel-body">' +
                            '<div class="tab-content" id="userTableInherit">' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'+
            '</div>' +
            '<div class="modal fade" id="createnewbatch" tabindex="-1" role="dialog" aria-labelledby="createnewbatch" aria-hidden="true" data-backdrop="static" style="margin-top:5%">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Add New Batch </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                    '<form role="form" id="batchform">' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-12">' +
                                            '<label for="batchname" class="control-label">Name of batch</label><span class="clsRed">*</span>' +
                                            '<input id="batchname" type="text" name="batch-name" class="form-control">' +
                                            '<div class="errorClss errorBatchNameReq"> Batch Name Required </div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-6">' +
                                            '<label for="batchvalidfrom" class="control-label">Batch start date</label><span class="clsRed">*</span>' +
                                            '<input id="batchvalidfrom" type="text" name="batch-validfrom" class="form-control" readonly>' +
                                            '<div class="errorClss errorBatchStar"> Required </div>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-6">' +
                                            '<label for="batchvalidto" class="control-label">Batch end date</label><span class="clsRed">*</span>' +
                                            '<input id="batchvalidto" type="text" name="batch-validto" class="form-control" readonly>' +
                                            '<div class="errorClss errorBatchEnd"> Required </div>' +
                                            '<div class="errorClss errortrEndDate"> Batch End Should be After Batch Start </div>' +
                                        '</div>' +
                                    '</div>' +
                              /*  '<div class="row">' + 
                                 '<div class ="form-group-control col-xs-6">' +
                                    '<label for="questionbox" class="control-label">Question Bank</label><span class="clsRed">*</span>' +
                                    '<select id="questionbox" type="text" name="question-name" class="form-control">' +
                                        '<option value="0">Select</option>' +
                                         '<option value="A">A</option>' +
                                         '<option value="B">B</option>' +
                                         '<option value="C">C</option>' +
                                         '<option value="D">D</option>' +
                                         '<option value="E">E</option>' +
                                         '<option value="F">F</option>' +
                                         '<option value="G">G</option>' +
                                         '<option value="H">H</option>' +
                                         '<option value="I">I</option>' +
                                         '<option value="J">J</option>' +
                                    '</select>' +
                                    '<div class="errorClss errorQuestionReq">selection of Question is Required  </div>' +
                                  '</div>' +
                                 '</div>' + */
                                 '<hr/>'+
                                          '<h1> EXPENSES </h1>' +
                                    '<hr/>' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-6">' +
                                            '<label for="actualexpenses" class="control-label">Actuals</label>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-6">' +
                                            '<label for="approvedexpenses" class="control-label">HO Approved</label><span class="clsRed">  </span><input  id="rulecheck" onclick="return false" type="checkbox" class="useruseCls" />' +
                                        '</div>' +
                                    '</div>' +
                                    '<div id="allownumbers">'+ 
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-3">' +
                                            '<label for="travellingexpenses" class="control-label">Travelling</label>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4">' +
                                           '<input id="travellingexpenses" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="travelling-expenses" class="form-control">' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4" id="approvedtrexpens">' +
                                            '<input id="aprovedtravelling" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="ho-approved" class="form-control" readonly>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-3">' +
                                            '<label for="lunchexpenses" class="control-label">Lunch</label>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4">' +
                                            '<input id="lunchexpenses" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="lunch-expenses" class="form-control">' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4" id="approvedluexpens">' +
                                            '<input id="aprovedlunch" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="ho-approved" class="form-control" readonly >' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-3">' +
                                            '<label for="miscellaneousexpenses" class="control-label">Miscellaneous</label>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4">' +
                                            '<input id="miscellaneousexpenses" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="miscellaneous-expenses" class="form-control">' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4" id="approvedmiexpens">' +
                                            '<input id="aprovedmiscellaneous" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="ho-approved" class="form-control" readonly>' +
                                        '</div>' +
                                    '</div>' +
                                 '</div>' +
                                '</form>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="errorClss errorOnCreSub pull-left"> Submitting Error, Try Later..! </div>' +
                            '<div class="btn btn-default" align="center" data-dismiss="modal"> Cancel </div>' +
                            '<div class="batchsubmit btn btn-default"> Submit </div>' +
                            '<div class="editbatchsubmit btn btn-default"> Submit </div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
            );
            /*if(localStorage.getItem("addStudentCheckValue") == 1){
        		$("#manage_user").show();
        	}*/
            this.batchTableCreate();
            this.dateValidation();
            this.getsearchdate();
        },
        
        
        validateEndDate: function(e) {
            $(".errorClss").hide();
            var baEndDate = parseInt($('#batchvalidto').val().replace(/\D+/g, ''));
            var baStartDate = parseInt($("#batchvalidfrom").val().replace(/\D+/g, ''));
            
            if(baEndDate < baStartDate){
                $(".errortrEndDate").show();
            }
        },
        
        
    /*Fetch The batch From The Table*/
        batchTableCreate: function (e) {
            var json
            $("#pageloading").show();
            $("#batchtable").show();
            $("#fromandenddate").hide();
            Backbone.ajax({
                dataType: "json",
                url: "tr-listbatchbymid?moduleId="+mid+"&empId="+localStorage.getItem("empId"),
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        swal({
                            title: "No Batch is created yet, You want to create new batch",
                            showCancelButton: true,
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Yes",
                            cancelButtonText: "No"
                        },
                             function(isConfirm){
                            if (isConfirm) {
                                trainerbatchindex.createNewBatch();
                            } else {
                                
                            }
                        });
                    } else{
                    	/*localStorage.setItem("addStudentCheckValue",addStudentCheck);
                    	if(localStorage.getItem("addStudentCheckValue") == 1){
                    		$("#manage_user").show();
                    	}*/
                        batchElement = Backbone.$("#batchtable").children("tbody").empty();
                        trainerbatchindex.generateBatchTable(data);
                        $("#batchtable").dataTable({
                            "ordering": false
                        });
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
   /** fething batch details by from - to date**/
        
        /*Fetch The batch From The Table*/
        batchTableCreateForDateSearch: function (fromdate,todate) {/*
        	$("#batchtablehide").hide();
       	 $("#fromandenddate").show();
           Backbone.$("#userTableInherit").empty().append(
              '<table id="usertable" class="table table-hover">' +
                     '<thead>' +
                                        '<tr >' +
                                        '<td><b>Batch Name</b></td>' +
                                        '<td><b>Start Date</b></td>' +
                                        '<td><b>End Date</b></td>' +
                                        '<td><b>Lunch Expenses</b></td>' +
                                        '<td><b>Miscellaneous Expenses</b></td>' +
                                        '<td><b>Travelling Expenses</b></td>' +
                                        '<td><b>Status</b></td>' +
                                        '<td><b>Edit</b></td>' +
                                        '<td><b>No of Students</b></td>' +
                                        '<td><b>Candidates Appeared</b></td>' +
                                        '<td><b>Candidates Passed</b></td>' +
                                        '<td><b>Candidates Failed</b></td>' +
                                        '<td><b>Passing %</b></td>' +
                                        '<td><b>Candidates Pending</b></td>' +
                                        '<td><b>Batch Report</b></td>' +
                                        '</tr>' +
                                    '</thead>' +
                                    '<tbody>' +
                                    '</tbody>' +

               '</table>');

           jQuery("#usertable").dataTable({
          
         
          
           "ajax": {
               "url": "tr-paginlistbatchbyfromdateandtodate?moduleId="+mid+"&empId="+localStorage.getItem("empId")+"&fromdate="+fromdate+"&todate="+todate,
               "dataSrc": ""
           },
           
            "columnDefs": [
            {
                   'targets': 1,
                   'searchable':false,
                   'orderable':false,
                   'render': function (data, type, full, meta){
                       return '<button class="btdetails" value="'+full.batchId+'"><a>'+full.batchName+'</a></button>';
                   }
               },
               {
                   'targets': -1,
                   'searchable':false,
                   'orderable':false,
                   'render': function (data, type, full, meta){
                       return '<a href="excelreportbatchwise?batchId='+data[i].batchId+'" target="_blank"> Download</a>';
                    }
                },
              
             

               {
                   'targets': 7,
                   'searchable':false,
                   'orderable':false,
                   'render': function (data, type, full, meta){
                       return '<button class="editbatch"><a> Edit </a></button>';
                   }
               },],
           "aoColumns" : [
            {
              "mDataProp": null,
              "sClass": "control center",
              "sDefaultContent": '<i class="fa fa-plus-circle" aria-hidden="true"></i>'
              
           },
                           {"mData": "" },
                           { "mData": "batchStartDate" },
                           { "mData": "batchEndDate"},
                           { "mData": "lunchExpenses" },
                           { "mData": "miselExpense" },
                           { "mData": "travellingExpense"},
                           { "mData": ""},
                           { "mData": "numStudent"},
                           { "mData": "candidateAppeared"},
                           { "mData": "candidatePassed"},
                           { "mData": "candidateFailed"},
                           { "mData": "passingPercentage"},
                           { "mData": "candidatePending"},
                           { "mData": ""}
                       ]
      
           });
 
           
          
       */

            var json
            $("#pageloading").show();
            $("#fromandenddate").show();
            $("#batchtablehide").hide();
            Backbone.ajax({
                dataType: "json",
                url: "tr-paginlistbatchbyfromdateandtodate?moduleId="+mid+"&empId="+localStorage.getItem("empId")+"&fromdate="+fromdate+"&todate="+todate,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {

                	/*localStorage.setItem("addStudentCheckValue",addStudentCheck);
                	if(localStorage.getItem("addStudentCheckValue") == 1){
                		$("#manage_user").show();
                	}*/
                    batchElementForDateSearch = Backbone.$("#userTableInherit").empty().append(
                    	'<table id="usertable" class="table table-hover">' +
	                            '<thead>' +
	                            '<tr >' +
	                            '<td><b>Batch Name</b></td>' +
	                            '<td><b>Start Date</b></td>' +
	                            '<td><b>End Date</b></td>' +
	                            '<td><b>Lunch Expenses</b></td>' +
	                            '<td><b>Miscellaneous Expenses</b></td>' +
	                            '<td><b>Travelling Expenses</b></td>' +
	                           /* '<td><b>Status</b></td>' +*/
	                            '<td><b>Edit</b></td>' +
	                            '<td><b>No of Students</b></td>' +
	                            '<td><b>Candidates Appeared</b></td>' +
	                            '<td><b>Candidates Passed</b></td>' +
	                            '<td><b>Candidates Failed</b></td>' +
	                            '<td><b>Passing %</b></td>' +
	                            '<td><b>Candidates Pending</b></td>' +
	                            '<td><b>Batch Report</b></td>' +
	                            '</tr>' +
	                        '</thead>' +
	                        '<tbody>' +
	                        '</tbody>' +

                    	'</table>');
                    trainerbatchindex.generateBatchTableByDateSearch(data);
                   $("#usertable").dataTable({
                        "ordering": false
                    });
                
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        
        },
        

    /*~~~~ GENERATE TABLE ELEMENTS OF BATCHES FROM JSON BATCHLIST~~~~*/
        generateBatchTable: function (data) {
            //get table id from jquery
            for (var i=0; i< data.length; i++){
                batchElement.append(
                    '<tr id="batch'+data[i].batchId+'">' +
                        '<td class="btdetails"><a>'+data[i].batchName+'</a></td>' +
                        '<td>'+data[i].batchStartDate+'</td>' +
                        '<td>'+data[i].batchEndDate+'</td>' +
                        '<td>'+data[i].lunchExpenses+'</td>' +
                        '<td>'+data[i].miselExpense+'</td>' +
                        '<td>'+data[i].travellingExpense+'</td>' +

                        /*'<td class="batchStatus" value='+data[i].active+'></td>' +*/
                        '<td><div class="editbatch"><a> Edit </a></div></td>' +
                        '<td>'+data[i].numStudent+'</td>' +
                        '<td>'+data[i].candidateAppeared+'</td>' +
                        '<td>'+data[i].candidatePassed+'</td>' +
                        '<td>'+data[i].candidateFailed+'</td>' +
                        '<td>'+data[i].passingPercentage+'</td>' +
                        '<td>'+data[i].candidatePending+'</td>' +
                        '<td><a href="excelreportbatchwise?batchId='+data[i].batchId+'" target="_blank"> Download</a></td>' +
                    '</tr>');
               /* if(data[i].active == 1){
                    $("#batch"+data[i].batchId+"").children(".batchStatus").text("Enable");
                } else {
                    $("#batch"+data[i].batchId+"").children(".batchStatus").text("Disable");
                }*/
            }
            
        },
        
        generateBatchTableByDateSearch: function (data) {
            //get table id from jquery
            for (var i=0; i< data.length; i++){
            	batchElementForDateSearch.children("#usertable").children("tbody").append(
                    '<tr id="batch'+data[i].batchId+'">' +
                        '<td class="btdetails"><a>'+data[i].batchName+'</a></td>' +
                        '<td>'+data[i].batchStartDate+'</td>' +
                        '<td>'+data[i].batchEndDate+'</td>' +
                        '<td>'+data[i].lunchExpenses+'</td>' +
                        '<td>'+data[i].miselExpense+'</td>' +
                        '<td>'+data[i].travellingExpense+'</td>' +

                        /*'<td class="batchStatus" value='+data[i].active+'></td>' +*/
                        '<td><div class="editbatch"><a> Edit </a></div></td>' +
                        '<td>'+data[i].numStudent+'</td>' +
                        '<td>'+data[i].candidateAppeared+'</td>' +
                        '<td>'+data[i].candidatePassed+'</td>' +
                        '<td>'+data[i].candidateFailed+'</td>' +
                        '<td>'+data[i].passingPercentage+'</td>' +
                        '<td>'+data[i].candidatePending+'</td>' +
                        '<td><a href="excelreportbatchwise?batchId='+data[i].batchId+'" target="_blank"> Download</a></td>' +
                    '</tr>');
               /* if(data[i].active == 1){
                    $("#batch"+data[i].batchId+"").children(".batchStatus").text("Enable");
                } else {
                    $("#batch"+data[i].batchId+"").children(".batchStatus").text("Disable");
                }*/
            }
            
        },
        
    /*~~~~ NEW BATCH CREATION CALL MODAL FOR VALUES FOR TAKING INPUT ~~~~~*/
        createNewBatch: function() {
            
            $(".errorClss").hide();
            $(".batchsubmit").css("display","inline-block");
            $(".editbatchsubmit").hide();
            $("#createnewbatch").modal({ keyboard: true });
            $("#createnewbatch").modal('show');
            //$("#yes-button label .batchcheck").prop('checked', true);
            $("#batchvalidfrom").val("");
            $("#batchvalidto").val("");
            $("#batchname").val("");
            $("#miscellaneousexpenses").val("");
            $("#travellingexpenses").val("");
            $("#lunchexpenses").val("");
            $("#aprovedmiscellaneous").val("");
            $("#aprovedtravelling").val("");
            $("#aprovedlunch").val("");
             $('#rulecheck').prop('checked', false);
            $("#approvedtrexpens").hide();
            $("#approvedluexpens").hide();
            $("#approvedmiexpens").hide(); 
        },
        
    /* Updating Deatils Fetch from Table */
        editBatchDetails: function(e){

            
            //  var bhid = e.currentTarget.parentElement.parentElement.id.slice(5);
             // var batchStatus = $('#'+e.currentTarget.parentElement.parentElement.id+'').children('.batchStatus').attr('value');
              // var batchname = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[0]).text();
              // var starttime = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[1]).text();
              // var endtime = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[2]).text();
              var noOfStudents=$($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[3]).text();
              var candidatesappeared=$($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[4]).text();
              var candidatespassed=$($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[5]).text();
              var candidatesfailed=$($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[6]).text();
              var passingpercentage=$($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[7]).text();
              var candidatespending=$($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[8]).text();
         

              var bhid = e.currentTarget.parentElement.parentElement.id.slice(5);
              //var batchStatus = $('#'+e.currentTarget.parentElement.parentElement.id+'').children('.batchStatus').attr('value');
                $(".batchsubmit").hide();
           
              $(".editbatchsubmit").css("display","inline-block");
              $('.editbatchsubmit').attr('value',bhid);
   
              $("#createnewbatch").modal({ keyboard: true });
              $("#createnewbatch").modal('show');


                {
                     
                       
                      $("#pageloading").show();
                      Backbone.ajax({
                           dataType: "json",
                          url: "getbatchdet?batchId="+bhid,
                          data: "",
                          type: "GET",
                          beforeSend: function(xhr) {
                              xhr.setRequestHeader(header, token);
                               xhr.setRequestHeader("Accept", "application/json");
                              xhr.setRequestHeader("Content-Type", "application/json");
                          },
                          success: function (data, textStatus, jqXHR) {
                           
                                 
                                   $("#batchvalidfrom").val(data.validFrom);
                                   $("#batchvalidto").val(data.validTo);
                                   $("#batchname").val(data.shortDesc);
                                   $("#aprovedmiscellaneous").val(data.appMissallaneousExp);
                                   $("#aprovedtravelling").val(data.appTravelExp);
                                   $("#aprovedlunch").val(data.appLunchExp);
                                   $("#miscellaneousexpenses").val(data.missallaneousExp);
                                   $("#travellingexpenses").val(data.travelExp);
                                   $("#lunchexpenses").val(data.lunchExp);

                                      if(data.expCheck==1)
                                   {
                                      $('#rulecheck').prop('checked', true);
                                      trainerbatchindex.expensesView();
                                   }
                                   if(data.expCheck==0)
                                   {
                                      $('#rulecheck').prop('checked', false);
                                      trainerbatchindex.expensesView();
                                   }

                              /*   '<div class="list-group-item">'+
                                     '<a  class="templateclick" value="'+data[i].transTempId+'">Click Here</a> to '+data[i].tranDesc+' with Allowable Status'+data[i].allowableStatus+
                                 '</div>'
                           */
                        
                          },
                          error: function (res, ioArgs) {
                              if (res.status === 440) {
                                  window.location.reload();
                              }
                          },
                            complete: function() {
                             $("#pageloading").hide();
                           },
                            
                      });
                  }
          },
    
    /* CALL ROUTE FOR SEEING BATCH DETAILS SENDING BATCH ID*/
        batchDetails: function(e) {
            var btName = $($("#"+e.currentTarget.parentElement.id+"").children()[0]).children("a").text();
            var bjson;
            bjson={"key":"batchName","value":btName};
            var batchId = (e.currentTarget.parentElement.id).slice(5);
            trainerbatchindex.sendBatchNameForFurtureUse( bjson, batchId );
        },
        
        sendBatchNameForFurtureUse: function( backenddate, batchId ){
            $(".errorClss").hide();
            $("#pageloading").show();
            Backbone.ajax({
                url: "addvariable",
                data: JSON.stringify(backenddate),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        $(".errorBatchName").show();
                    } else if(data == 1) {
                        landingroutes.navigate('trainerbatch/'+batchId+'/stdnt/'+mid+'/0', {trigger: true});
                    } else {
                        sweetAlert("Please contact IT Admin");
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
    /*CREATING A NEW BATCH DETAILS SENDING USING createbatch JSON */
        newBatchSubmit: function(e) {
            
            var trainernewenble=0;
               $(".errorClss").hide();
               if ($('#batchname').val() == 0) {
                   $(".errorBatchNameReq").show();
               } else if ($('#batchvalidfrom').val() == 0) {
                   $(".errorBatchStar").show();
               } else if ($('#batchvalidto').val() == 0) {
                   $(".errorBatchEnd").show();
               }  else if ($('#questionbox').val() == 0) {
                   $(".errorQuestionReq").show();
               }else {
                   var baEndDate = parseInt($('#batchvalidto').val().replace(/\D+/g, ''));
                   var baStartDate = parseInt($("#batchvalidfrom").val().replace(/\D+/g, ''));
               
                   if(baEndDate < baStartDate){
                       $(".errortrEndDate").show();
                   } else {

                           if( $('#rulecheck').is(':checked'))
                            {
                                trainernewenble=1;

                            }
                            else{
                               trainernewenble=0;
                            }
                       
                     
                       
                       var json;
                       try{
                           json={"validFrom":$('#batchvalidfrom').val(),"validTo":$('#batchvalidto').val(),"shortDesc":$('#batchname').val(),"moduleId":mid,"active":1,"expCheck":trainernewenble,"travelExp":$("#travellingexpenses").val(),
                           "lunchExp":$("#lunchexpenses").val(), "missallaneousExp":$("#miscellaneousexpenses").val(),"appTravelExp":$("#aprovedtravelling").val(),"appLunchExp":$("#aprovedlunch").val(),"appMissallaneousExp":$("#aprovedmiscellaneous").val(),"questionbox":$('#questionbox').val()};
                       }catch(e){}
                       $("#pageloading").show();
                       Backbone.ajax({
                           url: "tr-createbatch",
                           data: JSON.stringify(json),
                           type: "POST",
                           beforeSend: function(xhr) {
                               xhr.setRequestHeader(header, token);
                               xhr.setRequestHeader("Accept", "application/json");
                               xhr.setRequestHeader("Content-Type", "application/json");
                           },
                           success: function(data) {
                               if(data == 1){
                                   $("#createnewbatch").modal('hide');
                                   $("#createnewbatch").on('hidden.bs.modal', function(){
                                       trainerbatchindex.render(mid);
                                   });
                               } else if(data == 2){
                                   $(".errorOnCreSub").show();
                               } else {
                                   alert("Contact IT team");
                               }
                           },
                           error:function(res,ioArgs){
                               if(res.status == 403){
                                   window.location.reload();
                               }
                           },
                   complete: function() {
                       $("#pageloading").hide();
                   }
                       });
                   }
               }
           },
        
    /*EDITING A BATCH DETAILS SENDING USING editbatch JSON */
           editBatchSubmit: function(e) {

               var trainereditenable;
               
                $(".errorClss").hide();
                if ($('#batchname').val() == 0) {
                    $(".errorBatchNameReq").show();
                } else if ($('#batchvalidfrom').val() == 0) {
                    $(".errorBatchStar").show();
                } else if ($('#batchvalidto').val() == 0) {
                    $(".errorBatchEnd").show();
                } else {
                    var baEndDate = parseInt($('#batchvalidto').val().replace(/\D+/g, ''));
                    var baStartDate = parseInt($("#batchvalidfrom").val().replace(/\D+/g, ''));
                
                    if(baEndDate < baStartDate){
                        $(".errortrEndDate").show();
                    } else {
                                  
                            if( $('#rulecheck').is(':checked'))
                             {
                                 trainereditenable=1;

                             }
                             else{
                                trainereditenable=0;
                             }
                        
                        var json;
                        try{
                             json={"batchId":$('.editbatchsubmit').attr('value'),"validFrom":$('#batchvalidfrom').val(),"validTo":$('#batchvalidto').val(),shortDesc:$('#batchname').val(),"moduleId":mid,"active":1,"expCheck":trainereditenable,"travelExp":$("#travellingexpenses").val(),
                            "lunchExp":$("#lunchexpenses").val(), "missallaneousExp":$("#miscellaneousexpenses").val(),"appTravelExp":$("#aprovedtravelling").val(),"appLunchExp":$("#aprovedlunch").val(),"appMissallaneousExp":$("#aprovedmiscellaneous").val()};
                        }catch(e){}
                        $("#pageloading").show();
                        Backbone.ajax({
                            url: "tr-editbatch",
                            data: JSON.stringify(json),
                            type: "POST",
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader(header, token);
                                xhr.setRequestHeader("Accept", "application/json");
                                xhr.setRequestHeader("Content-Type", "application/json");
                            },
                            success: function(data) {
                                
                                if(data == 1){
                                    $("#createnewbatch").modal('hide');
                                    $("#createnewbatch").on('hidden.bs.modal', function(){
                                        trainerbatchindex.render(mid);
                                    });
                                } else if(data == 2){
                                    $(".errorOnCreSub").show();
                                } else {
                                    alert("Contact IT team");
                                }

                            },
                            error:function(res,ioArgs){
                                if(res.status == 403){
                                    window.location.reload();
                                }
                            },
                    complete: function() {
                        $("#pageloading").hide();
                    }
                        });
                    }
                }
            },
            
            expensesView:function(e){
           	 if($('#rulecheck').is(':checked')){
           	 
           	$("#approvedtrexpens").show();
           	$("#approvedluexpens").show();
           	$("#approvedmiexpens").show();
           	}
           	else if(!$('#rulecheck').is(':checked')){

           	  $("#approvedtrexpens").hide();
           	$("#approvedluexpens").hide();
           	$("#approvedmiexpens").hide();  
           	}
           	},
           
            getsearchdate:function(){
            	$( ".datepicker" ).datepicker({
                    dateFormat: "yy-mm-dd"
                });
            },
            
            getselecteddate:function(){
            	flag=0;
            	/*var date1=$('input[type=radio]').val();
            	if(date1 != "on")
            	$('input[type=search]').val(date1).keyup();*/
            	var fromdate=$("#fromdate").val();
            	var todate=$("#todate").val();
            	if(fromdate !=""){
            		$(".errorFromDate").hide();
            	}
            	else if(fromdate == null | fromdate ==""){
            		$(".errorFromDate").show();
            	}
            	
            	if(fromdate != "" & todate != ""){
            		this.batchTableCreateForDateSearch(fromdate,todate);
            	}
            	
            },
            
        
    /*CLOSE ALERT BOX*/
        clickAlertOk: function() {
            Alert.closeModal();
        },
    /* DATE PICKER CALLING*/
        dateValidation: function() {
            $("#batchvalidfrom").datepicker({
                dateFormat: "yy-mm-dd"
            });
            $('#batchvalidto').datepicker({
                dateFormat: "yy-mm-dd"
            });
        },
        
        trainerDetails: function (modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="training_done" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Training Details">&nbsp;&nbsp;<i class="fa fa-list-ol fa-fw"></i>' +
                                '<span class="content-hide">Dashboard</span>' +
                            '</li>' +
                            '<li id="trainerbatchmanagement" class="maind " data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="attend_manage" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="manage_user" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                        	'<span class="content-hide">User Management</span>' +
                        	'</li>' +
                        	'<li id="hierarchyview" class="maind" data-toggle="tooltip tab" data-original-title="Hierarchy View">&nbsp;&nbsp;<i class="fa fa-camera-retro fa-fw"></i>' +
                            '<span class="content-hide">Hierarchy View</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header"> Training List <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div class="row">' +
                '<div class="form-group col-xs-5">' +
                    '<label for="trainingFrom" class="control-label">Training start</label>' +
                    '<input id="trainingFrom" type="text" name="batch-validfrom" class="form-control" readonly>' +
                '</div>' +
                '<div class="form-group col-xs-5">' +
                    '<label for="trainingTill" class="control-label">Training end</label>' +
                    '<input id="trainingTill" type="text" name="batch-validto" class="form-control" readonly>' +
                '</div>' +
                '<div class="form-group col-xs-2">' +
                    '<label for="trainingTill" class="control-label">Filter </label><br>' +
                    '<button id="searchTraining" class="btn btn-default">Apply</button>' +
                '</div>' +
            '</div>' +
            '<div class="row" style="text-align : center;">' +
                '<div class="col-xs-3">' +
                    '<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<label for="noOfBatches" class="controle-label">No of Batches</label>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<div id="dash-NoOfBatch" class="circle-cls" tabindex="1">0</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="col-xs-3">' +
                    '<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<label for="noOfSession" class="controle-label">No of Sessions(classes)</label>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<div id="dash-NoOfSession" class="circle-cls" tabindex="1">0</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="col-xs-3">' +
                    '<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<label for="noOfStudents" class="controle-label">No of Students</label>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<div id="dash-NoOfStudents" class="circle-cls" tabindex="1">0</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            
            '<div class="row" style="text-align : center;">' +
            '<div class="col-xs-3">' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<label for="noOfStudentsPassed" class="controle-label">No of Students Passed</label>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div id="dash-NoOfStudentsPassed" class="circle-cls" tabindex="1">0</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-xs-3">' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<label for="noOfStudentsFailed" class="controle-label">NO of Students Failed</label>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div id="dash-NOOfStudentsFailed" class="circle-cls" tabindex="1">0</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="col-xs-3">' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<label for="noOfStudentsAppeared" class="controle-label">No of Students Appeared</label>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div id="dash-NoOfStudentsAppeared" class="circle-cls" tabindex="1">0</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
        
            '<div class="modal fade" id="dashBoardDetailsModal" tabindex="-1" role="dialog" aria-labelledby="dashBoardDetailsModal" aria-hidden="true" data-backdrop="static" style="margin-top:1%">' +
                '<div class="modal-dialog" style="width: 70%;">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<div class="close" data-dismiss="modal">x</div> '+
                            '<h4 class="modal-title"></h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="row">' +
                                '<div class="panel-body">' +
                                    '<div class="table-responsive">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<button class="btn btn-primary" align="center" data-dismiss="modal" tabindex="1"> Ok </button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
            );
            $("#trainingFrom").datepicker({
                dateFormat: "yy-mm-dd"
            });
            $('#trainingTill').datepicker({
                dateFormat: "yy-mm-dd"
            });
            
            /*if(localStorage.getItem("addStudentCheckValue") == 1){
        		$("#manage_user").show();
        	}*/
            
            trainerbatchindex.trainingDetailsList();
        },
        
        openDashBoardDetailsModal: function(e) {
            var dshDts = e.currentTarget.id;
            $("#dashBoardDetailsModal").modal("show");
            $("#dashBoardDetailsModal > div > div > div.modal-body > div > div > div").empty();
            var tdf;
            var tdt;
            if($("#trainingFrom").val() == 0){
                tdf = 0;
            } else {
                tdf = $("#trainingFrom").val();
            }

            if($("#trainingTill").val() == 0){
                tdt = 0;
            } else {
                tdt = $("#trainingTill").val();
            }
            
            if( dshDts == "dash-NoOfBatch"){
                $("#dashBoardDetailsModal > div > div > div.modal-header > h4").text("Batches");
                $("#dashBoardDetailsModal > div > div > div.modal-body > div > div > div").empty().append(
                    '<table id="dashBoardDetails" class="table table-hover display">' +
                        '<thead>' +
                            '<tr>'+
                                '<td><b>Batch Name</b></td>' +
                                '<td><b>Start</b></td>' +
                                '<td><b>End</b></td>' +
                                '<td><b>Session Done</b></td>' +
                                '<td><b>Students</b></td>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );

                $("#pageloading").show();
                Backbone.ajax({
                    dataType:"json",
                    url:"tr-getbatchwisetrainerstatus?startDate="+tdf+"&endDate="+tdt,
                    data:"",
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                    },
                    success: function(data, textStatus, jqXHR){
                        if(data == 0){
                            $("#dashBoardDetails > tbody").append('<h4>No Batches Found</h4>');
                            $("#dashBoardDetails > thead").empty();
                        } else {
                            for( var i = 0; i < data.length; i++ ){
                                $("#dashBoardDetails > tbody").append(
                                    '<tr>'+
                                        '<td>'+data[i].shortDesc+'</td>' +
                                        '<td>'+data[i].validFrom+'</td>' +
                                        '<td>'+data[i].validTo+'</td>' +
                                        '<td>'+data[i].sessionCount+'</td>' +
                                        '<td>'+data[i].traineesCount+'</td>' +
                                    '</tr>'
                                );
                            }
                            $("#dashBoardDetails").dataTable({
                                "ordering": false
                            });
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        $("#dashBoardDetails > tbody").append('<h4>No Batches Found</h4>');
                        $("#dashBoardDetails > thead").empty();
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            } else if( dshDts == "dash-NoOfSession"){
                $("#dashBoardDetailsModal > div > div > div.modal-header > h4").text("Sessions");
                $("#dashBoardDetailsModal > div > div > div.modal-body > div > div > div").empty().append(
                    '<table id="dashBoardDetails" class="table table-hover display">' +
                        '<thead>' +
                             '<tr>' +
                                '<td><b>Sl No</b></td>' +
                                '<td><b>Batch Name</b></td>' +
                                '<td><b>Date</b></td>' +
                                '<td><b>Slot Name</b></td>' +
                                '<td><b>Students Attended</b></td>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
                
                $("#pageloading").show();
                Backbone.ajax({
                    dataType:"json",
                    url:"tr-getsessionwisetrainerstatus?startDate="+tdf+"&endDate="+tdt,
                    data:"",
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                    },
                    success: function(data, textStatus, jqXHR){
                        if(data == 0){
                            $("#dashBoardDetails > tbody").append('<h4>No Session Found</h4>');
                            $("#dashBoardDetails > thead").empty();
                        } else {
                            for( var i = 0; i < data.length; i++ ){
                                $("#dashBoardDetails > tbody").append(
                                    '<tr>'+
                                        '<td>'+(i+1)+'</td>' +
                                        '<td>'+data[i].shortDesc+'</td>' +
                                        '<td>'+data[i].sessionDate+'</td>' +
                                        '<td>'+data[i].slot+'</td>' +
                                        '<td>'+data[i].studentCount+'/'+data[i].totalStudentCount+'</td>' +
                                    '</tr>'
                                );
                            }
                            $("#dashBoardDetails").dataTable({
                                "ordering": false
                            });
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        $("#dashBoardDetails > tbody").append('<h4>No Session Found</h4>');
                        $("#dashBoardDetails > thead").empty();
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            } else if( dshDts == "dash-NoOfStudents"){
                $("#dashBoardDetailsModal > div > div > div.modal-header > h4").text("Students");
                $("#dashBoardDetailsModal > div > div > div.modal-body > div > div > div").append(
                    '<table id="dashBoardDetails" class="table table-hover display">' +
                        '<thead>' +
                            '<tr>' +
                                '<td><b>Student Name</b></td>' +
                                '<td><b>Batch Name</b></td>' +
                                '<td><b>Attendance</b></td>' +
                                '<td><b>Assessment</b></td>' +
                                '<td><b>Average</b></td>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
                
                $("#pageloading").show();
                Backbone.ajax({
                    dataType:"json",
                    url:"tr-getstudentwisetrainerstatus?startDate="+tdf+"&endDate="+tdt,
                    data:"",
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                    },
                    success: function(data, textStatus, jqXHR){
                        if(data == 0){
                            $("#dashBoardDetails > tbody").append('<h4>No Students Found</h4>');
                            $("#dashBoardDetails > thead").empty();
                        } else {
                            for( var i = 0; i < data.length; i++ ){
                                $("#dashBoardDetails > tbody").append(
                                    '<tr>'+
                                        '<td>'+data[i].studentName+'</td>' +
                                        '<td>'+data[i].batchName+'</td>' +
                                        '<td>'+data[i].attendendSessions+'/'+data[i].maximumSessions+'</td>' +
                                        '<td>'+data[i].attemptsDone+'/'+data[i].maxAttempts+'</td>' +
                                        '<td>'+data[i].correctAnswerCount+'/'+data[i].maximumQuesCount+'</td>' +
                                    '</tr>'
                                );
                            }
                            $("#dashBoardDetails").dataTable({
                                "ordering": false
                            });
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        $("#dashBoardDetails > tbody").append('<h4>No Students Found</h4>');
                        $("#dashBoardDetails > thead").empty();
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }else if( dshDts == "dash-NoOfStudentsPassed"){
                $("#dashBoardDetailsModal > div > div > div.modal-header > h4").text("Students");
                $("#dashBoardDetailsModal > div > div > div.modal-body > div > div > div").append(
                    '<table id="dashBoardDetails" class="table table-hover display">' +
                        '<thead>' +
                            '<tr>' +
                                '<td><b>Student Name</b></td>' +
                                '<td><b>Batch Name</b></td>' +
                                '<td><b>Attendance</b></td>' +
                                '<td><b>Assessment</b></td>' +
                                '<td><b>Average</b></td>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
                
                $("#pageloading").show();
                Backbone.ajax({
                    dataType:"json",
                    url:"tr-getstatusofnumofstudentspassed?startDate="+tdf+"&endDate="+tdt,
                    data:"",
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                    },
                    success: function(data, textStatus, jqXHR){
                        if(data == 0){
                            $("#dashBoardDetails > tbody").append('<h4>No Students Found</h4>');
                            $("#dashBoardDetails > thead").empty();
                        } else {
                            for( var i = 0; i < data.length; i++ ){
                                $("#dashBoardDetails > tbody").append(
                                    '<tr>'+
                                        '<td>'+data[i].studentName+'</td>' +
                                        '<td>'+data[i].batchName+'</td>' +
                                        '<td>'+data[i].attendendSessions+'/'+data[i].maximumSessions+'</td>' +
                                        '<td>'+data[i].attemptsDone+'/'+data[i].maxAttempts+'</td>' +
                                        '<td>'+data[i].correctAnswerCount+'/'+data[i].maximumQuesCount+'</td>' +
                                    '</tr>'
                                );
                            }
                            $("#dashBoardDetails").dataTable({
                                "ordering": false
                            });
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        $("#dashBoardDetails > tbody").append('<h4>No Students Found</h4>');
                        $("#dashBoardDetails > thead").empty();
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }else if( dshDts == "dash-NOOfStudentsFailed"){
                $("#dashBoardDetailsModal > div > div > div.modal-header > h4").text("Students");
                $("#dashBoardDetailsModal > div > div > div.modal-body > div > div > div").append(
                    '<table id="dashBoardDetails" class="table table-hover display">' +
                        '<thead>' +
                            '<tr>' +
                                '<td><b>Student Name</b></td>' +
                                '<td><b>Batch Name</b></td>' +
                                '<td><b>Attendance</b></td>' +
                                '<td><b>Assessment</b></td>' +
                                '<td><b>Average</b></td>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
                
                $("#pageloading").show();
                Backbone.ajax({
                    dataType:"json",
                    url:"tr-getstatusofnumstudentsfailed?startDate="+tdf+"&endDate="+tdt,
                    data:"",
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                    },
                    success: function(data, textStatus, jqXHR){
                        if(data == 0){
                            $("#dashBoardDetails > tbody").append('<h4>No Students Found</h4>');
                            $("#dashBoardDetails > thead").empty();
                        } else {
                            for( var i = 0; i < data.length; i++ ){
                                $("#dashBoardDetails > tbody").append(
                                    '<tr>'+
                                        '<td>'+data[i].studentName+'</td>' +
                                        '<td>'+data[i].batchName+'</td>' +
                                        '<td>'+data[i].attendendSessions+'/'+data[i].maximumSessions+'</td>' +
                                        '<td>'+data[i].attemptsDone+'/'+data[i].maxAttempts+'</td>' +
                                        '<td>'+data[i].correctAnswerCount+'/'+data[i].maximumQuesCount+'</td>' +
                                    '</tr>'
                                );
                            }
                            $("#dashBoardDetails").dataTable({
                                "ordering": false
                            });
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        $("#dashBoardDetails > tbody").append('<h4>No Students Found</h4>');
                        $("#dashBoardDetails > thead").empty();
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }else if( dshDts == "dash-NoOfStudentsAppeared"){
                $("#dashBoardDetailsModal > div > div > div.modal-header > h4").text("Students");
                $("#dashBoardDetailsModal > div > div > div.modal-body > div > div > div").append(
                    '<table id="dashBoardDetails" class="table table-hover display">' +
                        '<thead>' +
                            '<tr>' +
                                '<td><b>Student Name</b></td>' +
                                '<td><b>Batch Name</b></td>' +
                                '<td><b>Attendance</b></td>' +
                                '<td><b>Assessment</b></td>' +
                                '<td><b>Average</b></td>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
                
                $("#pageloading").show();
                Backbone.ajax({
                    dataType:"json",
                    url:"tr-getstatusofnumofstudentsappeared?startDate="+tdf+"&endDate="+tdt,
                    data:"",
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                    },
                    success: function(data, textStatus, jqXHR){
                        if(data == 0){
                            $("#dashBoardDetails > tbody").append('<h4>No Students Found</h4>');
                            $("#dashBoardDetails > thead").empty();
                        } else {
                            for( var i = 0; i < data.length; i++ ){
                                $("#dashBoardDetails > tbody").append(
                                    '<tr>'+
                                        '<td>'+data[i].studentName+'</td>' +
                                        '<td>'+data[i].batchName+'</td>' +
                                        '<td>'+data[i].attendendSessions+'/'+data[i].maximumSessions+'</td>' +
                                        '<td>'+data[i].attemptsDone+'/'+data[i].maxAttempts+'</td>' +
                                        '<td>'+data[i].correctAnswerCount+'/'+data[i].maximumQuesCount+'</td>' +
                                    '</tr>'
                                );
                            }
                            $("#dashBoardDetails").dataTable({
                                "ordering": false
                            });
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        $("#dashBoardDetails > tbody").append('<h4>No Students Found</h4>');
                        $("#dashBoardDetails > thead").empty();
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
        trainingDetailsList: function() {
            var tdf;
            if($("#trainingFrom").val() == 0){
                tdf = 0;
            } else {
                tdf = $("#trainingFrom").val();
            }
            
            var tdt;
            if($("#trainingTill").val() == 0){
                tdt = 0;
            } else {
                tdt = $("#trainingTill").val();
            }
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"tr-gettrainerstatus?startDate="+tdf+"&endDate="+tdt,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    if(data == 0){
                        $("#dash-NoOfBatch").text(0);
                        $("#dash-NoOfSession").text(0);
                        $("#dash-NoOfStudents").text(0);
                        $("#dash-NoOfStudentsPassed").text(0);
                        $("#dash-NOOfStudentsFailed").text(0);
                        $("#dash-NoOfStudentsAppeared").text(0);
                        
                    } else {
                        $("#dash-NoOfBatch").text(data.batchCount);
                        $("#dash-NoOfSession").text(data.sessionCount);
                        $("#dash-NoOfStudents").text(data.traineeCount);
                        $("#dash-NoOfStudentsPassed").text(data.studPassCount);
                        $("#dash-NOOfStudentsFailed").text(data.studFailedCount);
                        $("#dash-NoOfStudentsAppeared").text(data.studAppearedCount)
                    }
                },
                error: function (res, ioArgs) {
                    
                    $("#dash-NoOfBatch").text(0);
                    $("#dash-NoOfSession").text(0);
                    $("#dash-NoOfStudents").text(0);
                    $("#dash-NoOfStudentsPassed").text(0);
                    $("#dash-NOOfStudentsFailed").text(0);
                    $("#dash-NoOfStudentsAppeared").text(0);
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    });
    
    /** BATCH DETAILS VIEW  **/
    trainer_batch_manage.Views.batchslist = Backbone.View.extend({
         
        events: {
            'click #studentdetailsid':'studentTable',
            'click .addstudent': 'addNewStudentToBatch',
            'click .attendancelist': 'showAttendence',
            'click .add_student': 'showStudentList',
            'click .studentcheck': 'addStudentToBatch',
            'click #submitaddtobatch': 'addUserToBatch',
            'click .removeStudent': 'removeClickedStudent',
            'click .revStdAll-check': 'selectAllStudent',
            'click .revStd-check': 'unselectStudent',
            'click .studentcheck': 'unselectStudent',
            'click .selectAllStudent': 'selectAllStudent',
            'focus .pdeDatePic': 'datePdeValidation',
            'click #tr-submitPerDateToJoinTable': 'addPreDateByTrainer',
        },
        
        
        initialize: function () {
            _.bindAll(this,'render');
        },
        
        render: function (b, modulid) {
            mid = modulid
            baid = b;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="training_done" class="maind" data-toggle="tooltip tab" data-original-title="Training Details">&nbsp;&nbsp;<i class="fa fa-list-ol fa-fw"></i>' +
                                '<span class="content-hide">Dashboard</span>' +
                            '</li>' +
                            '<li id="trainerbatchmanagement" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="attend_manage" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="manage_user" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                        	'<span class="content-hide">User Management</span>' +
                        	'</li>' +
                        	'<li id="hierarchyview" class="maind" data-toggle="tooltip tab" data-original-title="Hierarchy View">&nbsp;&nbsp;<i class="fa fa-camera-retro fa-fw"></i>' +
                            '<span class="content-hide">Hierarchy View</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header"> <span class="bat_Detail"></span> <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div id="student_wrapper">' +
                '<div class="row">' +
                '<ul class="nav nav-tabs">'+
                    '<li class="studentclass" data-toggle="tab">'+
                        '<div id="studentdetailsid"> Student Details '+
                            '<span></span>'+
                        '</div>'+
                    '</li>'+
                '</ul>'+
                '<br><div id="tablesbatchs" class="tab-content">'+
                '</div>' +
            '</div>' +
            '<div class="modal fade" id="addtobatch" tabindex="-1" role="dialog" aria-labelledby="addtobatch" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog" style="width: 70%">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
	                                '<div class="table-responsive" id="addContentToBatch">' +
	                                '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="errorClss errorUserSelect pull-left"> Please Select Atleast One Either Cancel</div>' +
                            '<div class="errorClss errorUserSubmit pull-left"> Submitting Error, Try Later..! </div>' +
                            '<div class="btn btn-default" align="center" data-dismiss="modal"> Cancel </div>' +
                            '<div id="submitaddtobatch" class="btn btn-default disabal"> Submit </div>' +
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'
            );
           /* if(localStorage.getItem("addStudentCheckValue") == 1){
        		$("#manage_user").show();
        	}*/
            this.addBatchHeading();
        },
        
        
    /* Select Unselect All */    
        selectAllStudent: function(e) {
            var allCheck = e.currentTarget.className;
            var childCheck;
            
            if(allCheck == "revStdAll-check"){
                childCheck = "trainercheck";
            } else if(allCheck == "selectAllStudent") {
                childCheck = "addtrainercheck";
            }
            if($('.'+allCheck+'').is(':checked')){
                $("."+childCheck+"").prop('checked',true);
            } else if(!$('.'+allCheck+'').is(':checked')) {
                $("."+childCheck+"").prop('checked',false);
            }
            
            
            
            if($('.'+allCheck).is(':checked')){
                $("."+childCheck).prop('checked',true);
                var aa= $("."+childCheck);
                for(var i=0;i<aa.length;i++){
                	var id=$(aa[i]).attr("id").slice(6);
                	 var index = $.inArray("emp_row_"+id, selected);
                     if ( index === -1 ) {
                         selected.push("emp_row_"+ id );
                         $("#emp_row_"+id).addClass('row_selected');
                        // $("#check_"+id.split("_")[2]).prop('checked',true);
                     }
                }
            } else if(!$('.'+allCheck).is(':checked')) {
                $("."+childCheck).prop('checked',false);
                var aa= $("."+childCheck);
                for(var i=0;i<aa.length;i++){
                	var id=$(aa[i]).attr("id").slice(6);
                	 var index = $.inArray("emp_row_"+id, selected);
                     if ( index != -1 ) {
                        // selected.push( id );
                         selected.splice( index, 1 );
                         $("#emp_row_"+id).removeClass('row_selected');
                        // $("#check_"+id.split("_")[2]).prop('checked',true);
                     }
                }
            }
        },
        
    /* Unselect Student */
        unselectStudent: function(e) {
            var childCheck = e.currentTarget.className;
            var allCheck;
            if(childCheck == "revStd-check"){
                allCheck = "revStdAll-check";
            } else if(childCheck == "studentcheck") {
                allCheck = "selectAllStudent";
            }
            
            if(!e.currentTarget.checked){
                if($('.'+allCheck+'').is(':checked')){
                    $("."+allCheck+"").prop('checked',false);
                }
            } else if(e.currentTarget.checked){
                var tempcount = 0;
                var userChecked = $("."+childCheck+"");
                for (var i = 0, length = userChecked.length; i < length; i++) {
                    if ($(userChecked[i]).is(':checked')) { 
                        tempcount++;
                    }
                }
                
                if(tempcount == $("."+childCheck+"").length){
                    $("."+allCheck+"").prop('checked',true);
                }
            }
        },
        
        addBatchHeading: function() {
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getvariable?key=batchName",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data){
                    $(".bat_Detail").text(data);
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
    /*TRINEE TABLE LOAD ROUTES*/
        trainerTable: function() {
            landingroutes.navigate('trainerbatch/'+baid+'/trines/'+mid+'/0', {trigger: true});
        },
        
    /*STUDENT TABLE IN BATCH */
        loadStudentTable: function() {
        	preDateArray = 0;
            Backbone.$("#tablesbatchs").empty().append(
                '<div class="tab-pane fade in active">' +
                    '<div class="col-xs-6" style="margin-bottom: 20px;">' +
                        '<button class="addstudent btn btn-default" type="button">' +
                            '<i class="glyphicon glyphicon-user"></i>  &nbsp;Add Student(s)' +
                        '</button>' +
                    '</div>' +
                    /*'<div id="tr-submitPerDateToJoinTable" class="btn btn-default " style="margin-left: 100px"> Submit </div>' +*/
                    '<div class="col-xs-6" style="margin-bottom: 20px;margin-left:-200px;">' +
                    
                    '<div class="col-xs-3" top:-8px;" id="tr-student_tcc"> ' +
                    '<form method="post" enctype="multipart/form-data">'+
                        '<div class="btn btn-outline btn-primary btn-file">'+
                            '<i class="fa fa-upload">'+
                            '</i> &nbsp;Upload <input name="filename" type="file" class="pdefiletrainer" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="left: 16px;width: 90px;">'+
                        '</div>'+
                    '</form>'+
                    '<div class="col-xs-9" top:-8px;>Students TCC and Preferred Date of Exam'+
                '</div>'+
                '</div>'+
                '<div  class="col-xs-12" top:-8px;>'+
                    '<a href="batchwisestudtcc?batchId='+baid+'" target="_blank">Click Here' +
                    '</a> to download "Students TCC and Preferred Date of Exam" for this batch' +
                    '</div>' +
                '</div>' +
                   
                    '<div  id="stdlist" class="row">' +
                        '<div class="col-xs-12">' +
                            '<div class="panel">' +
                                '<div class="panel-body">' +
                                    '<div class="table-responsive">' +
                                        '<table id="studentlisttable" class="table table-hover">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<td><b>URN</b></td>' +
                                                    '<td><b>Name</b></td>' +
                                                    '<td><b>E-mail</b></td>' +
                                                    '<td><b>Phone</b></td>' +
                                                    '<td><b>Branch Code</b></td>' +
                                                    '<td><b>Registered Date</b></td>' +
                                                    '<td><b>TCC Status</b></td>' +
                                                    '<td><b>Preferred Date Of Exam</b></td>' +
                                                    '<td><b>Attendance</b></td>' +
                                                    '<td><b><input type="checkbox" class="revStdAll-check"><button type="button" class="removeStudent btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></button></b></td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                    '<td class="errorClss errorStdCheck" colspan="10"><b>Select Atleast One Student </b></td>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '</tbody>' +
                                        '</table>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );
        
        /* JSON CALL FOR STUDENT LIST IN BATCH*/
            
            
            
            
            
            
            jQuery("#studentlisttable").dataTable({
                
                "iDisplayLength": 10,
                "bProcessing" : true,
                "bServerSide" : true,
                "bSort" : false,
                "sAjaxSource" : "tr-paginliststudentbybatchid?batchId="+baid+"&moduleId="+mid,
                 "columnDefs": [
                    {
                        'targets': -1,
                        'searchable':false,
                        'orderable':false,
                        'render': function (data, type, full, meta){
                        	  return '<input  id="check_'+full.uniId+'"  type=checkbox class="trainercheck" value='+full.uniId+'>';
                        }
                    },{
                        'targets': -2,
                        'searchable':false,
                        'orderable':false,
                        'render': function (data, type, full, meta){
                            return '<button class="attendancelist" id="btn_'+full.uniId+'"><a><i class="fa fa-bullseye"></i> View </a></button>';
                        },
                    },
                    {

                        'targets': -3,
                        'searchable':false,
                        'orderable':false,
                        'render': function (data, type, full, meta){
                        	 pdeStatus=full.pdeCheck;
                        	if(full.tccStatus == 1){
    	                    	return 'URN Expired';
                        	} else if(full.tccStatus == 4) {
                        		if(full.pde==null){
    	                    		return '<input size="8" type="text" name="batch-validfrom" class="form-control pdeDatePic" readonly="true" style="cursor:pointer;background-color: white;"> <button id="tr-submitPerDateToJoinTable" type="button" class=" btn btn-success btn-circle" style="margin-top:-60px;margin-left:150px"> <i class="fa fa-floppy-o" aria-hidden="true" /> </button>';
    	                    	}else {
    	                    		return '<input size="8" type="text" name="batch-validfrom" class="form-control pdeDatePic" value='+full.pde+' readonly="true" style="cursor:pointer;background-color: white;"><button id="tr-submitPerDateToJoinTable" type="button" class=" btn btn-success btn-circle" style="margin-top:-60px;margin-left:150px"> <i class="fa fa-floppy-o" aria-hidden="true" /> </button>';
    	                    	}
                        	}else {
                        		return 'N/A';
                        	}
                        }
                   
                    },{

                        'targets': -4,
                        'searchable':false,
                        'orderable':false,
                        'render': function (data, type, full, meta){
                        	tccStatus=full.tccCheck;
                        	if(full.tccStatus==1){
                        		return 'URN Expired';
                        	}else if(full.tccStatus==4){
                        		preDateArray = full.uniId;
                        		return 'YES';
                        	}else {
                        		return 'NO';
                        	}
                            
                        }
                    
                    }
                    
                    
                    ],
                "aoColumns" : [
                               { "mData": "employeeId" },
                               { "mData": "name" },
                               { "mData": "email" },
                               { "mData": "phoneNum" },
                               { "mData": "branchCode" },
                               { "mData": "createdDate" },
                               { "mData": "" },
                               { "mData": "" },
                               { "mData": "" },
                               { "mData": "" }
                ],
                'fnCreatedRow': function (nRow, data, iDataIndex) {
                    //    $(nRow).attr('id', data.uniId); // or whatever you choose to set as the id
                        $(nRow).attr('id', 'emp_row_' + data.uniId); 
                        if(tccStatus == 0){
                        	$("#studentlisttable").DataTable().column(-4).visible(false);
                        }else{
                        	$("#studentlisttable").DataTable().column(-4).visible(true);
                        }
                        
                        if( pdeStatus == 0){
                        	$("#studentlisttable").DataTable().column(-3).visible(false);
                        }else{
                        	$("#studentlisttable").DataTable().column(-3).visible(true);
                        }
                        
                    },
                    "rowCallback": function( row, data ) {
                    	selected=[];
                    	$(".revStdAll-check").prop('checked',false);
                        if ( $.inArray('emp_row_' + data.uniId, selected) !== -1 ) {
                            $(row).addClass('row_selected');
                            
                            $(row).children().children("#check_"+data.uniId).prop('checked',true);
                        }
                    }
                });
            
            $('#studentlisttable tbody').on('click', 'tr', function () {
                var id = this.id;
                var index = $.inArray(id, selected);
         
                if ( index === -1 ) {
                    selected.push( id );
                    $("#check_"+id.split("_")[2]).prop('checked',true);
                } else {
                    selected.splice( index, 1 );
                    $("#check_"+id.split("_")[2]).prop('checked',false);
                }
         
                $(this).toggleClass('row_selected');
            } );
            
        /*    $("#pageloading").show();
            
            
            Backbone.ajax({
                dataType:"json",
                url:"tr-liststudentbybatchid?batchId="+baid+"&moduleId="+mid,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    studentsIn = Backbone.$("#studentlisttable").children("tbody").empty();
                    trainerbatchdetails.generateStudentList(data);
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
        
    /*--``` GENERATING AND CREATING ROW FOR TABLE OF STUDNETS ACCORDING TO THERE BATCH ```*/
        generateStudentList: function(data) {
            for(var i=0; i< data.length; i++){
                studentsIn.append("<tr id='studenturn"+data[i].uniId+"'><td>"+data[i].employeeId+"</td><td>"+data[i].name+"</td><td>"+data[i].email+"</td><td class='attendancelist'><a><i class='fa fa-bullseye'></i> View </a></td><td><input type='checkbox' class='revStd-check' value="+data[i].uniId+"></td></tr>");
            }
            $('#studentlisttable').dataTable({
                "ordering": false
            });
        },
    
    /*---  LOAD NEW VIEW TO SHOW ATTENDANCE CALL ROUTES WITH STUDENT URN ID  ---*/
        showAttendence: function(e) {
            var studid = e.currentTarget.id.slice(4);
            landingroutes.navigate('trainerbatch/'+baid+'/batchstudentattendence/'+mid+'/'+studid, {trigger: true});
        },
    
    /*ATTENDENCE TABLE OF BATCH */
        batchStudentAttenTable: function(stid) {
            Backbone.$("#tablesbatchs").empty().append(
                '<div class="tab-pane fade in active">' +
                    '<div id="stuntatt" class="row">' +
                        '<div class="col-xs-6">' +
                            '<div class="panel panel-default">' +
                                '<div class="panel-heading">' +
                                    '<h6 style="margin-top: 0; margin-bottom: 0;"><b>Attendance Table </b></h6>' +
                                '</div>' +
                                '<div class="panel-body">' +
                                    '<div class="col-xs-4">Name: <span class="studeName"></span></div>' +
                                    '<div class="col-xs-4">URN: <span class="studId"></span> </div>' + 
                                    '<div class="col-xs-4">Attendance: <span class="present-days"></span>/<span  class="conducted-days"></span></div>' +
                                '</div>' +
                                '<div class="panel-body">' +
                                    '<div class="table-responsive">' +
                                        '<table id="batchstudentattendance" class="table table-hover">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<td><b>Date</b></td>' +
                                                    '<td><b>Slot</b></td>' +
                                                    '<td><b>Attendance</b></td>' +
                                                    '<td><b>Remarks</b></td>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '</tbody>' +
                                        '</table>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-xs-6">' +
                            '<div class="panel panel-default">' +
                                '<div class="panel-heading">' +
                                    '<h6 style="margin-top: 0; margin-bottom: 0;"><b>Score in respective assessments </b></h6>' +
                                '</div>' +
                                '<div class="panel-body">' +
                                    '<div class="table-responsive">' +
                                        '<table id="assesmentScore" class="table table-hover">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<td><b>Attempted</b></td>' +
                                                    '<td><b>Total Question</b></td>' +
                                                    '<td><b>Un Answered</b></td>' +
                                                    '<td><b>Correct Answered</b></td>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '</tbody>' +
                                        '</table>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );
        
        /* JSON CALL FOR STUDENT LIST IN BATCH*/
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"tr-getattofstudentbytid?batchId="+baid+"&studentId="+stid,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    batchAttendenceFlag = true;
                    batchStuAten = Backbone.$("#batchstudentattendance").children("tbody").empty();
                    $(".studeName").text(data.name);
                    $(".studId").text(data.urnId);
                    trainerbatchdetails.generateBatchStudentAtten(data.attendents, stid);
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
    /*--``` GENERATING AND CREATING ROW FOR TABLE OF STUDNETS ACCORDING TO THERE BATCH ```*/
        generateBatchStudentAtten: function(data, stid) {
            var paDat, prDat, prSlt, paSlt;
            presentCount = 0;
            noOfDays = data.length;
            for(var i=0; i< data.length; i++){
                batchStuAten.append('<tr><td>'+data[i].date+'</td><td>'+data[i].slotDesc+'</td><td>'+data[i].status+'</td><td>'+data[i].remarks+'</td></tr>');
                if(data[i].status == "P"){
                    presentCount++;
                }
            }
            $(".present-days").text(presentCount);
            $(".conducted-days").text(noOfDays);
            $("#batchstudentattendance").dataTable({
                "ordering": false
            });
            trainerbatchdetails.generateStudentScore(stid);
        },
        
    /*GENERATE STUDENT SCORE*/

        generateStudentScore: function(stid) {
            studentScore = Backbone.$("#assesmentScore").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"tr-getstudentmockstatus?batchId="+baid+"&studentId="+stid,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    for( var i=0; i< data.length; i++ ){
                        studentScore.append(
                            '<tr>' +
                            '<td>'+data[i].attemptedCount+'</td>' +
                            '<td>'+data[i].totalQuesCount+'</td>' +
                            '<td>'+data[i].unAnsCount+'</td>' +
                            '<td>'+data[i].totalCorrectAnsCount+'</td>' +
                            '</tr>'
                        );
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    /*** MODAL TO DISPLAY THE LIST OF STUDENTS TO ADD TO EXISTING LIST ***/
        addNewStudentToBatch: function() {
            $("#addtobatch").modal('show');
            $("#addtobatch").modal({ keyboard: false });
            json = [];
            $($("#addtobatch div div div")[0]).children(".modal-title").text("Add student");
            $("#addContentToBatch").empty().append(
                    '<table id="addtobatchlist" class="table table-hover display">' +
                        '<thead>' +
                            '<tr>' +
                                '<th> URN No </th>' +
                                '<th> Name </th>' +
                                '<th> Branch Code </th>' +
                                '<th> Register Date </th>' +
                                '<th> <input type=checkbox class="selectAllStudent"></th>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
            
            
            
            selected=[];
            jQuery("#addtobatchlist").dataTable({
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "bSort" : false,
            "sAjaxSource" : "tr-paginstudentlist?moduleId="+mid,
            "sServerMethod": "POST",

             "columnDefs": [
                {
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                    	return '<input  id="check_'+full.uniId+'"  type=checkbox class="addtrainercheck" value='+full.uniId+'>';
                    }
                }],
            "aoColumns" : [
                           { "mData": "employeeId" },
                           { "mData": "name" },
                           { "mData": "branchCode" },
                           { "mData": "createdDate" },
                           { "mData": "" }
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                //    $(nRow).attr('id', data.uniId); // or whatever you choose to set as the id
                    $(nRow).attr('id', 'emp_row_' + data.uniId); 
                },
                "rowCallback": function( row, data ) {
                    if ( $.inArray('emp_row_' + data.uniId, selected) !== -1 ) {
                        $(row).addClass('row_selected');
                        
                        $(row).children().children("#check_"+data.uniId).prop('checked',true);
                    }
                }
            });
            
            
            $('#addtobatchlist tbody').on('click', 'tr', function () {
                var id = this.id;
                var index = $.inArray(id, selected);
         
                if ( index === -1 ) {
                    selected.push( id );
                    $("#check_"+id.split("_")[2]).prop('checked',true);
                } else {
                    selected.splice( index, 1 );
                    $("#check_"+id.split("_")[2]).prop('checked',false);
                }
         
                $(this).toggleClass('row_selected');
            } );
            
       /* #^ CALL JSON FOR LOADING STUDENT LIST ^#
            listofStudents = Backbone.$("#addtobatchlist").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"tr-studentlist?moduleId="+mid,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    trainerbatchdetails.generateStudentSelect(data);
                    $("#addtobatchlist").dataTable({
                        "ordering": false
                    });
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
        
    /*** GENERATING A TABLE FOR STUDENTS FOR SELECTING IN ORDER TO ADD FOR A BATCH ***/
        generateStudentSelect: function(data) {
            for(var i=0; i< data.length; i++){
                listofStudents.append(trainerbatchdetails.createStudentRow(data[i]));
            }
            $('#addtobatchlist').dataTable({
                "ordering": false
            }).yadcf([
                {column_number : 2},
                {column_number : 3}
            ]);
        },
        
    /**** GENERATE ROW FOR STUDENT LIST ON MODAL ****/
        createStudentRow: function(rowObj) {
            try{
                var trElement = "<tr id='"+rowObj.uniId+"'><td>"+rowObj.employeeId+"</td><td>"+rowObj.name+"</td><td>"+rowObj.branchCode+"</td><td>"+rowObj.createdDate+"</td><td><input type=checkbox class='studentcheck' value='"+rowObj.uniId+"'></td>";
                return trElement+"</tr>";
            }catch(e){}
        },

    /*** SELECTED STUDENTS DETAILS ***/
        addStudentToBatch: function(e) {
            studentFlag = true;
            var selstid = e.currentTarget.parentElement.parentElement.id;
            if(e.currentTarget.checked){
                try{
                    json.push({"uniId":e.currentTarget.parentElement.parentElement.id});
                }catch(e){} 
            } else {
                try{
                    this.removeA(e.currentTarget.parentElement.parentElement.id);
                }catch(e){}  
            }
        },
    
    /*** SENDING STUDENTS & TRAINER DETAILS TO ADD A BATCH ***/
        addUserToBatch: function(e) {
            $(".errorClss").hide();
            json = [];
            var urlv;
            
            for(var i=0;i<selected.length;i++){
        		json.push({"uniId": selected[i].split("_")[2]});
        	}
          
            if($(".selectAllStudent").is(':checked')){
                urlv="addstudenttobatch?batchId="+baid+"&addAll=1";
            } else {
                urlv="addstudenttobatch?batchId="+baid+"&addAll=2";
            }
            if(json.length > 0){
                $("#pageloading").show();
                Backbone.ajax({
                    url: urlv,
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        json=[];
                        selected=[];
                        if(data == 1) {
                            reloadErrorFlag = true;
                            $("#addtobatch").modal('hide');
                            $("#addtobatch").on('hidden.bs.modal', function(){
                                if(reloadErrorFlag){
                                    trainerbatchdetails.reloadBatchDeailsPage();
                                }
                            });
                        } else if(data == 2){
                            $(".errorUserSubmit").show();
                        } else {
                            alert("Contact IT team");
                        }
                    },
                    error:function(res,ioArgs){
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                    complete: function() {
                        $("#pageloading").hide();
                    }
                });
            }
            else{
                $(".errorUserSelect").show();
            }
        },
    
    /* PRPBLEM IN MODAL */
        reloadBatchDeailsPage:function(e) {
            trainerbatchdetails.loadStudentTable();
        },
        
    /*** REMOVING A STUDENT FOR BATCH ***/
        removeClickedStudent: function(e) {
            $(".errorClss").hide();
            $(".errorStudentRemove").remove();
            swal({
                    title: "Remove students from this batch",
                    showCancelButton: true,
                    confirmButtonColor: "#F01E1E",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No"
                },
                 function(isConfirm){
                if(isConfirm){
                    var useUnid = [];
                    var remAll;
                    if($('.revStdAll-check').is(':checked')){
                        remAll = 2;
                        var userChecked = $(".trainercheck");
                        for (var i = 0, length = userChecked.length; i < length; i++) {
                            if ($(userChecked[i]).is(':checked')) { 
                                useUnid.push(userChecked[i].value);
                            }
                        }
                    } else {
                        remAll = 2;
                        var userChecked = $(".trainercheck");
                        for (var i = 0, length = userChecked.length; i < length; i++) {
                            if ($(userChecked[i]).is(':checked')) { 
                                useUnid.push(userChecked[i].value);
                            }
                        }
                    }
                    if(useUnid.length>0){
                        $("#pageloading").show();
                        Backbone.ajax({
                            dataType:"json",
                            url:"tr-removestudentbatch?removeAll="+remAll+"&uniId="+useUnid+"&batchId="+baid,
                            data:"",
                            type: "POST",
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader(header, token);
                            },
                            success: function(data){
                            	selected=[];
                                if(data == 1){
                                    trainerbatchdetails.loadStudentTable();
                                } else if(data == 2){
                                    $(e.currentTarget.parentElement).append('<div class="errorClss errorStudentRemove"><br>Failed Try Again</div>');
                                    $(".errorStudentRemove").show();
                                } else{
                                    alert("Contact IT team");
                                }
                            },
                            error: function (res, ioArgs) {

                                if (res.status === 440) {
                                    window.location.reload();
                                }
                            },
                            complete: function() {
                                $("#pageloading").hide();
                            }
                        });
                    } else {
                        $(".errorStdCheck").show();
                    }
                }
            });
        },
        
        removeA: function(v) {  
            for(var i=0;i<json.length;i++){
                if(json[i].uniId == v){
                    json.splice(i, 1);
                }
            }
        },
        
         studentPdeListUploadTrainer:function(e){
            $("#pageloading").show();
            $(".errorClss").hide();
            var urlv = "tr-studentpdelistupload?"+csrf_url+"="+token;
            $(e.currentTarget).parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".pdefiletrainer").parent();
                    $(".pdefiletrainer").remove();
                    $(pf).append('<input name="filename" type="file" class="pdefiletrainer" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    if(data=="1"){
                        sweetAlert("Students Preferred Date of Exam Uploaded Successfully");
                    } else if(data=="2"){
                        $(".errorIntUpFailed").show();
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    $(".pdefiletrainer").val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
            
        },
        /* DATE PICKER CALLING*/
        datePdeValidation: function(e) {
            var startDate = new Date(parseInt($($(e.currentTarget.parentElement.parentElement).children()[5]).text(), 10));
            //var endDate = new Date(startDate.setMonth(startDate.getMonth() + 11));

            $($($(e.currentTarget.parentElement.parentElement).children()[7]).children()).datepicker({
                dateFormat: "yy-mm-dd",
                minDate: new Date(),
                maxDate: "+11M"
            });
            
        },
        
        addPreDateByTrainer: function(e) {
            $(".errorClss").hide();
            json = [];
            var preDate = 0;
            var j = 0;
           /* for(var i = 0; i < preDateArray.length; i++ ) {*/
            	preDateArray=e.currentTarget.parentElement.parentElement.id.split("_");
              var d = $("#emp_row_"+preDateArray[2]+"").children().children(".pdeDatePic").val();
              if(!!d){
                preDate = preDateArray[2]+"_"+d;
                j++;
              }
          /*  }*/
            //var uniIdAndPreDAte = [];
              var urlv = "tr-studentpdelistupdate";
              /*for(var i=0;i<preDate.length / 2;i++){*/
                //uniIdAndPreDAte =  preDate[i].split("_");
              json.push({"uniId":preDate.split("_")[0], "pde": preDate.split("_")[1] });
            /*}*/
              if(json.length > 0){
                $("#pageloading").show();
                Backbone.ajax({
                      url: urlv,
                      data: JSON.stringify(json),
                      type: "POST",
                      beforeSend: function(xhr) {
                          xhr.setRequestHeader(header, token);
                          xhr.setRequestHeader("Accept", "application/json");
                          xhr.setRequestHeader("Content-Type", "application/json");
                      },
                      complete: function() {
                          $("#pageloading").hide();
                          sweetAlert("Preferred Date Of Exam Submited Successfully ");
                      }
                  });
                
              }
              
          },

        

        
    });
    
    
})();
