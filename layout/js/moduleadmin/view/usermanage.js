(function (){
    
    var userRole, userElement, mid, trinerUnid;
    var selected = [];
    window.user_manage = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    user_manage.Views.header = Backbone.View.extend({
        
        events: {
            'click .newbackofficeuser': 'newOfficeUserConfig',
            'click .newtrainersupload': 'newTrainerListUpload',
            'click .interStdListCls': 'internalStudentListUpload',
            'click .extStdListCls': 'externalStudentListUpload',
            'click #intstuidsubmit': 'inTernalStudentIdSubmit',
            'click #officeidsubmit': 'backOfficeUsrIdSubmit',
            'click #traineridsubmit': 'trainerUsrIdSubmit',
            'change .internalstudentfile': 'studentInternalListUpload',
            'change .externalstudentfile': 'studentExternalListUpload',
            'change .trainersfile': 'trainersEmployeeListUpload',
            'change .branchmapfile': 'trainerBrnachMapListUpload',
            'click .trainerCls': 'removeTrainerModule',
            'click .interCls': 'removeInternalStdModule',
            'click .exterCls': 'removeExternalStudentModule',
            'click .removeuser': 'removeBOModule',
            'click #exteridmodal': 'externalStudentAdding',
            'click .exStudSub': 'extStudentDetailsSubmit',
            'focusout #exurid': 'checkForUrnPresent',
            'focus #exurid': 'blockSubmitButton',
            'click .trainerUseCls': 'loadTrainerBrachMaping',
            'click .addbranch': 'addBranchToTrainer',
            'click .removebranch': 'removeBranchToTrainer',
            'focusout #userphone': 'validatePhoneNo',
            'focusout #useremail': 'validateEmailId',
            'click .useAll-check': 'userSelectAll',
            'click .rem-Allbranch': 'selRembranch',
            'click .add-Allbranch': 'selAddbranch',
            'click #addStudentCheck': 'addStuByTrainer',
            'click .newheirarchyupload':'HeirarchyListupload',
            'click #addtrainerHeirarchymodule':'SubmitHeirarchyId',
            'change  .heirarchybranchmapfile':'hierarchyBrnachMapListUpload',
            'click .heirarchyCls' :'removehierarchybyModule',
            	
        },
        
        initialize: function () {
            _.bindAll(this,'render');
        },
        
        render: function (modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="config_module" class="maind" data-toggle="tooltip tab" data-original-title="Module Configuration">&nbsp;&nbsp;<i class="fa fa-file-excel-o fa-fw"></i>' +
                                '<span class="content-hide">Configuration</span>' +
                            '</li>' +
                            '<li id="user_manage" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                                '<span class="content-hide">User Management</span>' +
                            '</li>' +
                            '<li id="batchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="ques_master" class="maind" data-toggle="tooltip tab" data-original-title="Question Bank">&nbsp;&nbsp;<i class="fa fa-pencil fa-fw"></i>' +
                                '<span class="content-hide">Question Bank</span>' +
                            '</li>' +
                            '<li id="admin_attedance" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="paper_create" class="maind" data-toggle="tooltip tab" data-original-title="Test Management">&nbsp;&nbsp;<i class="fa fa-sitemap fa-fw"></i>' +
                                '<span class="content-hide">Test Management</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
            
            if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">'+
                '<div class="col-xs-12">'+
                    '<h1 class="page-header"><span class="user-header"></span><div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>'+
                '</div>'+
                '<!-- /.col-xs-12 -->'+
            '</div>' +
            '<div id="user_wrapper">' +
                /*Tabs */
                '<div class="row" style="margin-bottom: 20px">' +
                    '<ul class="nav nav-pills">' +
                        '<li class="newbackofficeuser active">' +
                            '<a data-toggle="pill" href="#boMenu">Back Office User</a>' +
                        '</li>' +
                        '<li class="newtrainersupload"><a data-toggle="pill" href="#addNewTrainer">Add Trainers</a></li>' +
                        '<li class="dropdown">' +
                            '<a class="dropdown-toggle" data-toggle="dropdown" href="#">Add Students<span class="caret"></span></a>' +
                            '<ul class="dropdown-menu">' +
                                '<li class="interStdListCls"><a data-toggle="pill" href="#intNewStudent">Internal Student</a></li>' +
                                '<li class="extStdListCls"><a data-toggle="pill" href="#extNewStudent">External Student</a></li>' +
                            '</ul>' +
                        '</li>' +
                        '<li class="newheirarchyupload"><a data-toggle="pill" href="#addNewheirarchy">Add Hierarchy</a></li>' +
                      '</ul>' +
                '</div>' +
            /*Tabs Contents*/
                
                '<div class="tab-content" style="margin-bottom: 20px">' +
                
                /*BO user Group adding*/
                    '<div id="boMenu" class="tab-pane fade in active">' +
                        '<div class="row" style="margin-bottom: 20px">' +
                            '<div class="col-xs-6">' +
                                '<label for="backoffgrpid" class="control-label">Group Id</label><span class="clsRed">*</span>' +
                                '<input type="text" name="back-office-group" id="backoffgrpid" class="form-control" placeholder="Group Id" tabindex="1">' +
                                '<div class="errorClss errorBoGroupId">Fill Group ID</div>' +
                                '<div class="errorClss errorBoFailed">Back Office User Adding Failed. Try Again.. !</div>' +
                            '</div>' +
                            '<div class="col-xs-1">' +
                                '<label class="control-label"> </label>' +
                                '<button id="officeidsubmit" class="btn btn-primary"  tabindex="1" style="margin-top: 45%;"> Add </button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                
                /*Trainner Adding*/
                    '<div id="addNewTrainer" class="tab-pane fade">' +
                        '<div class="row" style="margin-bottom: 20px">' +
                            '<div class=" col-xs-3">' +
                                '<label for="trainerusrid" class="control-label">Employee Id</label><span class="clsRed">*</span>' +
                                '<input type="text" name="trainer-empid-adding" style="width:50%;"  id="trainerusrid" class="form-control" placeholder="Employee Id" tabindex="1">' +
                                '<div class="errorClss errorTrainerEmpId">Employee ID Mandatory</div>' +
                                '<div class="errorClss errorTrainerFailed">Trainer Adding Failed. Try Again.. !</div>' +
                                '<div class="errorClss errorTrinerEmpId">This Employee Id Not Found </div>' +
                                '<div class="errorClss errorTrinerLogId">Employee \"Login\" Id Not Found</div>' +
                            '</div>' +
                            '<div class=" col-xs-3">' +
                                '<label for="trainerbc" class="control-label">Branch Code</label><span class="clsRed">*</span>' +
                                '<input type="text" name="trainer-group-adding" style="width:50%;" id="trainerbc" class="form-control" placeholder="Branch Code" tabindex="1">' +
                                '<div class="errorClss errorTrainerBc">Branch Code Mandatory</div>' +
                            '</div>' +
                            '<div  class="col-md-3">' +
                            '<label for="trainerbc" class="control-label">Add Student</label>' +
                            '<input type="checkbox" name="trainer-group-adding" style="margin-top: 30px;margin-left:-40px" class="btn"  id="checkTriner">' +
                            '</div>' +
                            '<div class="col-md-3">' +
                                '<label class="control-label"> </label>' +
                                '<button id="traineridsubmit" class="btn btn-primary"  tabindex="1" style="margin-top:5%;margin-left:-150px;"> Add </button>' +
                            '</div>' +
                        '</div>' +
                        '<div class="row" style="margin-bottom: 20px">' +
                            '<div class="col-xs-5">' +
                                '<label class="control-label">Upload excel of trainers list</label><span class="clsRed">*</span>' +
                                '<div class="col-xs-1" style="left:64%; top:-8px;">' +
                                    '<form method="post" enctype="multipart/form-data">'+
                                        '<div class="btn btn-outline btn-primary btn-file">'+
                                            '<i class="fa fa-upload">'+
                                            '</i> &nbsp;Upload <input name="filename" type="file" class="trainersfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="left: 16px;width: 90px;">'+
                                        '</div>'+
                                    '</form>'+
                                '</div>'+
                                '<div class="errorClss errorTrinerUploadFailed">Trainer\'s Upload Failed. Try Again..!</div>' +
                            '</div>'+
                            '<div class="col-xs-5">' +
                                '<span><a href="trainerslistuploadtemplate" target="_blank">Click Here </a>To Download Template for Trainers Upload</span>' +
                            '</div>'+
                            '<div class="col-xs-5">' +
                            '<span><a href="trainerslistuploadeddata" target="_blank">Click Here </a>To Download Uploaded Trainers</span>' +
                        '</div>'+
                        '</div>'+
                        '<div class="row">' +
                        '<div class="col-xs-5">' +
                            '<label class="control-label">Upload branch mapping list </label><span class="clsRed">*</span>' +
                            '<div class="col-xs-1" style="left:64%; top:-8px;">' +
                                '<form method="post" enctype="multipart/form-data">'+
                                    '<div class="btn btn-outline btn-primary btn-file">'+
                                        '<i class="fa fa-upload">'+
                                        '</i> &nbsp;Upload <input name="filename" type="file" class="branchmapfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="left: 16px;width: 90px;">'+
                                    '</div>'+
                                '</form>'+
                            '</div>'+
                            '<div class="errorClss errorbrmpFailed">Uploading Failed. Try Again...!</div>' +
                        '</div>'+
                        '<div class="col-xs-5">' +
                            '<span><a href="trainerbranchuploadtemplate" target="_blank">Click Here </a>To Download Template For Trainer Branch Mapping</span>' +
                        '</div>'+
                        '<div class="col-xs-5">' +
                        '<span><a href="branchmapdata" target="_blank">Click Here </a>To Download Trainer Branch Mapped Data</span>' +
                    '</div>'+
                    '</div>'+
                    '</div>' +
                    
                    /* Adding Hierarchy Details static page */
                    '<div id="addNewheirarchy" class="tab-pane fade">' +
                    '<div class="row" style="margin-bottom: 20px">' +
                        '<div class=" col-xs-3">' +
                            '<label for="trainerheirarchyid" class="control-label">Employee Id</label><span class="clsRed">*</span>' +
                            '<input type="text" name="trainer-empid-adding" id="trainerheirarchyid" class="form-control" placeholder="Employee Id" tabindex="1">' +
                            '<div class="errorClss errorTrainerEmpId">Employee ID Mandatory</div>' +
                            '<div class="errorClss errorTrainerFailed">Trainer Adding Failed. Try Again.. !</div>' +
                            '<div class="errorClss errorTrinerEmpId">This Employee Id Not Found </div>' +
                            '<div class="errorClss errorTrinerLogId">Employee \"Login\" Id Not Found</div>' +
                        '</div>' +
                        '<div class=" col-xs-3">' +
                            '<label for="managerbc" class="control-label">Manager ID</label>' +
                            '<input type="text" name="trainer-group-adding" id="managerbc" class="form-control" placeholder="Manager Id" tabindex="1">' +
                        '</div>' +
                        '<div class=" col-xs-2" style="width:20%">' +
                        '<label for="" class="control-label">Employee Designation</label><span class="clsRed">*</span>' +
                        '<input type="text" name="students-group-adding" id="employeebc" class="form-control" placeholder="Employee Designation" tabindex="1" >' +
                        '<div class="errorClss errorEmployeeBc">Employee Designation Mandatory</div>' +
                       '</div>' +
                        
                        '<div class="col-md-1" style= "margin-top: 5px">' +
                            '<label class="control-label"> </label>' +
                            '<button id="addtrainerHeirarchymodule" class="btn btn-primary"  tabindex="1" > Add </button>' +
                        '</div>' +
                       
                    '</div>' +
                    '<div class="row" style="margin-bottom: 20px">' +
                    '<div class="col-xs-5">' +
                        '<label class="control-label">Upload branch mapping list </label><span class="clsRed">*</span>' +
                        '<div class="col-xs-1" style="left:64%; top:-8px;">' +
                            '<form method="post" enctype="multipart/form-data">'+
                                '<div class="btn btn-outline btn-primary btn-file">'+
                                    '<i class="fa fa-upload">'+
                                    '</i> &nbsp;Upload <input name="filename" type="file" class="heirarchybranchmapfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="left: 16px;width: 90px;">'+
                                '</div>'+
                            '</form>'+
                        '</div>'+
                        '<div class="errorClss errorUploadingFailed">Uploading Failed. Try Again After Some Time...!</div>' +
                    '</div>'+
                    '<div class="col-xs-5">' +
                        '<span><a href="heirarchybranchuploadtemplate" target="_blank">Click Here </a>To Download Template For Hierarchy Branch Mapping</span>' +
                    '</div>'+
                    '<div class="col-xs-5">' +
                    '<span><a href="heirarchybtmbranchdetails" target="_blank">Click Here </a>To Download Hierarchy Branch Mapped Data</span>' +
                '</div>'+
                '</div>'+
                '</div>' +
                
                /*Internal Student adding*/
                    '<div id="intNewStudent" class="tab-pane fade">' +
                        '<div class="row" style="margin-bottom: 20px">' +
                            '<div class="col-xs-5">' +
                                '<label for="studentempid" class="control-label">Student Id</label><span class="clsRed">*</span>' +
                                '<input type="text" name="student-group-adding" id="studentempid" class="form-control" placeholder="Student Id" tabindex="1">' +
                                '<div class="errorClss errorInterNalStudent">Fill Student ID</div>' +
                                '<div class="errorClss errorStdFailed">Adding Failed. Try Again.. !</div>' +
                                '<div class="errorClss errorStdIdNFound">This Student Id Not Found</div>' +
                                '<div class="errorClss errorStdLogIdNFound">Student \"Login\" Id Not Found</div>' +
                            '</div>' +
                            '<div class="col-xs-5">' +
                                '<label for="studentbc" class="control-label">Branch Code</label><span class="clsRed">*</span>' +
                                '<input type="text" name="trainer-group-adding" id="studentbc" class="form-control" placeholder="Branch Code" tabindex="1">' +
                                '<div class="errorClss errorInteBCStudent">Required</div>' +
                            '</div>' +
                            '<div class="col-xs-1">' +
                                '<label class="control-label"> </label>' +
                                '<button id="intstuidsubmit" class="btn btn-primary"  tabindex="1" style="margin-top: 45%;"> Add </button>' +
                            '</div>' +
                        '</div>' +
                        '<div class="row" style="margin-bottom: 20px">' +
                            '<div class="col-xs-4" style="text-align:right">' +
                                '<label class="control-label">Upload excel of internal students list</label><span class="clsRed">*</span>' +
                                '<div class="errorClss errorIntUpFailed">Uploading Failed. Try Again...!</div>' +
                            '</div>'+
                            '<div class="col-xs-2">' +
                                '<form method="post" enctype="multipart/form-data">'+
                                    '<div class="btn btn-outline btn-primary btn-file">'+
                                        '<i class="fa fa-upload">'+
                                        '</i> &nbsp;Upload <input name="filename" type="file" class="internalstudentfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="left: 16px;width: 90px;">'+
                                    '</div>'+
                                '</form>'+
                            '</div>'+
                            '<div class="col-xs-5">' +
                                '<span><a href="internalstudentlistuploadtemplate" target="_blank">Click Here </a>To Download Template For Uploading Internal Students</span>' +
                            '</div>'+
                            '<div class="col-xs-5">' +
                            '<span><a href="internalstudentlistuploadedData" target="_blank">Click Here </a>To Download Uploaded Internal Students</span>' +
                        '</div>'+
                        '</div>' +
                    '</div>' +
                
                /*External Student adding*/
                    '<div id="extNewStudent" class="tab-pane fade">' +
                        '<div class="row" style="margin-bottom: 20px">' +
                            '<div class="col-md-6">' +
                                '<button id="exteridmodal" class=" btn btn-default"  tabindex="1"> Add Student </button>' +
                            '</div>' +
                            '<div class="col-md-6">' +
                                '<div class="errorClss errorExtStuSub">Student Entry Failed. Try Again...!</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="row" style="margin-bottom: 20px">' +
                            '<div class="col-xs-6">' +
                                '<label class="control-label">Upload excel of students list</label><span class="clsRed">*</span>' +
                                '<div class="col-xs-1" style="left:64%; top:-8px;">' +
                                    '<form method="post" enctype="multipart/form-data">'+
                                        '<div class="btn btn-outline btn-primary btn-file">'+
                                            '<i class="fa fa-upload">'+
                                            '</i> &nbsp;Upload <input name="filename" type="file" class="externalstudentfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="left: 16px;width: 90px;">'+
                                        '</div>'+
                                    '</form>'+
                                '</div>'+
                                '<div class="errorClss errorExtUpFailed">Uploading Failed. Try Again...!</div>' +
                            '</div>'+
                            '<div class="col-xs-6">' +
                                '<span><a href="externalstudentlistuploadtemplate" target="_blank">Click Here </a>To Download Template For External Students Upload</span>' +
                            '</div>'+
                            '<div class="col-xs-6">' +
                            '<span><a href="externalstudentlistuploaddata" target="_blank">Click Here </a>To download the Uploaded External Students</span>' +
                        '</div>'+
                        '</div>' +
                    '</div>' +
                '</div>' +
                
            /*Display user name */
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel panel-default">' +
                            '<div class="panel-heading">' +
                                '<h4 style="margin-top:0; margin-bottom:0; text-align: center;" class="user_heading"></h4>' +
                            '</div>' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive" id="userTableInherit">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            
        /*From for adding External student*/
            '<div class="modal fade" id="createnewuser" tabindex="-1" role="dialog" aria-labelledby="createnewuser" aria-hidden="true" data-backdrop="static" style="margin-top:5%">' +
                '<div class="modal-dialog" style="width: 50%">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> New external student entry </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-lg-12">' +
                                        '<form role="form" id="userform">' +
                                            '<div class="row">' +
                                                '<div class="form-group col-lg-5">' +
                                                    '<label for="fname" class="control-label">First Name</label><span class="clsRed">*</span>' +
                                                    '<input type="text" name="fname" id="fname" class="form-control extform-emp" placeholder="First Name">' +
                                                '</div>' +
                                                '<div class="form-group col-lg-3">' +
                                                    '<label for="mname" class="control-label">Middle Name</label>' +
                                                    '<input type="text" name="mname" id="mname" class="form-control extform-emp" placeholder="Middle Name">' +
                                                '</div>' +
                                                '<div class="form-group col-lg-4">' +
                                                    '<label for="lname" class="control-label">Last Name</label>' +
                                                    '<input type="text" name="lname" id="lname" class="form-control extform-emp" placeholder="Last Name">' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="row">' +
                                                '<div class="form-group col-lg-12">' +
                                                    '<div class="errorClss errorExtFNMan">First Name is Mandatory </div>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="row">' +
                                                '<div class="form-group col-lg-12">' +
                                                    '<label for="exurid" class="control-label">URN ID</label><span class="clsRed">*</span>' +
                                                    '<input name="ext-urn-id" id="exurid" class="form-control extform-emp" placeholder="URN ID">' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="row">' +
                                                '<div class="form-group col-lg-12">' +
                                                    '<div class="errorClss errorExtUrIdMan">URN ID is Mandatory </div>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="row">' +
                                                '<div class="form-group col-lg-6">' +
                                                    '<label for="useremail" class="control-label">E-mail Id</label>' +
                                                    '<input type="text" name="email" id="useremail" class="form-control extform-emp" placeholder="email id">' +
                                                    '<span class="ext-email-no"></span>' +
                                                '</div>' +
                                                '<div class="form-group col-lg-5">' +
                                                    '<label for="userphone" class="control-label">Phone No</label>' +
                                                    '<input name="phoneno" id="userphone" class="form-control extform-emp" placeholder="Phone no">' +
                                                    '<span class="ext-phone-no"></span>' +
                                                '</div>' +
                                            '</div>' +
                                            '<div class="form-group">' +
                                                '<label for="brcd" class="control-label">Branch Code</label><span class="clsRed">*</span>' +
                                                '<input name="brach_code" id="brcd" class="form-control extform-emp" placeholder="Brach Code">' +
                                            '</div>' +
                                            '<div class="row">' +
                                                '<div class="form-group col-lg-12">' +
                                                    '<div class="errorClss errorBRCDMan">Brach Code is Mandatory </div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</form>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<button type="submit" class="exStudSub btn btn-default"  tabindex="1">Submit </button>' +
                            '<button type="reset" class="btn btn-default" data-dismiss="modal"  tabindex="1"> Cancel </button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
            );
            usermanageindex.newOfficeUserConfig();
        },
        
        
    /* User Select All */
        userSelectAll: function(e) {
        	
            if($('.useAll-check').is(':checked')){
                $(".user-check").prop('checked',true);
                var aa= $(".user-check");
                for(var i=0;i<aa.length;i++){
                	var id=$(aa[i]).attr("id").slice(6);
                	 var index = $.inArray("emp_row_"+id, selected);
                     if ( index === -1 ) {
                         selected.push("emp_row_"+ id );
                         $("#emp_row_"+id).addClass('row_selected');
                        // $("#check_"+id.split("_")[2]).prop('checked',true);
                     }
                }
            } else if(!$('.useAll-check').is(':checked')) {
                $(".user-check").prop('checked',false);
                var aa= $(".user-check");
                for(var i=0;i<aa.length;i++){
                	var id=$(aa[i]).attr("id").slice(6);
                	 var index = $.inArray("emp_row_"+id, selected);
                     if ( index != -1 ) {
                        // selected.push( id );
                         selected.splice( index, 1 );
                         $("#emp_row_"+id).removeClass('row_selected');
                        // $("#check_"+id.split("_")[2]).prop('checked',true);
                     }
                }
            }
        },
        
        
    /* Email ID Validation */
        
        validateEmailId: function(e) {
            if (usermanageindex.validEmail('useremail')) {
                $('.ext-email-no').html('Valid');
                $('.ext-email-no').css('color', 'green');
            }
            else {
                $('.ext-email-no').html('Invalid');
                $('.ext-email-no').css('color', 'red');
            }
        },
        
        validEmail: function (useremail) {
            
            var a = document.getElementById(useremail).value;
            var filter = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        },
        
    /* Phone NO Validation */
        
        validatePhoneNo: function(e) {
            if (usermanageindex.validatePhone('userphone')) {
                $('.ext-phone-no').html('Valid');
                $('.ext-phone-no').css('color', 'green');
            }
            else {
                $('.ext-phone-no').html('Invalid');
                $('.ext-phone-no').css('color', 'red');
            }
        },
        
        validatePhone: function (userphone) {
            
            var a = document.getElementById(userphone).value;
            var filter = /^\d{10}$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        },
        
    /**** Show Modal for adding student ****/
        externalStudentAdding: function(e) {
            $("#createnewuser").modal({ keyboard: false });
            $("#createnewuser").modal('show');
            $(".extform-emp").val("");
        },
        
    /*** Toggel of Uploading and config ***/
        newOfficeUserConfig: function(e) {
            var url = "paginlistofbackofficeuser?moduleId="+mid;
            usermanageindex.togglingConfigUploadFiles("backofficeclass");
            usermanageindex.backOfficeUserForModule(url);
            $(".user_heading").text("Back Office Group Id");
            $(".user-header").text("Back Office User Details");
        },
        
        newTrainerListUpload: function(e) {
            var url = "listoftrainers?moduleId="+mid;
            usermanageindex.togglingConfigUploadFiles("trainersaddclass");
            usermanageindex.trainersListForModule(url, "trainerCls");
            $(".user_heading").text("Trainers Id");
            $(".user-header").text("Trainers Details");
        },
        
        internalStudentListUpload: function(e) {
            var url = "listofinternalstudents?moduleId="+mid;
            usermanageindex.togglingConfigUploadFiles("internalstudentaddclass");
            usermanageindex.usersListForModule(url, "interCls");
            $(".user_heading").text("Internal Student Id");
            $(".user-header").text("Internal Student Details");
        },
        externalStudentListUpload: function(e) {
            var url = "listofexternalstudents?moduleId="+mid;
            usermanageindex.togglingConfigUploadFiles("externalstudentaddclass");
            usermanageindex.usersListForModule(url, "exterCls");
            $(".user_heading").text("External Student Id");
            $(".user-header").text("External Student Details");
        },
        
        togglingConfigUploadFiles: function(detail) {
            $('#backoffusrid').val("");
            $('#backoffgrpid').val("");
            $('#studentempid').val("");
            $('#trainerbc').val("");
            $('#studentbc').val("");
        },
        
        HeirarchyListupload:function(e) {
            var url="paginlistofheirarchybymodule";
            usermanageindex.togglingConfigUploadFiles("heirarchyaddClass");
            usermanageindex.heirarchyListForModule(url,"heirarchyCls")
             $(".user_heading").text("Hierarchy Details");
            $(".user-header").text("Hierarchy Details");

        },
    
    /*** ITERNAL STUDENTS EMPLOYEE ID SUBMITTING ***/
        inTernalStudentIdSubmit:function(e) {
            $(".errorClss").hide();
            if($('#studentempid').val() == 0) {
               $(".errorInterNalStudent").show();
            } else if ($('#studentbc').val() == 0){
               $(".errorInteBCStudent").show();
            } else {
                
                var json = [];
                try{
                    json.push({"employeeId":$('#studentempid').val(),"branchCode":$('#studentbc').val()});
                }catch(e){}
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addinternalstudentmodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        
                        if(data == 1){
                            $('#studentempid').val("");
                            usermanageindex.internalStudentListUpload();
                            sweetAlert("Success","Students added successfully");
                        } else if(data == 2){
                            $(".errorStdFailed").show();
                        }  else if(data == 3){
                            $(".errorStdIdNFound").show();
                        } else if(data == 4){
                            $(".errorStdLogIdNFound").show();
                        } else {
                            sweetAlert(data);
                        }
                    },
                    error:function(res,ioArgs){
                        
                        $('#studentempid').val("");
                        $('#studentbc').val("");
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
    
    /*** BACKOFFICE USER ID SUBMIT ***/
        backOfficeUsrIdSubmit:function() {
            $(".errorClss").hide();
            if(($('#backoffgrpid').val() == 0)){
                $(".errorBoGroupId").show();
            } else {
                
                var json;
                try{
                    json={"groupId":$('#backoffgrpid').val(),"moduleId":mid};
                }catch(e){}
                $("#pageloading").show();
                Backbone.ajax({
                    url: "backofficeusergrpidmodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        
                        if(data == 1){
                            sweetAlert("Back office users added successfully");
                            usermanageindex.newOfficeUserConfig();
                        } else if(data == 2){
                            $(".errorBoFailed").show();
                        } else{
                            alert("Contact IT team");
                        }
                    },
                    error:function(res,ioArgs){
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
        /***Heirarchy list upload*/
        SubmitHeirarchyId:function() {
           $(".errorClss").hide();
           if ($('#trainerheirarchyid').val() == 0){
               $(".errorTrainerEmpId").show();
           } else if($('#employeebc').val()== 0){
               $(".errorEmployeeBc").show();

           }else{
               
               var json;
               try{
                   json={"managerId":$('#managerbc').val(),"trainerId":$('#trainerheirarchyid').val(),"designation":$('#employeebc').val()};
               }catch(e){}
               $("#pageloading").show();
               Backbone.ajax({
                   url: "addtrainerHeirarchymodule",
                   data: JSON.stringify(json),
                   type: "POST",
                   //contentType: "application/json; charset=utf-8",
                   beforeSend: function(xhr) {
                       xhr.setRequestHeader(header, token);
                      /* xhr.setRequestHeader("Accept", "text/plain");*/
                       xhr.setRequestHeader("Content-Type", "application/json");
                   },
                   success: function(res, data) {
                       
                       if(res == 1){
                           $('#trainerheirarchyid').val("");
                           $('#managerbc').val("");
                           $('#employeebc').val("");
                           usermanageindex.HeirarchyListupload();
                           sweetAlert("Hierarchy list added successfully");
                       } else if(res == 2){
                           $(".errorTrainerFailed").show();
                       } /*else if(data == 3){
                    	   sweetAlert("Employee ID already mapped to a Manager ID. Please first delink it in order to proceed...!");
                       }*/ else if(res == 4){
                           $(".errorTrinerLogId").show();
                       } else{

                           swal({
                               title: res,
                               showCancelButton: true,
                               confirmButtonColor: "#F01E1E",
                               confirmButtonText: "Yes",
                               cancelButtonText: "No"
                           },
                                function(isConfirm){
                               if (isConfirm) {
                            	   usermanageindex.linkNewHierarchyAfterDelink();
                               } 
                           });
                       
                       }
                   },
                   error:function(res,ioArgs){
                       
                       $('#trainerusrid').val("");
                       $('#managerbc').val("");
                       $('#employeebc').val("");
                       if(res.status == 403){
                           window.location.reload();
                       }
                   },
               complete: function() {
                   $("#pageloading").hide();
               }
               });
           }
       },
       
       /*** Delinking/Linking the Hierarchy  ***/
       linkNewHierarchyAfterDelink:function(){
    	  
           var json = [];
           try{
               json.push({"managerId":$('#managerbc').val(),"trainerId":$('#trainerheirarchyid').val(),"designation":$('#employeebc').val()});
           }catch(e){}
           $("#pageloading").show();
           Backbone.ajax({
               url: "linknewdataafterdelinking",
               data: JSON.stringify(json),
               type: "POST",
               //contentType: "application/json; charset=utf-8",
               beforeSend: function(xhr) {
                   xhr.setRequestHeader(header, token);
                   //xhr.setRequestHeader("Accept", "text/plain");
                   xhr.setRequestHeader("Content-Type", "application/json");
               },
               success: function(res, data) {
            	   $(".errorClss").hide();
                   $(".batchsubmit").css("display","inline-block");
                   $(".editbatchsubmit").hide();
                   $("#createnewbatch").modal({ keyboard: false });
                   if(res == 1){
                       $('#trainerheirarchyid').val("");
                       $('#managerbc').val("");
                       $('#employeebc').val("");
                       usermanageindex.HeirarchyListupload();
                       sweetAlert("Hierarchy list added successfully");
                   } else if(res == 2){
                       $(".errorTrainerFailed").show();
                   } /*else if(data == 3){
                	   sweetAlert("Employee ID already mapped to a Manager ID. Please first delink it in order to proceed...!");
                   }*/ else if(res == 4){
                       $(".errorTrinerLogId").show();
                   } 
               },
               error:function(res,ioArgs){
                   
                   $('#trainerusrid').val("");
                   $('#managerbc').val("");
                   $('#employeebc').val("");
                   if(res.status == 403){
                       window.location.reload();
                   }
               },
           complete: function() {
               $("#pageloading").hide();
           }
           });
       },
       
       /*** Delinking/Linking the Hierarchy for upload  ***/
       linknewdataafterdelinkingforupload:function(){
    	  
           var json = [];
           try{
               json.push({"managerId":$('#managerbc').val(),"trainerId":$('#trainerheirarchyid').val(),"designation":$('#employeebc').val()});
           }catch(e){}
           $("#pageloading").show();
           Backbone.ajax({
               url: "linknewdataafterdelinkingforupload",
               data: JSON.stringify(json),
               type: "POST",
               //contentType: "application/json; charset=utf-8",
               beforeSend: function(xhr) {
                   xhr.setRequestHeader(header, token);
                   //xhr.setRequestHeader("Accept", "text/plain");
                   xhr.setRequestHeader("Content-Type", "application/json");
               },
               success: function(res, data) {
            	   $(".errorClss").hide();
                   $(".batchsubmit").css("display","inline-block");
                   $(".editbatchsubmit").hide();
                   $("#createnewbatch").modal({ keyboard: false });
                   if(res == 1){
                       $('#trainerheirarchyid').val("");
                       $('#managerbc').val("");
                       $('#employeebc').val("");
                       usermanageindex.HeirarchyListupload();
                       sweetAlert("Hierarchy list added successfully");
                   } else if(res == 2){
                       $(".errorTrainerFailed").show();
                   } /*else if(data == 3){
                	   sweetAlert("Employee ID already mapped to a Manager ID. Please first delink it in order to proceed...!");
                   }*/ else if(res == 4){
                       $(".errorTrinerLogId").show();
                   } 
               },
               error:function(res,ioArgs){
                   
                   $('#trainerusrid').val("");
                   $('#managerbc').val("");
                   $('#employeebc').val("");
                   if(res.status == 403){
                       window.location.reload();
                   }
               },
           complete: function() {
               $("#pageloading").hide();
           }
           });
       },
    /*** TRAINERS INTERNAL ID SUBMIT ***/  
        trainerUsrIdSubmit:function() {
            $(".errorClss").hide();
            if ($('#trainerusrid').val() == 0){
                $(".errorTrainerEmpId").show();
            } else if($('#trainerbc').val() == 0) {
                $(".errorTrainerBc").show();
            } else {
                
                var json = [];
                try{
                	if(!$("#checkTriner").prop("checked")){
                		json.push({"branchCode":$('#trainerbc').val(),"employeeId":$('#trainerusrid').val(),"addStudentCheck":0});
                	}else{
                		json.push({"branchCode":$('#trainerbc').val(),"employeeId":$('#trainerusrid').val(),"addStudentCheck":1});
                	}
                    
                }catch(e){}
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addtrainermodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        
                        if(data == 1){
                            $('#trainerusrid').val("");
                            $('#trainerbc').val("");
                            usermanageindex.newTrainerListUpload();
                            sweetAlert("Trainers added successfully");
                        } else if(data == 2){
                            $(".errorTrainerFailed").show();
                        } else if(data == 3){
                            $(".errorTrinerEmpId").show();
                        } else if(data == 4){
                            $(".errorTrinerLogId").show();
                        } else{
                            sweetAlert(data);
                        }
                    },
                    error:function(res,ioArgs){
                        
                        $('#trainerusrid').val("");
                        $('#trainerbc').val("")
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
    /*** TRAINERS EMPLOYE ID LIST UPLOAD ***/
        trainersEmployeeListUpload:function(e){
            $("#pageloading").show();
            $(".errorClss").hide();
            var urlv = "trainerslistupload?"+csrf_url+"="+token+"&moduleId="+mid;
            $(e.currentTarget).parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".trainersfile").parent();
                    $(".trainersfile").remove();
                    $(pf).append('<input name="filename" type="file" class="trainersfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    if(data == 1){
                        usermanageindex.newTrainerListUpload();
                        sweetAlert("Trainers List uploaded successfully");
                    } else if(data == 2){
                    	sweetAlert("Uploading Failed. Try Again...!"); 
                    } else{
                        alert("Contact IT team");
                    }
                },
                error:function(res,ioArgs){
                    $(".trainersfile").val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
            
        },
        
    /*** STUDENTS INTERNAL EMPLOYE ID LIST UPLOAD ***/
        studentInternalListUpload:function(e){
            $("#pageloading").show();
            $(".errorClss").hide();
            var urlv = "internalstudentlistupload?"+csrf_url+"="+token+"&moduleId="+mid;
            $(e.currentTarget).parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".internalstudentfile").parent();
                    $(".internalstudentfile").remove();
                    $(pf).append('<input name="filename" type="file" class="internalstudentfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    if(data=="1"){
                        usermanageindex.internalStudentListUpload();
                        sweetAlert("Internal Students uploaded successfully");
                    } else if(data=="2"){
                    	sweetAlert("Uploading Failed. Try Again...!");
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    $(".internalstudentfile").val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
            
        },
        
    /*** STUDENT EXTERNAL EMPLOYE ID LIST UPLOAD ***/
        studentExternalListUpload:function(e){
            $("#pageloading").show();
            $(".errorClss").hide();
            var urlv = "externalstudentlistupload?"+csrf_url+"="+token+"&moduleId="+mid;
            $(e.currentTarget).parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".externalstudentfile").parent();
                    $(".externalstudentfile").remove();
                    $(pf).append('<input name="filename" type="file" class="externalstudentfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    if(data=="1"){
                        usermanageindex.externalStudentListUpload();
                        sweetAlert("External Students uploaded successfully");
                    } else if(data=="2"){
                    	sweetAlert("Uploading Failed. Try Again...!"); 
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    $(".externalstudentfile").val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
            
        },
        
    /*** DISPLAY USER LISTS ***/
        trainersListForModule: function(urls, cls) {
        	
        	Backbone.$("#userTableInherit").empty().append(
                    '<table id="usertable" class="table table-hover">' +
                        '<thead>' +
                            '<tr>' +
                                '<td><b>Employee Id</b></td>' +
                                '<td><b>Name</b></td>' +
                                '<td><b>E-mail</b></td>' +
                                '<td><b>Branch Code</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Add Students</b><input type="checkbox"  class="useAll-check"><button id="addStudentCheck" type="button" class=" btn btn-success btn-circle"> <i class="fa fa-plus" aria-hidden="true"/> </button></td>' +
                                /*'<td></td>' +
                                '<td></td>' +*/
                               '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
        	
        	
        	
  
        	var studentTable;
        	 
        	studentTable = jQuery("#usertable").dataTable({
        	 
        	"iDisplayLength": 10,
        	"bProcessing" : true,
        	"bServerSide" : true,
        	"bSort" : false,
        	"sAjaxSource" :urls,
            "sServerMethod": "POST",
            "bSort" : false,
        	/*"sAjaxDataProp":"",*/
        	 "columnDefs": [ {
                 "targets": -2,
                 "data": null,
                 "defaultContent": "<button class='trainerUseCls btn btn-default' style='color:#00f;'> Edit </button>  <button class='"+cls  +" btn btn-outline btn-primary'> Remove </button>"
             },
             {
                 'targets': -1,
                 'searchable':false,
                 'orderable':false,
                 'render': function (data, type, full, meta){
                	 if(full.addStudentCheck) {
                		 return '<input id="check_'+full.uniId+'" type="checkbox" class="user-check" checked="true"  value='+full.uniId+'  ">';
                	 } else {
                		 return '<input id="check_'+full.uniId+'" type="checkbox" class="user-check"  value='+full.uniId+'  ">';
                	 }
                 }
             }],
        	 
        	"aoColumns" : [
        	               { "mData": "employeeId" },
        	               { "mData": "name" },
        	               { "mData": "email" },
        	               { "mData": "branchCode" },
        	               { "mData": "" },
        	               { "mData": "" },

        	              
        	],
        		
        	rowCallback: function ( row, data ) {
                $('user-check', row).prop( 'checked', data.addStudentCheck == 1 );
            },
        	
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'module' + data.uniId); // or whatever you choose to set as the id
                   
            },
             
        	});
        	
        	/*
            Backbone.$("#userTableInherit").empty().append(
                '<table id="usertable" class="table table-hover">' +
                    '<thead>' +
                        '<tr>' +
                            '<td><b>Employee Id</b></td>' +
                            '<td><b>Name</b></td>' +
                            '<td><b>E-mail</b></td>' +
                            '<td><b>Branch Code</b></td>' +
                            '<td><b>Branch Mapping</b></td>' +
                            '<td></td>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                '</table>'
            );
            Backbone.$("#usertable").children("tbody").empty();
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: urls,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0){
                        
                    } else{
                        userElement = Backbone.$("#usertable").children("tbody").empty();
                        usermanageindex.generateTrainerUserTable(data, cls);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        */},
    
    /*~~~~ GENERATE TABLE ELEMENTS OF USERS FROM JSON moduleLIST~~~~*/
        /*generateTrainerUserTable: function (data, cls) {
            //get table id from jquery
            for (var i=0; i< data.length; i++){
                userElement.append(
                '<tr id="module'+data[i].uniId+'">' +
                    '<td>'+data[i].employeeId+'</td>' +
                    '<td>'+data[i].name+'</td>' +
                    '<td>'+data[i].email+'</td>' +
                    '<td>'+data[i].branchCode+'</td>' +
                    '<td><button class="trainerUseCls btn btn-default" style="color:#00f;"> Edit </button></td>' +
                    '<td><div class="'+cls  +' btn btn-outline btn-primary"> Remove </div></td>' +
                '</tr>');
            }
            
            $("#usertable").dataTable({
                "ordering": false
            });
        },*/
        
        /*Hierarchy Details Data Table */
        heirarchyListForModule:function(urls, cls) {
            Backbone.$("#userTableInherit").empty().append(
                '<table id="usertable" class="table table-hover">' +
                    '<thead>' +
                        '<tr>' +
                            '<td><b>Employee Id</b></td>' +
                            '<td><b>Name</b></td>' +
                            '<td><b>E-mail</b></td>' +
                            '<td><b>Manager Id</b></td>' +
                            '<td><b>Employee Designation</b></td>' +
                            '<td><input type="checkbox" class="useAll-check"><button type="button" class="'+cls+' btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></button></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="errorClss errorUserCheck" colspan="7"><b>Select Atleast One Candidates </b></td>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                '</table>'
            );
            
            
            var heirarchyTable;
            selected = [];
         
            heirarchyTable = jQuery("#usertable").dataTable({

                "iDisplayLength": 10,
                "bProcessing" : true,
                "bServerSide" : true,
                "sAjaxSource" :urls,
                "bSort" : false,
                "sServerMethod": "POST",
                /*"columnDefs": [ {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<input type='checkbox' class='user-check'>"
                } ],*/
                "columnDefs": [{
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<input id="check_'+full.trainerId+'" type="checkbox" class="user-check" value='+full.trainerId+'  ">';
                    }
                }],
             
               "aoColumns" : [
                   { "mData": "trainerId" },
                   { "mData": "name" },
                   { "mData": "email" },
                   { "mData": "managerId" },
                   { "mData": "designation" },
                   { "mData": " " }
               ],
               'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'emp_row_' + data.trainerId); // or whatever you choose to set as the id
                },
                "rowCallback": function( row, data ) {
                    selected=[];
                    $(".useAll-check").prop('checked',false);
                    if ( $.inArray('emp_row_' + data.trainerId, selected) !== -1 ) {
                        $(row).addClass('row_selected');
                        
                        $(row).children().children("#check_"+data.trainerId).prop('checked',true);
                    }
                }
            });
            
            $('#usertable tbody').on('click', 'tr', function () {
                var id = this.id;
                var index = $.inArray(id, selected);
         
                if ( index === -1 ) {
                    selected.push( id );
                    $("#check_"+id.split("_")[2]).prop('checked',true);
                } else {
                    selected.splice( index, 1 );
                    $("#check_"+id.split("_")[2]).prop('checked',false);
                }
         
                $(this).toggleClass('row_selected');
            } );
        },
        
        
    /*** DISPLAY STUDENTS LISTS ***/
        usersListForModule: function(urls, cls) {
            Backbone.$("#userTableInherit").empty().append(
                '<table id="usertable" class="table table-hover">' +
                    '<thead>' +
                        '<tr>' +
                            '<td><b>Student Id</b></td>' +
                            '<td><b>Name</b></td>' +
                            '<td><b>E-mail</b></td>' +
                            '<td><b>Phone</b></td>' +
                            '<td><b>Branch Code</b></td>' +
                            '<td><input type="checkbox" class="useAll-check"><button type="button" class="'+cls+' btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></button></td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="errorClss errorUserCheck" colspan="7"><b>Select Atleast One Student </b></td>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                '</table>'
            );
            
            
            var studentTable;
            selected = [];
       	 
        	studentTable = jQuery("#usertable").dataTable({

                "iDisplayLength": 10,
                "bProcessing" : true,
                "bServerSide" : true,
                "bSort" : false,
                "sAjaxSource" :urls,
                "bSort" : false,
                "sServerMethod": "POST",
                /*"columnDefs": [ {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<input type='checkbox' class='user-check'>"
                } ],*/
                "columnDefs": [{
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<input id="check_'+full.uniId+'" type="checkbox" class="user-check" value='+full.uniId+'  ">';
                    }
                }],
        	 
        	   "aoColumns" : [
                   { "mData": "employeeId" },
            	   { "mData": "name" },
                   { "mData": "email" },
                   { "mData": "phoneNum" },
                   { "mData": "branchCode" },
                   { "mData": "" }
               ],
               'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'emp_row_' + data.uniId); // or whatever you choose to set as the id
                },
                "rowCallback": function( row, data ) {
                	selected=[];
                	$(".useAll-check").prop('checked',false);
                    if ( $.inArray('emp_row_' + data.uniId, selected) !== -1 ) {
                        $(row).addClass('row_selected');
                        
                        $(row).children().children("#check_"+data.uniId).prop('checked',true);
                    }
                }
            });
        	
        	$('#usertable tbody').on('click', 'tr', function () {
                var id = this.id;
                var index = $.inArray(id, selected);
         
                if ( index === -1 ) {
                    selected.push( id );
                    $("#check_"+id.split("_")[2]).prop('checked',true);
                } else {
                    selected.splice( index, 1 );
                    $("#check_"+id.split("_")[2]).prop('checked',false);
                }
         
                $(this).toggleClass('row_selected');
            } );
            
            
            
/*            Backbone.$("#usertable").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: urls,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0){
                        
                    } else{
                        userElement = Backbone.$("#usertable").children("tbody").empty();
                        usermanageindex.generateUsersTable(data, cls);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
    
    /*~~~~ GENERATE TABLE ELEMENTS OF USERS FROM JSON moduleLIST~~~~*/
        /*generateUsersTable: function (data, cls) {
            //get table id from jquery
            for (var i=0; i< data.length; i++){
                userElement.append(
                '<tr id="module'+data[i].uniId+'">' +
                    '<td>'+data[i].employeeId+'</td>' +
                    '<td>'+data[i].name+'</td>' +
                    '<td>'+data[i].email+'</td>' +
                    '<td>'+data[i].branchCode+'</td>' +
                    '<td><input value='+data[i].uniId+' type="checkbox" class="user-check"></td>' +
                '</tr>');
            }
            
            $("#usertable").dataTable({
                            "ordering": false
                        });
        },*/
        
    /*** DISPLAY BACK OFFICE ADMIN  ***/
        backOfficeUserForModule: function(urls) {
            
            Backbone.$("#userTableInherit").empty().append(
                '<table id="usertable" class="table table-hover">' +
                    '<thead>' +
                        '<tr>' +
                            '<td><b>Group Id</b></td>' +
                            '<td><b></b></td>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                '</table>'
            );

            var backOfficeUserTabel;
             
             
            backOfficeUserTabel = jQuery("#usertable").dataTable({
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" :urls,
            "bSort" : false,
            /*"sAjaxDataProp":"",*/
             "columnDefs": [ {
                 "targets": -1,
                 "data": null,
                 "defaultContent": "<button class='removeuser btn btn-outline btn-primary' > Remove </button> "
             } ],
             
            "aoColumns" : [
                           { "mData": "groupId" },
                           { "mData": "" }
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'module' + data.groupId); // or whatever you choose to set as the id
                },
            });


            /*Backbone.$("#usertable").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: urls,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0){
                        
                    } else{
                        for (var i=0; i< data.length; i++){
                            Backbone.$("#usertable").children("tbody").append(
                            '<tr id="module'+data[i].groupId+'">' +
                                '<td>'+data[i].groupId+'</td>' +
                                '<td><div class="removeuser btn btn-outline btn-primary"> Remove </div></td>' +
                            '</tr>');
                        }
                        $("#usertable").dataTable({
                            "ordering": false
                        });
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
        
        
        removeTrainerModule: function(e) {
            $(".errorInRem").remove();
            var unID = e.currentTarget.parentElement.parentElement.id.slice(6);
            swal({
                title: "Remove following trainer from module",
                confirmButtonColor: "#F01E1E",
                confirmButtonText: "Yes",
                showCancelButton: true,
                cancelButtonText: "No"
            },
                 function(){
                    $("#pageloading").show();
                    Backbone.ajax({
                        url: "removetrainermodule?uniId="+unID,
                        data: "",
                        type: "POST",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(data) {
                            
                            if(data == 1){
                                var url = "listoftrainers?moduleId="+mid;
                                usermanageindex.trainersListForModule(url, "trainerCls");
                            } else if(data == 2){
                                $(e.currentTarget.parentElement).append('<div class="errorClss errorInRem">Failed. Try Again</div>');
                                $(".errorInRem").show();
                            } else {
                                alert("Contact IT team");
                            }
                        },
                        error:function(res,ioArgs){
                            if(res.status == 403){
                                window.location.reload();
                            }
                        },
                complete: function() {
                    $("#pageloading").hide();
                }
                    });
            });
        },
        
    /*REMOVE INERNAL STUDENT FROM THE MODULE*/
        removeInternalStdModule: function(e) {
            $(".errorClss").hide();
            $(".errorInRem").remove();
            var unID = e.currentTarget.parentElement.parentElement.id.slice(6);
            swal({
                title: "Remove internal student from module",
                confirmButtonColor: "#F01E1E",
                confirmButtonText: "Yes",
                showCancelButton: true,
                cancelButtonText: "No"
            },
                 function(){
                var useUnid = [];
                var remAll;
                if($('.useAll-check').is(':checked')){
                    remAll = 1;
                   // var userChecked = sele;
                    for (var i = 0, length = selected.length; i < length; i++) {
                            useUnid.push(selected[i].split("_")[2]);
                    }
                } else {
                    remAll = 2;
                    var userChecked = $(".user-check");
                    for (var i = 0, length = selected.length; i < length; i++) {
                        useUnid.push(selected[i].split("_")[2]);
                    }
                }
                if($('.user-check').is(':checked')){
                    $("#pageloading").show();
                    Backbone.ajax({
                        url: "removeinternalstudentmodule?removeAll=2&uniId="+useUnid,
                        data: "",
                        type: "POST",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(data) {
                        	selected=[];
                            if(data == 1){
                                var url = "listofinternalstudents?moduleId="+mid;
                                usermanageindex.usersListForModule(url, "interCls");
                            } else if(data == 2){
                                $(e.currentTarget.parentElement).append('<div class="errorClss errorInRem">Failed. Try Again</div>');
                                $(".errorInRem").show();
                            } else {
                                alert("Contact IT team");
                            }
                        },
                        error:function(res,ioArgs){
                            if(res.status == 403){
                                window.location.reload();
                            }
                        },
                        complete: function() {
                            $("#pageloading").hide();
                        }
                    });
                } else {
                    $(".errorUserCheck").show();
                }
            });
        },
        
        
        //removing heirarchy module
        removehierarchybyModule:function(e) {
            $(".errorClss").hide();
            $(".errorInRem").remove();
            var unID = e.currentTarget.parentElement.parentElement.id.slice(6);
            swal({
                title: "Remove Trainers from hierarchy",
                confirmButtonColor: "#F01E1E",
                confirmButtonText: "Yes",
                showCancelButton: true,
                cancelButtonText: "No"
            },
                 function(){
                var useUnid = [];
                var remAll;
                if($('.useAll-check').is(':checked')){
                    remAll = 1;
                   // var userChecked = sele;
                    for (var i = 0, length = selected.length; i < length; i++) {
                            useUnid.push(selected[i].split("_")[2]);
                    }
                } else {
                    remAll = 2;
                    var userChecked = $(".user-check");
                    for (var i = 0, length = selected.length; i < length; i++) {
                        useUnid.push(selected[i].split("_")[2]);
                    }
                }
                if($('.useAll-check').is(':checked') | $('.user-check').is(':checked')){
                    $("#pageloading").show();
                    Backbone.ajax({
                        url: "removehierarchymodule?removeAll=2&uniId="+useUnid+"&moduleId="+mid,
                        data: "",
                        type: "POST",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(data) {
                            selected=[];
                            if(data == 1){
                                var url = "paginlistofheirarchybymodule?moduleId="+mid;
                                usermanageindex.heirarchyListForModule(url, "heirarchyCls");
                            } else if(data == 2){
                                $(e.currentTarget.parentElement).append('<div class="errorClss errorInRem">Failed. Try Again</div>');
                                $(".errorInRem").show();
                            } else {
                                alert("Contact IT team");
                            }
                        },
                        error:function(res,ioArgs){
                            if(res.status == 403){
                                window.location.reload();
                            }
                        },
                        complete: function() {
                            $("#pageloading").hide();
                        }
                    });
                } else {
                    $(".errorUserCheck").show();
                }
            });
        },
        
    /*REMOVE EXTERNAL STUDENT*/
        removeExternalStudentModule: function(e) {
            $(".errorInRem").remove();
            var unID = e.currentTarget.parentElement.parentElement.id.slice(6);
            swal({
                title: "Remove external student from module",
                confirmButtonColor: "#F01E1E",
                confirmButtonText: "Yes",
                showCancelButton: true,
                cancelButtonText: "No"
            },
                 function(){
                var useUnid = [];
                var remAll;
                if($('.useAll-check').is(':checked')){
                    remAll = 1;
                    var userChecked = $(".user-check");
                    for (var i = 0, length = userChecked.length; i < length; i++) {
                        if ($(userChecked[i]).is(':checked')) { 
                            useUnid.push(userChecked[i].value);
                        }
                    }
                } else {
                    remAll = 2;
                    var userChecked = $(".user-check");
                    for (var i = 0, length = userChecked.length; i < length; i++) {
                        if ($(userChecked[i]).is(':checked')) { 
                            useUnid.push(userChecked[i].value);
                        }
                    }
                }
                if($('.user-check').is(':checked')){
                    $("#pageloading").show();
                    Backbone.ajax({
                        url: "removeexternalstudentmodule?removeAll="+remAll+"&uniId="+useUnid,
                        data: "",
                        type: "POST",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(data) {
                            
                            if(data == 1){
                                var url = "listofexternalstudents?moduleId="+mid;
                                usermanageindex.usersListForModule(url, "exterCls");
                            } else if(data == 2){
                                $(e.currentTarget.parentElement).append('<div class="errorClss errorInRem">Failed. Try Again</div>');
                                $(".errorInRem").show();
                            } else {
                                alert("Contact IT team");
                            }
                        },
                        error:function(res,ioArgs){
                            if(res.status == 403){
                                window.location.reload();
                            }
                        },
                complete: function() {
                    $("#pageloading").hide();
                }
                    });
                } else {
                    $(".errorUserCheck").show();
                }
            });
        },
        
        /**/
        removeBOModule: function(e) {
            $(".errorInRem").remove();
            var unID = e.currentTarget.parentElement.parentElement.id.slice(6);
            swal({
                title: "You want to remove back office user from module",
                confirmButtonColor: "#F01E1E",
                confirmButtonText: "Yes",
                showCancelButton: true,
                cancelButtonText: "No"
            },
                 function(){
                    $("#pageloading").show();
                Backbone.ajax({
                        url: "removebogroup?uniId="+unID,
                        data: "",
                        type: "POST",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(data) {
                            
                            if(data == 1){
                                var url = "paginlistofbackofficeuser?moduleId="+mid;
                                usermanageindex.backOfficeUserForModule(url);
                            } else if(data == 2){
                                $(e.currentTarget.parentElement).append('<div class="errorClss errorInRem">Failed. Try Again</div>');
                                $(".errorInRem").show();
                            } else {
                                alert("Contact IT team");
                            }
                        },
                        error:function(res,ioArgs){
                            if(res.status == 403){
                                window.location.reload();
                            }
                        },
                complete: function() {
                    $("#pageloading").hide();
                }
                    });
            });
        },
        
        extStudentDetailsSubmit: function(e) {
            $(".errorClss").hide();
            var mid = $("#module_name").attr('value');
            if($('#fname').val() == 0) {
                $(".errorExtFNMan").show();
            } else if ($('#exurid').val() == 0){
                $(".errorExtUrIdMan").show();
            } else if ($('#brcd').val() == 0){
                $(".errorBRCDMan").show();
            } else {
            
                var json = [];
                
                var fn = $("#fname").val();
                var mn = $("#mname").val();
                var ln = $("#lname").val();
                var eml = $("#useremail").val();
                var phno = $("#userphone").val();
                var brcd = $('#brcd').val();
                var urid = $('#exurid').val();
                try{
                    json.push({"urn":urid,"branchcode":brcd,"firstName":fn,"lastName":ln,"middleName":mn,"emailId":eml,"phNumber":phno});
                }catch(e){}
                
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addexternalstudentmodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        
                        if(data == 1){
                            window.location.reload();
                            /*var extTriggerOnceFlag = true;
                            $("#createnewuser").modal('hide');
                            $("#createnewuser").on('hidden.bs.modal', function(){
                                if(extTriggerOnceFlag){
                                    extTriggerOnceFlag = false;
                                    usermanageindex.externalStudentListUpload();
                                }
                            });*/
                        } else {
                            $("#createnewuser").modal('hide');
                            $(".errorExtStuSub").show();
                        }
                        
                    },
                    error:function(res,ioArgs){
                        
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
    
        
    /* URN CHECKING FOR EXTERNAL STUDENTS */
        checkForUrnPresent: function() {
            
            $("#pageloading").show();
            Backbone.ajax({
                url: "checkurnid?urnid="+$("#exurid").val(),
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    
                    if(data == 1){
                        $("#exurid").parent().children('p').remove();
                        $(".exStudSub").prop('disabled',false);
                    } else if(data == 2){
                        $("#exurid").parent().children('p').remove();
                        $("#exurid").parent().append('<p style="color: red; float: right;">This URN already present in the application</p>');
                        $(".exStudSub").prop('disabled',false);
                    } else{
                        alert("Contact IT team");
                    }
                },
                error:function(res,ioArgs){
                    
                    $('#studentempid').val("");
                    $('#studentempid').val("");
                    $('#studentbc').val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        blockSubmitButton: function() {
            $(".exStudSub").prop('disabled',true);
        },
        
    /* TRAINER BRANCH MAPING MODULE */
        
        loadTrainerBrachMaping: function(e) {
            var trinerUnId = e.currentTarget.parentElement.parentElement.id.slice(6);
            landingroutes.navigate('branchmap/' + trinerUnId, {trigger: true});
        },
        
        
        trainerBranchMap: function(uid) {
            trinerUnid = uid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="config_module" class="maind" data-toggle="tooltip tab" data-original-title="Module Configuration">&nbsp;&nbsp;<i class="fa fa-file-excel-o fa-fw"></i>' +
                                '<span class="content-hide">Configuration</span>' +
                            '</li>' +
                            '<li id="user_manage" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                                '<span class="content-hide">User Management</span>' +
                            '</li>' +
                            '<li id="batchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="ques_master" class="maind" data-toggle="tooltip tab" data-original-title="Question Bank">&nbsp;&nbsp;<i class="fa fa-pencil fa-fw"></i>' +
                                '<span class="content-hide">Question Bank</span>' +
                            '</li>' +
                            '<li id="admin_attedance" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="paper_create" class="maind" data-toggle="tooltip tab" data-original-title="Test Management">&nbsp;&nbsp;<i class="fa fa-sitemap fa-fw"></i>' +
                                '<span class="content-hide">Test Management</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
            
            if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">'+
                '<div class="col-xs-12">'+
                    '<h1 class="page-header">Trainer Branch Mapping <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>'+
                '</div>'+
                '<!-- /.col-xs-12 -->'+
            '</div>'+
            '<div class="row">'+
                '<div class="col-xs-6">'+
                    /*'<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<label class="control-label">Upload branch mapping list </label><span class="clsRed">*</span>' +
                            '<div class="col-xs-1" style="left:64%; top:-8px;">' +
                                '<form method="post" enctype="multipart/form-data">'+
                                    '<div class="btn btn-outline btn-primary btn-file">'+
                                        '<i class="fa fa-upload">'+
                                        '</i> &nbsp;Upload <input name="filename" type="file" class="branchmapfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="left: 16px;width: 90px;">'+
                                    '</div>'+
                                '</form>'+
                            '</div>'+
                            '<div class="errorClss errorbrmpFailed">Uploading Failed. Try Again...!</div>' +
                        '</div>'+
                    '</div>'+*/
                    '<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<div class="panel panel-default">' +
                                '<div class="panel-heading">' +
                                    '<h4>Trainer Assigned Branches</h4>' +
                                '</div>' +
                                '<div class="panel-body">' +
                                    '<div class="table-responsive">' +
                                        '<table id="selectedBranches" class="table table-hover">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<td><b>Branch Code</b></td>' +
                                                    '<td><b>Branch Name</b></td>' +
                                                    '<td><input type="checkbox" class="rem-Allbranch"><button type="button" class="removebranch btn btn-danger btn-circle"><i class="fa fa-chain-broken"></i></button></td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                    '<td class="errorClss errorMinOne" colspan="3"><b>Select Atleast One Branch </b></td>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '</tbody>' +
                                        '</table>' +
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<!-- /.col-xs-6 -->'+
                '<div class="col-xs-6">'+/*
                    '<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<span><a href="trainerbranchuploadtemplate" target="_blank">Click Here </a>to download excel template of trainer branch mapping</span>' +
                        '</div>'+
                    '</div>'+*/
                    '<div class="row">' +
                        '<div class="col-xs-12">' +
                            '<div class="panel panel-default">' +
                                '<div class="panel-heading">' +
                                    '<h4>All Branches</h4>' +
                                '</div>' +
                                '<div class="panel-body">' +
                                    '<div class="table-responsive">' +
                                        '<table id="allBranches" class="table table-hover">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<th> Branch Code </th>' +
                                                    '<th> Branch Name </th>' +
                                                    '<th> <input type="checkbox" class="add-Allbranch"> <button type="button" class="addbranch btn btn-success btn-circle"> <i class="fa fa-link"></i> </button></th>' +
                                                '</tr>' +
                                                '<tr>' +
                                                    '<td class="errorClss errorMiOne" colspan="3"><b>Select Atleast One Branch </b></td>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '</tbody>' +
                                        '</table>' +
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<!-- /.col-xs-6 -->'+
            '</div>'
            );
            
            usermanageindex.loadMappedBranches(uid);
        },
        
    /* MAPPED BRACHES DISPLAY */  
        loadMappedBranches: function(uid) {
/*
            jQuery("#selectedBranches").dataTable({
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" :"gettrainerbranches?trainerId="+uid,
            "sServerMethod": "POST",

             "columnDefs": [{
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" class="rem-branch" value='+full.mapId+'>';
                    }
                }],
            "aoColumns" : [
                           { "mData": "branchCode" },
                           { "mData": "branchName" },
                           { "mData": "" }
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'module' + data.mapId); // or whatever you choose to set as the id
                },
            });
            
            */
            usermanageindex.loadAllBranches(uid);
            Backbone.$("#selectedBranches").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "gettrainerbranches?trainerId="+uid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0){
                       // usermanageindex.loadAllBranches(uid);
                    } else{
                        for (var i=0; i< data.length; i++){
                            Backbone.$("#selectedBranches").children("tbody").append(
                            '<tr id="branch'+data[i].mapId+'">' +
                                '<td>'+data[i].branchCode+'</td>' +
                                '<td>'+data[i].branchName+'</td>' +
                                '<td><input type="checkbox" class="rem-branch" value='+data[i].mapId+'></td>' +
                            '</tr>');
                        }
                       // usermanageindex.loadAllBranches(uid);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    
    /* ALL BRACHES DISPLAY */
        loadAllBranches: function(uid) {

            /*jQuery("#allBranches").dataTable({
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" :"getbranches?trainerId="+uid,
            "sServerMethod": "POST",

             "columnDefs": [{
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" class="add-branch" value='+full.branchId+'>';
                    }
                }],
            "aoColumns" : [
                           { "mData": "branchCode" },
                           { "mData": "branchName" },
                           { "mData": "" }
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'module' + data.branchId); // or whatever you choose to set as the id
                },
            });

*/
            Backbone.$("#allBranches").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getbranches?trainerId="+uid,
                data: "",
                type: "POST",
                searchable:"true",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0){
                        
                    } else{
                        for (var i=0; i< data.length; i++){
                            Backbone.$("#allBranches").children("tbody").append(
                            '<tr id="branch'+data[i].branchId+'">' +
                                '<td>'+data[i].branchCode+'</td>' +
                                '<td>'+data[i].branchName+'</td>' +
                                '<td><input type="checkbox" class="add-branch" value='+data[i].branchId+'></td>' +
                            '</tr>');
                        }
                    }
                    //$("#allBranches").dataTable();
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
    /* Adding Branch To Trainer */
        
        addBranchToTrainer: function(e) {
            $(".errorClss").hide();
            var json = [];
            var addAll;
            if($('.add-Allbranch').is(':checked')){
                addAll = 1;
                var userChecked = $(".add-branch");
                for (var i = 0, length = userChecked.length; i < length; i++) {
                    if ($(userChecked[i]).is(':checked')) {
                        json.push({"branchId":userChecked[i].value});
                    }
                }
            } else {
                addAll = 2;
                var userChecked = $(".add-branch");
                for (var i = 0, length = userChecked.length; i < length; i++) {
                    if ($(userChecked[i]).is(':checked')) {
                        json.push({"branchId":userChecked[i].value});
                    }
                }
            }
            if($(".add-branch").is(':checked')){
                $("#pageloading").show();
                Backbone.ajax({
                    url: "maptrainertobranches?addAll="+addAll+"&trainerId="+trinerUnid,
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        
                        if(data == 1){
                            window.location.reload();
                        } else if(data == 2){
                            sweetAlert("Problem in adding branch. Try Again..!")
                        } else {
                            alert("Contact IT team");
                        }
                    },
                    error:function(res,ioArgs){
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                    
                }
                });
            } else {
                $(".errorMiOne").show();
            }
        },
        
        /*Uploding hierarchy by excel */
        hierarchyBrnachMapListUpload:function(e){
            $("#pageloading").show();
            $(".errorClss").hide();
            var urlv = "heirarchybranchupload?"+csrf_url+"="+token+"&moduleId="+mid;
            $(e.currentTarget).parent().parent().prop("action",urlv).ajaxForm({
                success : function(res,data) {
                    var pf = $(".heirarchybranchmapfile").parent();
                    $(".heirarchybranchmapfile").remove();
                    $(pf).append('<input name="filename" type="file" class="heirarchybranchmapfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    if(res == 1){
                        sweetAlert("Heirarchy branch mapping uploaded successfully");
                        usermanageindex.HeirarchyListupload();
                    } else if(res == 2){
                    	 $(".errorUploadingFailed").show();
                    } /*else if(res == 3){
                    	sweetAlert("Employee ID already mapped to a Manager ID. Please first delink it in order to proceed...!");
                    }*/else{

                        swal({
                            title: res,
                            showCancelButton: true,
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Yes",
                            cancelButtonText: "No"
                        },
                             function(isConfirm){
                            if (isConfirm) {
                         	   usermanageindex.linknewdataafterdelinkingforupload();
                            } 
                        });
                    
                    }
                },
                error:function(res,ioArgs){
                    $(".heirarchybranchmapfile").val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
            
        },
        
    /* makinf Check Box True */
        selRembranch: function(){
            if($('.rem-Allbranch').is(':checked')){
                $(".rem-branch").prop('checked',true);
            } else if(!$('.rem-Allbranch').is(':checked')) {
                $(".rem-branch").prop('checked',false);
            }
        },
        
        selAddbranch: function(){
            if($('.add-Allbranch').is(':checked')){
                $(".add-branch").prop('checked',true);
            } else if(!$('.add-Allbranch').is(':checked')) {
                $(".add-branch").prop('checked',false);
            }
        },
        
    /* Removeing Branch From Trainer */
        
        removeBranchToTrainer: function(e) {
            $(".errorClss").hide();
            var brid = [];
            var remAll;
            if($('.rem-Allbranch').is(':checked')){
                remAll = 1;
                var userChecked = $(".rem-branch");
                for (var i = 0, length = userChecked.length; i < length; i++) {
                    if ($(userChecked[i]).is(':checked')) { 
                        brid.push(userChecked[i].value);
                    }
                }
            } else {
                remAll = 2;
                var userChecked = $(".rem-branch");
                for (var i = 0, length = userChecked.length; i < length; i++) {
                    if ($(userChecked[i]).is(':checked')) { 
                        brid.push(userChecked[i].value);
                    }
                }
            }
            if($(".rem-branch").is(':checked')){
                $("#pageloading").show();
                Backbone.ajax({
                    url: "removemaptrainerbranches?removeAll="+remAll+"&trainerId="+trinerUnid+"&mapId="+brid,
                    data: "",
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        
                        if(data == 1){
                            window.location.reload();
                        } else if(data == 2){
                            sweetAlert("Problem in removing branch. Try Again..!")
                        } else {
                            alert("Contact IT team");
                        }
                    },
                    error:function(res,ioArgs){
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            } else {
                $(".errorMinOne").show();
            }
        },
        
    /*** TRAINERS BRANCH MAPPING LIST UPLOAD ***/
        trainerBrnachMapListUpload:function(e){
            $("#pageloading").show();
            $(".errorClss").hide();
            var urlv = "trainerbranchupload?"+csrf_url+"="+token;
            $(e.currentTarget).parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".branchmapfile").parent();
                    $(".branchmapfile").remove();
                    $(pf).append('<input name="filename" type="file" class="branchmapfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    if(data == 1){
                        sweetAlert("Trainer branch mapping uploaded successfully");
                    } else if(data == 2){
                    	sweetAlert("Uploading Failed. Try Again...!"); 
                    } else{
                        alert("Contact IT team");
                    }
                },
                error:function(res,ioArgs){
                    $(".branchmapfile").val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
            
        },
        /*** enable trainer to add student in batch ***/
       
        addStuByTrainer: function(e) {
            $(".errorClss").hide();
            var json = [];
            var addAll;
            if($('.add-Allbranch').is(':checked')){
                addAll = 1;
                var userChecked = $(".user-check");
                for (var i = 0, length = userChecked.length; i < length; i++) {
                    if ($(userChecked[i]).is(':checked')) {
                        json.push({"loginUserDetail":userChecked[i].value});
                    }
                }
            } else {
                addAll = 2;
                var userChecked = $(".user-check");
                for (var i = 0, length = userChecked.length; i < length; i++) {
                    if ($(userChecked[i]).is(':checked')) {
                        json.push({"loginUserDetail":userChecked[i].value,"addStudentCheck":1});
                    }else{
                    	json.push({"loginUserDetail":userChecked[i].value,"addStudentCheck":0});
                    }
                }
            }
            if(true){
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addStudentCheck",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        
                        if(data == 1){
                        	sweetAlert("Add Student Checked Successfully");
                        } else if(data == 2){
                            sweetAlert("Problem in adding Trainer. Try Again..!");
                        } else {
                            alert("Contact IT team");
                        }
                    },
                    
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            } else {
                $(".errorMiOne").show();
            }
        },
    });
})();
