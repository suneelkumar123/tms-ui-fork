(function () {
    
    /* GLOBAL VARIABLES FOR THIS BATCH MANEGMENT */
    var batchElement, listofTrainers, listofStudents, trainersIn, studentsIn, studentlistIn, baid, batchAttendenceFlag, batchStuAten, mid;
    var json = [];
    var studentFlag = false;
    var reloadErrorFlag = false;
    var selected=[];
    var preDateArray=[];
    var adminmodulestatus=0;
    var anOpen =[];
    
    /* GLOBAL CALLING ALL BACKBONE MVC */
    window.batch_manage = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    
    /*CREATING VIEW FOR BATCH MANAGEMENT */
    batch_manage.Views.header = Backbone.View.extend({
        
        events: {
            'click .addnewbatch': 'createNewBatch',
            'click .editbatch': 'editBatchDetails',
            'click .btdetails': 'batchDetails',
            'click .batchsubmit': 'newBatchSubmit',
            'click .editbatchsubmit': 'editBatchSubmit',
            'click .alertOk': 'clickAlertOk',
            'change #batchvalidto': 'validateEndDate',
            'click .viewexpenses': 'viewExpensesDetails',
            'click .updateexpensessubmit': 'updateExpensesSubmit',
            'keypress .integervalid': 'testTimingValidation',
            'click  #rulecheck':'expensesView',
            'keypress #allownumbers':'testTimingValidation',
            'click #batchtable tbody tr td': "btachRowAccordian",
            'click #userTableInherit tbody tr td': "btachRowAccordian",
            'change .searchdatepic' :'getselecteddate'
            
        },
        
        initialize: function () {
            _.bindAll(this, 'render');
        },
        
        render: function (modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="config_module" class="maind" data-toggle="tooltip tab" data-original-title="Module Configuration">&nbsp;&nbsp;<i class="fa fa-file-excel-o fa-fw"></i>' +
                                '<span class="content-hide">Configuration</span>' +
                            '</li>' +
                            '<li id="user_manage" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                                '<span class="content-hide">User Management</span>' +
                            '</li>' +
                            '<li id="batchmanagement" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="ques_master" class="maind" data-toggle="tooltip tab" data-original-title="Question Bank">&nbsp;&nbsp;<i class="fa fa-pencil fa-fw"></i>' +
                                '<span class="content-hide">Question Bank</span>' +
                            '</li>' +
                            '<li id="admin_attedance" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="paper_create" class="maind" data-toggle="tooltip tab" data-original-title="Test Management">&nbsp;&nbsp;<i class="fa fa-sitemap fa-fw"></i>' +
                                '<span class="content-hide">Test Management</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header"> List of batches <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div id="batch_wrapper">' +
                '<div class="row">' +
                    '<div class="col-xs-3" style="margin-bottom: 20px;">' +
                        '<button class="addnewbatch btn btn-default" type="button">' +
                            '<i class="fa fa-calendar"></i>  &nbsp;Add New Batch' +
                        '</button>' +
                        
                    '</div>' +
                    '<div class="col-xs-9">' +
                        '<span> <a href="excelreport" target="_blank"> Click Here</a> download module report</span>'+
                        '<div class="pull-right errorClss errorBatchName">Batch Selection Failed. Try Again..!</div>'+
                    '</div>'+
                    '<div class="col-xs-9">' +
                    '<span> <a href="excelreportofallbatch" target="_blank"> Click Here</a> download module report batchwise</span>'+
                    '<div class="pull-right errorClss errorBatchName">Batch Selection Failed. Try Again..!</div>'+
                    '</div>'+
                    '<div class="batch_calendar">' +
                    	'<span">Search by date:</span>'+
//                    	'<input  readonly="readonly" size="8" type="text" class="searchdatepic fa fa-calendar">'+
                    	'<input id="fromdate" readonly="readonly" size="8" type="text" class="datepicker searchdatepic fa fa-calendar" placeholder="From">'+
                    	'<span class=" errorClss errorFromDate">Select From Date..</span>'+
                    	'<input id="todate" readonly="readonly" size="8" type="text" class="datepicker searchdatepic fa fa-calendar" placeholder="To">'+
                    	'<span class="errorClss errorToDate">Select To Date..</span>'+
                    '</div>'+
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body" id="batchtablehide">' +
                                '<div class="table-responsive">' +
                                    '<table id="batchtable" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                            '<td> </td>' +
                                            '<td><b>Batch Name</b></td>' +
                                            /*'<td><b>Start Date</b></td>' +
                                            '<td><b>End Date</b></td>' +
                                            '<td><b>Lunch Expenses</b></td>' +
                                            '<td><b>Miscellaneous Expenses</b></td>' +
                                            '<td><b>Travelling Expenses</b></td>' +*/
                                            /*'<td><b>Status</b></td>' +*/
                                            '<td><b>Edit</b></td>' +
                                            '<td><b>No of Students</b></td>' +
                                            /*'<td><b>Candidates Appeared</b></td>' +
                                            '<td><b>Candidates Passed</b></td>' +
                                            '<td><b>Candidates Failed</b></td>' +
                                            '<td><b>Passing %</b></td>' +*/
                                            '<td><b>Candidates Pending</b></td>' +
                                            '<td><b>Batch Report</b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-xs-12" id="fromandenddate">' +
                    '<div class="panel panel-default", style="margin-top:30px">' +
                        '<div class="panel-heading">' + 
                            '<h4 style="margin-top:0; margin-bottom:0; text-align: center;" class="user_heading"></h4>' +
                        '</div>' +
                        '<div class="panel-body">' +
                            '<div class="tab-content" id="userTableInherit">' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'+
            '</div>' +
            '<div class="modal fade" id="createnewbatch" tabindex="-1" role="dialog" aria-labelledby="createnewbatch" aria-hidden="true" data-backdrop="static" style="margin-top:5%">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Add New Batch </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                    '<form role="form" id="batchform">' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-12">' +
                                            '<label for="batchname" class="control-label">Name of batch</label><span class="clsRed">*</span>' +
                                            '<input id="batchname" type="text" name="batch-name" class="form-control">' +
                                            '<div class="errorClss errorBatchNameReq"> Batch Name Required </div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-6">' +
                                            '<label for="batchvalidfrom" class="control-label">Batch start date</label><span class="clsRed">*</span>' +
                                            '<input id="batchvalidfrom" type="text" name="batch-validfrom" class="form-control" readonly>' +
                                            '<div class="errorClss errorBatchStar"> Required </div>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-6">' +
                                            '<label for="batchvalidto" class="control-label">Batch end date</label><span class="clsRed">*</span>' +
                                            '<input id="batchvalidto" type="text" name="batch-validto" class="form-control" readonly>' +
                                            '<div class="errorClss errorBatchEnd"> Required </div>' +
                                            '<div class="errorClss errorEndDate"> Batch End Should be After Batch Start </div>' +
                                        '</div>' +
                                    '</div>' +
                                '<div class="row">' + 
                                  '<div class ="form-group col-xs-6">' +
                                    '<label for="questionbox" class="control-label">Question Bank</label>' +
                                    '<select id="questionbox" type="text" name="question-name" class="form-control">' +
                                
                                    '</select>' +
                                    '<div class="errorClss errorQuestionReq">selection of Question is Required  </div>' +
                                  '</div>' +
                                '</div>' + 
                                     '<hr/>'+
                                       '<h1> EXPENSES </h1>' +
                                     '<hr/>' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-6">' +
                                            '<label for="actualexpenses" class="control-label">Actuals</label>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-6">' +
                                            '<label for="approvedexpenses" class="control-label">HO Approved</label><span class="clsRed">     </span><input  id="rulecheck" type="checkbox" class="useruseCls" />' +
                                        '</div>' +
                                    '</div>' +
                                    '<div id="allownumbers">'+ 
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-3">' +
                                            '<label for="travellingexpenses" class="control-label">Travelling</label>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4">' +
                                           '<input id="travellingexpenses" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="travelling-expenses" class="form-control" readonly>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4" id="approvedtrexpens">' +
                                            '<input id="aprovedtravelling" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="ho-approved" class="form-control" >' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-3">' +
                                            '<label for="lunchexpenses" class="control-label">Lunch</label>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4">' +
                                            '<input id="lunchexpenses" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="lunch-expenses" class="form-control" readonly>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4" id="approvedluexpens">' +
                                            '<input id="aprovedlunch" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="ho-approved" class="form-control" >' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="form-group-control col-xs-3">' +
                                            '<label for="miscellaneousexpenses" class="control-label">Miscellaneous</label>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4">' +
                                            '<input id="miscellaneousexpenses" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="miscellaneous-expenses" class="form-control" readonly>' +
                                        '</div>' +
                                        '<div class="form-group-control col-xs-4" id="approvedmiexpens">' +
                                            '<input id="aprovedmiscellaneous" type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="ho-approved" class="form-control">' +
                                        '</div>' +
                                    '</div>' +
                                 '</div>' +
                              '</form>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="errorClss errorOnCreSub pull-left"> Submitting Error, Try Later..! </div>' +
                            '<button class="btn btn-default" align="center" data-dismiss="modal"> Cancel </button>' +
                            '<button class="batchsubmit btn btn-default"> Submit </button>' +
                            '<button class="editbatchsubmit btn btn-default"> Submit </button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'+
          //------------------------------------------------- start---------------------------------------------------//

            '<div class="modal fade" id="expenses" tabindex="-1" role="dialog" aria-labelledby="expenses" aria-hidden="true" data-backdrop="static" style="margin-top:5%">' +
               '<div class="modal-dialog">' +
                   '<div class="modal-content" style="border:10px solid #003264">' +

                       '<div class="modal-header">' +
                           '<h4 class="modal-title"> Expenses </h4>' +
                       '</div>' +

                       '<div class="modal-body">' +
                           '<div class="panel-body">' +
                               '<div class="row">' +
                                   '<div class="col-xs-12">' +
                                   
                                       '<form role="form" id="expensesform">' +
                                           '<div class="row">' +
                                               '<div class="form-group col-xs-10">' +
                                                   '<label for="lunchexpenses" class="control-label"> Lunch Expenses</label><span class="clsRed">*</span>' +
                                                   '<input id="lunchexpenses" type="text" name="lunch-expenses" class="integervalid form-control">' +
                                                   '<div class="errorClss errorLunchExpenses"> Please enter a valid number </div>' +
                                               '</div>' +
                                               '</div>' + 
                                               '<div class="row">' +
                                               '<div class="form-group col-xs-10">' +
                                                   '<label for="travelexpenses" class="control-label">Travel Expenses</label><span class="clsRed">*</span>' +
                                                   '<input id="travelexpenses" type="text" name="travel-expenses"  class="integervalid form-control" >' +
                                                   '<div class="errorClss errorTravelExpenses"> Please enter a valid number </div>' +
                                               '</div>' +
                                               '</div>' +
                                               '<div class="row">' +
                                               '<div class="form-group col-xs-10">' +
                                                   '<label for="missallaneousexpenses" class="control-label">Miscellaneous Expenses</label><span class="clsRed">*</span>' +
                                                   '<input id="missallaneousexpenses" type="text" name="missallaneous-expenses" class="integervalid form-control"   >' +
                                                   '<div class="errorClss errorMissallaneousExpenses"> Please enter a valid number</div>' +
                                                  // '<div class="errorClss errorEndDate"> Batch End Should be After Batch Start </div>' +
                                               '</div>' +
                                              '</div>'+
                                           /*'<div class="row beforeAdminDetails">' +
                                               '<div class="form-group">' +
                                                   '<div class="col-xs-4" style="padding: 12px">' +
                                                       '<label for="batchcheck" class="control-label" style="vertical-align: middle;">Batch active : </label>' +
                                                   '</div>' +
                                                   '<div class="col-xs-6">' +
                                                       '<div id="yes-button">' +
                                                           '<label>' +
                                                               '<input type="radio" class="batchcheck" name="batch-status" value="1">' +
                                                               '<span>YES</span>' +
                                                           '</label>' +
                                                       '</div>' +
                                                       '<div id="no-button">' +
                                                           '<label>' +
                                                               '<input type="radio" class="batchcheck" name="batch-status" value="0">' +
                                                               '<span>NO</span>' +
                                                           '</label>' +
                                                       '</div>' +
                                                   '</div>' +
                                               '</div>' +
                                           '</div>' +*/
                                       '</form>' +
                                   '</div>' +
                               '</div>' +
                           '</div>' +
                       '</div>' +
                       '<div class="modal-footer">' +
                          '<div class="integerError errorClss">It should be an Integer</div>'+
                           '<div class="errorClss errorOnCreSub pull-left"> Submitting Error, Try Later..! </div>' +
                           '<button class="btn btn-default" align="center" data-dismiss="modal"> Cancel </button>' +
                           '<button class="expensessubmit btn btn-default"> Submit </button>' +
                           '<button class="updateexpensessubmit btn btn-default"> Submit </button>' +
                       '</div>' +
                   '</div>' +
               '</div>' +
           '</div>'


//----------------------------------------------end---------------------------------------------------------------//

            );
         /*   this.changeToReset();*/
            this.batchTableCreate();
            this.dateValidation();
            this.getsearchdate();
        },
        
        validateEndDate: function(e) {
            $(".errorClss").hide();
            var baEndDate = parseInt($('#batchvalidto').val().replace(/\D+/g, ''));
            var baStartDate = parseInt($("#batchvalidfrom").val().replace(/\D+/g, ''));
            
            if(baEndDate < baStartDate){
                $(".errorEndDate").show();
            }
        },
        
        
        
      /*  
        changeToReset : function(e){
            $('#questionbox').prop('selectedIndex',0);
          
        },*/
        
        
        /*Fetch The batch From The Table*/
        batchTableCreate: function (e) {
            selected = [];
            $("#fromandenddate").hide();
        //var sImageUrl = "../DEc12layout/css/images/closebox.png";
            table1 =jQuery("#batchtable").dataTable({
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "bSort" : false,
            "sAjaxSource" : "paginlistbatchbymid?moduleId="+mid,
           
             "columnDefs": [
             {
                    'targets': 1,
                    'searchable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="btdetails" value="'+full.batchId+'"><a>'+full.batchName+'</a></button>';
                    }
                },
                {
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<a href="excelreportbatchwise?batchId='+full.batchId+'" target="_blank"> Download</a>';
                     }
                 },
               
              

                {
                    'targets': 2,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="editbatch" value="'+full.batchId+'"><a> Edit </a></button>';
                    }
                }],
            "aoColumns" : [
             {
               "mDataProp": null,
               "sClass": "control center",
               "sDefaultContent": '<i class="fa fa-plus-circle" aria-hidden="true"></i>'
               
            },
                           
                            { "mData": " " },
                            { "mData": " "},
                            { "mData": "numStudent" },
                            { "mData": "candidatePending" },
                            { "mData": "" }
                        ]
       
            });
  
            
           
        },
        
        /** fething batch details by from - to date**/
        
        /*Fetch The batch From The Table*/
        batchTableCreateForDateSearch: function (fromdate,todate) {
        	$("#batchtablehide").hide();
        	 $("#fromandenddate").show();
            Backbone.$("#userTableInherit").empty().append(
               '<table id="usertable" class="table table-hover">' +
                      '<thead>' +
                                         '<tr>' +
                                         '<td> </td>' +
                                         '<td><b>Batch Name</b></td>' +
                                       
                                         '<td><b>Edit</b></td>' +
                                         '<td><b>No of Students</b></td>' +
                                     
                                         '<td><b>Candidates Pending</b></td>' +
                                         '<td><b>Batch Report</b></td>' +
                                         '</tr>' +
                                     '</thead>' +
                                     '<tbody>' +
                                     '</tbody>' +

                '</table>');

            table1 = jQuery("#usertable").dataTable({
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "bSort" : false,
            "sAjaxSource" : "paginlistbatchbyfromdateandtodate?moduleId="+mid+"&fromdate="+fromdate+"&todate="+todate,
           
             "columnDefs": [
             {
                    'targets': 1,
                    'searchable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="btdetails" value="'+full.batchId+'"><a>'+full.batchName+'</a></button>';
                    }
                },
                {
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<a href="excelreportbatchwise?batchId='+full.batchId+'" target="_blank"> Download</a>';
                     }
                 },
               
              

                {
                    'targets': 2,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="editbatch" value="'+full.batchId+'"><a> Edit </a></button>';
                    }
                }],
            "aoColumns" : [
             {
               "mDataProp": null,
               "sClass": "control center",
               "sDefaultContent": '<i class="fa fa-plus-circle" aria-hidden="true"></i>'
               
            },
                           
                            { "mData": " " },
                            { "mData": " "},
                            { "mData": "numStudent" },
                            { "mData": "candidatePending" },
                            { "mData": "" }
                        ]
       
            });
  
            
           
        },
        
        
        
   /*     
    Fetch The batch From The Table
        batchTableCreate: function (e) {
        	selected=[];
            jQuery("#batchtable").dataTable({
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" : "paginlistbatchbymid?moduleId="+mid,
            "bSort" : false,

             "columnDefs": [{
                    'targets': 0,
                    'searchable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="btdetails" value="'+full.batchId+'"><a>'+full.batchName+'</a></button>';
                    }
                },
                {
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<a href="excelreportbatchwise?batchId='+full.batchId+'" target="_blank"> Download</a>';
                    }
                },
              //--------------------------------------------start---------------------------------------------------------------------           
                {
                    'targets': -2,
                    'searchable':false,
                    'orderable':false,
                       'data': null,
                    'render': function (data, type, full, meta){
                        return '<button class="viewexpenses"><a><i class="glyphicon glyphicon-edit"></i> View </a></button>';
                    }
                },
//---------------------------------------------------end----------------------------------------------------------------------------------
                {
                    'targets': 6,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="editbatch" value="'+full.batchId+'"><a> Edit </a></button>';
                    }
                }],
            "aoColumns" : [
                           { "mData": "" },
                           { "mData": "batchStartDate" },
                           { "mData": "batchEndDate" },
                           { "mData": "lunchExpenses" },
                           { "mData": "miselExpense" },
                           { "mData": "travellingExpense" },
                           { "mData": "active" },
                           { "mData": "" },
                        
                           { "mData": "numStudent" },
                           { "mData": "candidateAppeared" },
                           { "mData": "candidatePassed" },

                           { "mData": "candidateFailed" },
                           { "mData": "passingPercentage" },
                           { "mData": "candidatePending" },
                           { "mData": "" }

                          
                           
//---------------------------------start---------------------------------------------------------------------------                       
                         

//-----------------------------------------end---------------------------------------------------------------------------
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'batch' + data.batchId); // or whatever you choose to set as the id
                },
            });
            
           
        },*/
       
       /* batchTableCreate: function (e) {
        	selected=[];
            jQuery("#batchtable").dataTable({
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" : "paginlistbatchbymid?moduleId="+mid,

             "columnDefs": [{
                    'targets': 0,
                    'searchable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="btdetails"><a>'+full.shortDesc+'</a></button>';
                    }
                },
                {
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<a href="excelreportbatchwise?batchId='+full.batchId+'" target="_blank"> Download</a>';
                    }
                },
              //--------------------------------------------start---------------------------------------------------------------------           
                {
                    'targets': -2,
                    'searchable':false,
                    'orderable':false,
                       'data': null,
                    'render': function (data, type, full, meta){
                        return '<button class="viewexpenses"><a><i class="glyphicon glyphicon-edit"></i> View </a></button>';
                    }
                },
//---------------------------------------------------end----------------------------------------------------------------------------------
                {
                    'targets': -3,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="editbatch"><a><i class="glyphicon glyphicon-edit"></i> Edit </a></button>';
                    }
                }],
            "aoColumns" : [
                           { "mData": "" },
                           { "mData": "validFrom" },
                           { "mData": "validTo" },
                           { "mData": "" },
                           { "mData": "" },
//---------------------------------start---------------------------------------------------------------------------                       
                           { "mData": "" }

//-----------------------------------------end---------------------------------------------------------------------------
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'batch' + data.batchId); // or whatever you choose to set as the id
                },
            });
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listbatchbymid?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        swal({
                            title: "No Batch is created yet, You want to create new batch",
                            showCancelButton: true,
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Yes",
                            cancelButtonText: "No"
                        },
                             function(isConfirm){
                            if (isConfirm) {
                                batchindex.createNewBatch();
                            }
                        });
                    } else{
                        batchElement = Backbone.$("#batchtable").children("tbody").empty();
                        batchindex.generateBatchTable(data);
                        $("#batchtable").dataTable({
                            "ordering": false
                        });
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },*/

        
    /*~~~~ GENERATE TABLE ELEMENTS OF BATCHES FROM JSON BATCHLIST~~~~*/
        //generateBatchTable: function (data) {
            //get table id from jquery
            //for (var i=0; i< data.length; i++){
              //  batchElement.append(
                //    '<tr id="batch'+data[i].batchId+'">' +
                  //      '<td><button class="btdetails"><a>'+data[i].shortDesc+'</a></button></td>' +
                    //    '<td>'+data[i].validFrom+'</td>' +
                      //  '<td>'+data[i].validTo+'</td>' +
                        //'<td class="batchStatus" value='+data[i].active+'></td>' +
                        //'<td><button class="editbatch"><a><i class="glyphicon glyphicon-edit"></i> Edit </a></button></td>' +
                        //'<td><a href="excelreportbatchwise?batchId='+data[i].batchId+'" target="_blank"> Download</a></td>' +
                    //'</tr>');
                /*if(data[i].active == 1){
                    $("#batch"+data[i].batchId+"").children(".batchStatus").text("Enable");
                } else {
                    $("#batch"+data[i].batchId+"").children(".batchStatus").text("Disable");
                }*/
            //}
            
        //},
        
    /*~~~~ NEW BATCH CREATION CALL MODAL FOR VALUES FOR TAKING INPUT ~~~~~*/
        /**Creating New Batch **/
        createNewBatch: function() {
            
            $(".errorClss").hide();
            $(".batchsubmit").css("display","inline-block");
            $(".editbatchsubmit").hide();
            $("#createnewbatch").modal({ keyboard: true });
            $("#createnewbatch").modal('show');
            var qbsid=0;
            batchindex.DispQuestionBankToSelect(qbsid);
            //$("#yes-button label .batchcheck").prop('checked', true);
            $("#batchvalidfrom").val("");
            $("#batchvalidto").val("");
            $("#batchname").val("");
            $("#miscellaneousexpenses").val("");
            $("#travellingexpenses").val("");
            $("#lunchexpenses").val("");
            $("#aprovedmiscellaneous").val("");
            $("#aprovedtravelling").val("");
            $("#aprovedlunch").val("");
             $('#rulecheck').prop('checked', false);
            $("#approvedtrexpens").hide();
            $("#approvedluexpens").hide();
            $("#approvedmiexpens").hide(); 



        },
        
        
        /** modified view in batch **/
        btachRowAccordian: function(e) {
            var that = e.currentTarget.parentNode;
            var nTr = that;
            var i = $.inArray( nTr, anOpen );
            

            if ( i === -1 ) {
         
                table1.fnOpen( nTr, this.fnFormatDetails(table1, nTr), 'details' );
                anOpen.push( nTr );
                 $(that).find('i').toggleClass('fa-plus-circle fa-minus-circle');
            }
            else {
              
                $(that).find('i').toggleClass('fa-plus-circle fa-minus-circle');
                table1.fnClose( nTr );
                anOpen.splice( i, 1 );
            }
        },
        
        /** backend data attaching to table**/
        fnFormatDetails: function( table1, nTr ){
            var oData = table1.fnGetData( nTr );
            var sOut =
            '<div class="innerDetails">'+
                '<table class="tableAccordion" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                '<tr>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td class="simplelabel">Start Date:</td>'+
                    '<td>'+oData.batchStartDate+'</td>'+
                    '<td class="simplelabel">End Date:</td>'+
                    '<td>'+oData.batchEndDate+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td class="simplelabel">Lunch Expenses:</td>'+
                    '<td>' +oData.lunchExpenses+'</td>'+
                    '<td class="simplelabel">Miscellaneous expenses</td>'+
                    '<td>' +oData.miselExpense+'</td>'+
                    '<td class="simplelabel">Travelling Expenses</td>'+
                    '<td>' +oData.travellingExpense+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td class="simplelabel">Candidates Appeared</td>'+
                    '<td>' +oData.candidateAppeared+'</td>'+
                    '<td class="simplelabel">candidates passed</td>'+
                    '<td>' +oData.candidatePassed+'</td>'+
                    '<td class="simplelabel">Candidates Failed</td>'+
                    '<td>' +oData.candidateFailed+'</td>'+
                    '<td class="simplelabel">Passing percentage</td>'+
                    '<td>' +oData.passingPercentage+'</td>'+
                '</tr>'+
            '</table>'+
            '</div>';
            return sOut;
        },
        
        /* Updating Deatils Fetch from Table */
        editBatchDetails: function(e){
         

            

            var bhid = $(e.currentTarget).attr('value');
            //var batchStatus = $('#'+e.currentTarget.parentElement.parentElement.id+'').children('.batchStatus').attr('value');
              $(".batchsubmit").hide();
         
            $(".editbatchsubmit").css("display","inline-block");
            $('.editbatchsubmit').attr('value',bhid);
 
            $("#createnewbatch").modal({ keyboard: true });
            $("#createnewbatch").modal('show');


              {
                   
                     
                    $("#pageloading").show();
                    Backbone.ajax({
                        dataType: "json",
                        url: "getbatchdet?batchId="+bhid,
                        data: "",
                        type: "GET",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                        },
                        success: function (data, textStatus, jqXHR) {
                        	var  qbsid=data.qbsId;
                        	batchindex.DispQuestionBankToSelect(qbsid);
                                 $("#batchvalidfrom").val(data.validFrom);
                                 $("#batchvalidto").val(data.validTo);
                                 $("#batchname").val(data.shortDesc);
                                 $("#miscellaneousexpenses").val(data.missallaneousExp);
                                 $("#travellingexpenses").val(data.travelExp);
                                 $("#lunchexpenses").val(data.lunchExp);
                                 $("#aprovedmiscellaneous").val(data.appMissallaneousExp);
                                 $("#aprovedtravelling").val(data.appTravelExp);
                                 $("#aprovedlunch").val(data.appLunchExp);
                                
                                 
                            /*     if(data.qbsId==0)
                                 {

                                         batchindex.DispQuestionBankToSelect();
                                 }
                                 else
                                 {
                                     $("#questionbox").val(data.qbsId);     
                                 }*/

                                 if(data.expCheck==1)
                                 {
                                    $('#rulecheck').prop('checked', true);
                                    batchindex.expensesView();
                                 }
                                 if(data.expCheck==0)
                                 {
                                    $('#rulecheck').prop('checked', false);
                                    batchindex.expensesView();
                                 }

                            /*   '<div class="list-group-item">'+
                                   '<a  class="templateclick" value="'+data[i].transTempId+'">Click Here</a> to '+data[i].tranDesc+' with Allowable Status'+data[i].allowableStatus+
                               '</div>'
                         */
                      
                        },
                        error: function (res, ioArgs) {
                            if (res.status === 440) {
                                window.location.reload();
                            }
                        },
                           complete: function() {
                            $("#pageloading").hide();
                       } ,
                        
                    });
                }
        
        },
      //-----------------------------start----------------------------------------------------------------------------------

        

        
        DispQuestionBankToSelect: function(id) {
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listqbsbymodid?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
            
                		if(id==0)
                    {
                    	 Backbone.$("#questionbox").empty();
                    	 if(data != ""){
                    		 Backbone.$("#questionbox").append( '<option value='+data[0].qbsId+' class="languageCls">'+data[0].shortDesc+'</option>');
                        	 id=data[0].qbsId;
                    	 }else{
                    		 Backbone.$("#questionbox").append( '<option value=0 class="languageCls">Select</option>');
                    	 }
                    	 
                    }
                	 for (var i=0; i< data.length; i++)
                	{
                	    	 if(data[i].qbsId == id){
                	       Backbone.$("#questionbox").empty();
                             Backbone.$("#questionbox").append(
                            	 
                             '<option value='+data[i].qbsId+' class="languageCls">'+data[i].shortDesc+'</option>'
                            /* '<option value='+data[0].qbsId+' class="languageCls">'+data[0].shortDesc+'</option>'*/
                             );
                          }
                	 }
                	 for (var i=0; i< data.length; i++)
                	 {
                			 if(data[i].qbsId != id){
                          Backbone.$("#questionbox").append(
                            '<option value='+data[i].qbsId+' class="languageCls">'+data[i].shortDesc+'</option>'
                            );
                           }
                	 }
                		 	    	
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                    
                }
            });
        },
        
        
        
   
        

        /* Updating expenses to Batch Detailes Table */
        viewExpensesDetails: function(e){
            var batchId = e.currentTarget.parentElement.parentElement.id.slice(5);
            //var batchStatus = $('#'+e.currentTarget.parentElement.parentElement.id+'').children('.batchStatus').attr('value');
            // var lunchexpenses = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[0]).text();
            // var travelexpenses = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[1]).text();
            // var missallaneousexpenses = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children("td")[2]).text();

             $('.viewexpenses').attr('value',batchId);
          
              {
                    /*var active;
                        if($('.batchcheck').is(':checked')){
                            for( var i = 0; i < $('.batchcheck').length; i++ ){
                                if( $('.batchcheck')[i].checked){
                                    active = $($('.batchcheck')[i]).val();
                                }
                            }
                        }*/
                    var batchId = $('.viewexpenses').attr('value');
                   /* $("#pageloading").show();*/
                    Backbone.ajax({
                        url: "getbatchexpenses?batchId="+batchId,
                        data: "",
                        type: "GET",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                        },
                        success: function (data, textStatus, jqXHR) {
                            
                            if(data != 0){
                            	 $("#missallaneousexpenses").val(data.missallaneousExp);
                                 $("#travelexpenses").val(data.travelExp);
                                 $("#lunchexpenses").val(data.lunchExp);
                            } else{
                                moduleconfigindex.generateConfigTable(data);
                            }
                        },
                        error: function (res, ioArgs) {
                            if (res.status === 440) {
                                window.location.reload();
                            }
                        },
                        
                    });
                }



            $(".expensessubmit").hide();
            $(".errorClss").hide();
            $(".updateexpensessubmit").css("display","inline-block");
            $('.updateexpensessubmit').attr('value',batchId);
          
            $("#missallaneousexpenses").val("");
            $("#travelexpenses").val("");
            $("#lunchexpenses").val("");
            $("#expenses").modal({ keyboard: true });
            $("#expenses").modal('show');



            },



    // -----------------------------------end-----------------------------------------------------------------------------------

    
    /* CALL ROUTE FOR SEEING BATCH DETAILS SENDING BATCH ID*/
        batchDetails: function(e) {
            var btName = $($("#"+e.currentTarget.parentElement.parentElement.id+"").children()[0]).children().children("a").text();
            var bjson;
            bjson={"key":"batchName","value":btName};
            var batchId = $(e.currentTarget).attr('value');
            batchindex.sendBatchNameForFurtureUse( bjson, batchId );
        },
        
        sendBatchNameForFurtureUse: function( backenddate, batchId ){
            $(".errorClss").hide();
            $("#pageloading").show();
            Backbone.ajax({
                url: "addvariable",
                data: JSON.stringify(backenddate),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0) {
                        $(".errorBatchName").show();
                    } else if(data == 1) {
                        landingroutes.navigate('batch/'+batchId+'/trines/'+mid+'/0', {trigger: true});
                    } else {
                        ("Please contact IT Admin");
                    }
                    
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
    /*CREATING A NEW BATCH DETAILS SENDING USING createbatch JSON */
        newBatchSubmit: function(e) {
            //   window.location.reload();
            var newbatchenable=0;
            $(".errorClss").hide();
            if ($('#batchname').val() == 0) {
                $(".errorBatchNameReq").show();
            } else if ($('#batchvalidfrom').val() == 0) {
                $(".errorBatchStar").show();
            } else if ($('#batchvalidto').val() == 0) {
                $(".errorBatchEnd").show();
            }/*else if ($('#questionbox').val() == 0) {
                $(".errorQuestionReq").show();
            }*/ else {
                var baEndDate = parseInt($('#batchvalidto').val().replace(/\D+/g, ''));
                var baStartDate = parseInt($("#batchvalidfrom").val().replace(/\D+/g, ''));
            
                if(baEndDate < baStartDate){
                    $(".errorEndDate").show();
                } else {
                         if( $('#rulecheck').is(':checked'))
                         {
                             newbatchenable=1;

                         }
                         else{
                            newbatchenable=0;
                         }
                    
                    var json;
                    try{
                        json={"validFrom":$('#batchvalidfrom').val(),"validTo":$('#batchvalidto').val(),"shortDesc":$('#batchname').val(),"moduleId":mid,"active":1,"expCheck":newbatchenable,"travelExp":$("#travellingexpenses").val(),
                        "lunchExp":$("#lunchexpenses").val(), "missallaneousExp":$("#miscellaneousexpenses").val(),"appTravelExp":$("#aprovedtravelling").val(),"appLunchExp":$("#aprovedlunch").val(),"appMissallaneousExp":$("#aprovedmiscellaneous").val(),"qbsId":$('#questionbox').val()};
                    }catch(e){}
                    $("#pageloading").show();
                    Backbone.ajax({
                        url: "createbatch",
                        data: JSON.stringify(json),
                        type: "POST",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(data) {
                            if(data == 1){
                                $("#createnewbatch").modal('hide');
                                $("#createnewbatch").on('hidden.bs.modal', function(){
                                    batchindex.render(mid);
                                });
                            } else if(data == 2){
                                $(".errorOnCreSub").show();
                            } else {
                                alert("Contact IT team");
                            }
                        },
                        error:function(res,ioArgs){
                            if(res.status == 403){
                                window.location.reload();
                            }
                        },
                complete: function() {
                    $("#pageloading").hide();
                }
                    });
                }
            }
        },
        
        /*EDITING A BATCH DETAILS SENDING USING editbatch JSON */
        editBatchSubmit: function(e) {
           // window.location.reload();
           var editbatchenable=0;
            $(".errorClss").hide();
            if ($('#batchname').val() == 0) {
                $(".errorBatchNameReq").show();
            } else if ($('#batchvalidfrom').val() == 0) {
                $(".errorBatchStar").show();
            } else if ($('#batchvalidto').val() == 0) {
                $(".errorBatchEnd").show();
            }/* else if ($('#questionbox').val() == 0) {
                $(".errorQuestionReq").show();
            }*/ else {
                var baEndDate = parseInt($('#batchvalidto').val().replace(/\D+/g, ''));
                var baStartDate = parseInt($("#batchvalidfrom").val().replace(/\D+/g, ''));
            
                if(baEndDate < baStartDate){
                    $(".errorEndDate").show();
                } else {
                         
                         if( $('#rulecheck').is(':checked'))
                         {
                             editbatchenable=1;

                         }
                         else{
                            editbatchenable=0;
                         }
                    
                    var json;
                    try{
                        json={"batchId":$('.editbatchsubmit').attr('value'),"validFrom":$('#batchvalidfrom').val(),"validTo":$('#batchvalidto').val(),shortDesc:$('#batchname').val(),"moduleId":mid,"active":1,"expCheck":editbatchenable,"travelExp":$("#travellingexpenses").val(),
                        "lunchExp":$("#lunchexpenses").val(), "missallaneousExp":$("#miscellaneousexpenses").val(),"appTravelExp":$("#aprovedtravelling").val(),"appLunchExp":$("#aprovedlunch").val(),"appMissallaneousExp":$("#aprovedmiscellaneous").val(),"qbsId":$('#questionbox').val()};
                    }catch(e){}
                    $("#pageloading").show();
                    Backbone.ajax({
                        url: "editbatch",
                        data: JSON.stringify(json),
                        type: "POST",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                        success: function(data) {
                            if(data == 1){
                                $("#createnewbatch").modal('hide');
                                $("#createnewbatch").on('hidden.bs.modal', function(){
                                    batchindex.render(mid);
                                });
                            } else if(data == 2){
                                $(".errorOnCreSub").show();
                            } else {
                                alert("Contact IT team");
                            }

                        },
                        error:function(res,ioArgs){
                            if(res.status == 403){
                                window.location.reload();
                            }
                        },
                        complete: function() {
                            $("#pageloading").hide();
                        }
                    });
                }
            }
        },
        
        
      //-------------------------------------------------start---------------------------------------------------------
        
        
        testTimingValidation: function(e) {


            var key;
            var keychar;

            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;

            keychar = String.fromCharCode(key);

            // control keys
            if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
                return true;

            // numbers
            else if ((("0123456789").indexOf(keychar) > -1))
                return true;

            // only one decimal point
            else if ((keychar == "."))
            {
                if (myfield.value.indexOf(keychar) > -1)
                    return false;
            }
            else
                return false;
        },
        // Updating The Expenses in BATCH DETAILS USING JSON 
             updateExpensesSubmit: function(e) {
                 $(".errorClss").hide();
              if ($('#lunchexpenses').val() == "") {
                     $(".errorLunchExpenses").show();
                 } else if ($('#travelexpenses').val() == "") {
                     $(".errorTravelExpenses").show();
                 } else if ($('#missallaneousexpenses').val() == "") {
                     $(".errorMissallaneousExpenses").show();
                 } 
                  else {
                         /*var active;
                             if($('.batchcheck').is(':checked')){
                                 for( var i = 0; i < $('.batchcheck').length; i++ ){
                                     if( $('.batchcheck')[i].checked){
                                         active = $($('.batchcheck')[i]).val();
                                     }
                                 }
                             }*/
                         
                         var json;
                         try{
                             json={"batchId":$('.updateexpensessubmit').attr('value'),"lunchExp":$('#lunchexpenses').val(),"travelExp":$('#travelexpenses').val(),'missallaneousExp':$('#missallaneousexpenses').val()};
                         }catch(e){}
                         $("#pageloading").show();
                         Backbone.ajax({
                             url: "updatebatchexpenses",
                             data: JSON.stringify(json),
                             type: "POST",
                             beforeSend: function(xhr) {
                                 xhr.setRequestHeader(header, token);
                                 xhr.setRequestHeader("Accept", "application/json");
                                 xhr.setRequestHeader("Content-Type", "application/json");
                             },
                             success: function(data) {
                             
                                 if(data == 1){
                                     $("#expenses").modal('hide');
                                     $("#expenses").on('hidden.bs.modal', function(){
                                         batchindex.render(mid);
                                     });
                                 } else if(data == 2){
                                     $(".errorOnCreSub").show();
                                 } else {

                                       ("Please contact IT Admin");
                                    
                                 }

                             },
                             error:function(res,ioArgs){
                                 if(res.status == 403){
                                     window.location.reload();
                                 }
                             },
                             complete: function() {
                                 $("#pageloading").hide();
                             }
                         });
                     }
                 },
     //--------------------------------------------------end------------------------------------------------------------ 
    
    /*CLOSE ALERT BOX*/
        clickAlertOk: function() {
            Alert.closeModal();
        },
    /* DATE PICKER CALLING*/
        dateValidation: function() {
            $("#batchvalidfrom").datepicker({
                dateFormat: "yy-mm-dd"
            });
            $('#batchvalidto').datepicker({
                dateFormat: "yy-mm-dd"
            });
        },
        expensesView:function(e){
        	 if($('#rulecheck').is(':checked')){
        	 
        	$("#approvedtrexpens").show();
        	$("#approvedluexpens").show();
        	$("#approvedmiexpens").show();
        	}
        	else if(!$('#rulecheck').is(':checked')){

        	  $("#approvedtrexpens").hide();
        	$("#approvedluexpens").hide();
        	$("#approvedmiexpens").hide();  
        	}
        	},
        	
        getsearchdate:function(){
        	$( ".datepicker" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
        },
        
        getselecteddate:function(){
        	flag=0;
        	/*var date1=$('input[type=radio]').val();
        	if(date1 != "on")
        	$('input[type=search]').val(date1).keyup();*/
        	var fromdate=$("#fromdate").val();
        	var todate=$("#todate").val();
        	if(fromdate !=""){
        		$(".errorFromDate").hide();
        	}
        	else if(fromdate == null | fromdate ==""){
        		$(".errorFromDate").show();
        	}
        	
        	if(fromdate != "" & todate != ""){
        		batchindex.batchTableCreateForDateSearch(fromdate,todate);
        	}
        	
        }
        
    });
    
    /** BATCH DETAILS VIEW  **/
    batch_manage.Views.batchslist = Backbone.View.extend({
         
        events: {
            'click #trainerdetailid': 'trainerTable',
            'click #studentdetailsid':'studentTable',
            'click .addtraine': 'addNewTrainerToBatch',
            'click .addstudent': 'addNewStudentToBatch',
            'click .attendancelist': 'showAttendence',
            'click .add_student': 'showStudentList',
            //'click .trainercheck': 'addTrainerToBatch',
            'click #submitaddtobatch': 'addUserToBatch',
            'click .removeStudent': 'removeClickedStudent',
            'click .removeTrainer': 'removeClickedTrainer',
            'click .revStdAll-check': 'selectAllStudent',
            'click .selectAllStudent': 'selectAllStudent',
            'click .studentcheck': 'unselectStudent',
            'click .revStd-check': 'unselectStudent',
            'change .pdefile': 'studentPdeListUpload',
            'click #submitPerDateToJoinTable': 'addPreDate',
            'focus .pdeDatePic': 'datePdeValidation',
            
        },
        
        
        initialize: function () {
            _.bindAll(this,'render');
        },
        
        render: function (b, modulid) {
            mid = modulid
            baid = b;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="config_module" class="maind" data-toggle="tooltip tab" data-original-title="Module Configuration">&nbsp;&nbsp;<i class="fa fa-file-excel-o fa-fw"></i>' +
                                '<span class="content-hide">Configuration</span>' +
                            '</li>' +
                            '<li id="user_manage" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                                '<span class="content-hide">User Management</span>' +
                            '</li>' +
                            '<li id="batchmanagement" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="ques_master" class="maind" data-toggle="tooltip tab" data-original-title="Question Bank">&nbsp;&nbsp;<i class="fa fa-pencil fa-fw"></i>' +
                                '<span class="content-hide">Question Bank</span>' +
                            '</li>' +
                            '<li id="admin_attedance" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="paper_create" class="maind" data-toggle="tooltip tab" data-original-title="Test Management">&nbsp;&nbsp;<i class="fa fa-sitemap fa-fw"></i>' +
                                '<span class="content-hide">Test Management</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header"> <span class="bat_Detail"></span> <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div id="student_wrapper">' +
                '<div class="row">' +
                    '<ul class="nav nav-tabs">'+
                        '<li class="trainerclass" data-toggle="tab">'+
                            '<div id="trainerdetailid" data-toggle="tab"> Trainer Details '+
                                '<span></span>'+
                            '</div>'+
                        '</li> '+
                        '<li class="studentclass" data-toggle="tab">'+
                            '<div id="studentdetailsid"> Student Details '+
                                '<span></span>'+
                            '</div>'+
                        '</li>'+
                    '</ul>'+
                '<br><div id="tablesbatchs" class="tab-content">'+
                '</div>' +
            '</div>' +
            '<div class="modal fade" id="addtobatch" tabindex="-1" role="dialog" aria-labelledby="addtobatch" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog" style="width: 70%">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive" id="addContentToBatch">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="errorClss errorUserSelect pull-left"> Please Select Atleast One Either Cancel</div>' +
                            '<div class="errorClss errorUserSubmit pull-left"> Submitting Error, Try Later..! </div>' +
                            '<div class="btn btn-default" align="center" data-dismiss="modal"> Cancel </div>' +
                            '<div id="submitaddtobatch" class="btn btn-default disabal"> Submit </div>' +
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'
            );
            this.addBatchHeading();
        },
    
    
    /* Select Unselect All */    
        selectAllStudent: function(e) {
            var allCheck = e.currentTarget.className;
            var childCheck;
            if(allCheck == "revStdAll-check"){
                childCheck = "trainercheck";
            } else if(allCheck == "selectAllStudent") {
                childCheck = "studentcheck";
            }
            if($('.'+allCheck+'').is(':checked')){
                $("."+childCheck+"").prop('checked',true);
            } else if(!$('.'+allCheck+'').is(':checked')) {
                $("."+childCheck+"").prop('checked',false);
            }
            
            
            if($('.'+allCheck).is(':checked')){
                $("."+childCheck).prop('checked',true);
                var aa= $("."+childCheck);
                for(var i=0;i<aa.length;i++){
                	var id=$(aa[i]).attr("id").split("_")[1];
                	 var index = $.inArray("emp_row_"+id, selected);
                     if ( index === -1 ) {
                         selected.push("emp_row_"+ id );
                         $("#emp_row_"+id).addClass('row_selected');
                        // $("#check_"+id.split("_")[2]).prop('checked',true);
                     }
                }
            } else if(!$('.'+allCheck).is(':checked')) {
                $("."+childCheck).prop('checked',false);
                var aa= $("."+childCheck);
                for(var i=0;i<aa.length;i++){
                	var id=$(aa[i]).attr("id").split("_")[1];
                	 var index = $.inArray("emp_row_"+id, selected);
                     if ( index != -1 ) {
                        // selected.push( id );
                         selected.splice( index, 1 );
                         $("#emp_row_"+id).removeClass('row_selected');
                        // $("#check_"+id.split("_")[2]).prop('checked',true);
                     }
                }
            }
        },
        
    /* Unselect Student */
        unselectStudent: function(e) {
            var childCheck = e.currentTarget.className;
            var allCheck;
            if(childCheck == "revStd-check"){
                allCheck = "revStdAll-check";
            } else if(childCheck == "studentcheck") {
                allCheck = "selectAllStudent";
            }
            
            if(!e.currentTarget.checked){
                if($('.'+allCheck+'').is(':checked')){
                    $("."+allCheck+"").prop('checked',false);
                }
            } else if(e.currentTarget.checked){
                var tempcount = 0;
                var userChecked = $("."+childCheck+"");
                for (var i = 0, length = userChecked.length; i < length; i++) {
                    if ($(userChecked[i]).is(':checked')) { 
                        tempcount++;
                    }
                }
                
                if(tempcount == $("."+childCheck+"").length){
                    $("."+allCheck+"").prop('checked',true);
                }
            }
        },
        
    /*Batch Heading */
        addBatchHeading: function() {
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getvariable?key=batchName",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data){
                    $(".bat_Detail").text(data);
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    /*TRINEE TABLE LOAD ROUTES*/
        trainerTable: function() {
            landingroutes.navigate('batch/'+baid+'/trines/'+mid+'/0', {trigger: true});
        },
        
    /*** TRAINER BELONG TO THE BATCH DISPLY IN TABLE TRAINERTABLE ***/
        loadTrainerTable: function() {
        	selected=[];
            Backbone.$("#tablesbatchs").empty().append(
                '<div class="tab-pane fade in active">' +
                    '<div class="col-xs-3" style="margin-bottom: 20px;">' +
                        '<button class="addtraine btn btn-default" type="button">' +
                            '<i class="fa fa-user"></i>  &nbsp;Add Trainer(s)' +
                        '</button>' +
                    '</div>' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="batchtrainertable" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<td><b>Trainer ID</b></td>' +
                                                '<td><b>Trainer Name</b></td>' +
                                                '<td><b>E-mail</b></td>' +
                                                '<td><b></b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );
            
            jQuery("#batchtrainertable").dataTable({
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" : "paginlisttrainerbybatchid?batchId="+baid+"&moduleId="+mid,
            //"ordering": false,
            "sServerMethod": "POST",
           "bSort" : false,

             "columnDefs": [
                {
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="removeTrainer btn btn-default">Remove</button>';
                    }
                }],
            "aoColumns" : [
                           { "mData": "employeeId" },
                           { "mData": "name" },
                           { "mData": "email" },
                           { "mData": "" }
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'train' + data.uniId); // or whatever you choose to set as the id
                },
            });
            /*$("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"listtrainerbybatchid?batchId="+baid+"&moduleId="+mid,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    if(data == 0){
                        
                    } else {
                        trainersIn = $("#batchtrainertable").children("tbody").empty();
                        batchdetails.generateTrainersList(data);
                    }
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
    
    /*GENERATE BATCH TRAINER LIST*/
        /*generateTrainersList: function(data) {
            for(var i=0; i< data.length; i++){
                trainersIn.append("<tr id='train"+data[i].uniId+"'><td>"+data[i].employeeId+"</td><td>"+data[i].name+"</td><td>"+data[i].email+"</td><td><button class='removeTrainer btn btn-default'>Remove</button></td></tr>");
            }
            $('#batchtrainertable').dataTable({
                "ordering": false
            });
        },*/
    
    /*STUDENT TABLE LOAD ROUTES*/
        studentTable: function() {
            landingroutes.navigate('batch/'+baid+'/stdnt/'+mid+'/0', {trigger: true});
        },
    
    /*STUDENT TABLE IN BATCH */
        loadStudentTable: function() {
        	selected=[];
        	var pdeStatus;
        	var tccStatus;
        	 preDateArray=0;
            Backbone.$("#tablesbatchs").empty().append(
                '<div class="tab-pane fade in active">' +
                    '<div class="col-xs-3" style="margin-bottom: 20px;">' +
                        '<button class="addstudent btn btn-default" type="button">' +
                            '<i class="glyphicon glyphicon-user"></i>  &nbsp;Add Student(s)' +
                        '</button>' +
                    '</div>' +
                    /*'<div id="submitPerDateToJoinTable" class="btn btn-default " style="margin-left: 600px;"> Submit </div>' +*/
                    '<div class="col-xs-9">' +
                    '<div class="col-xs-12">' +
                    /*'<div class="col-xs-2" top:-8px;" style="margin-top: -30px;">' +
                    '<form method="post" enctype="multipart/form-data">'+
                        '<div class="btn btn-outline btn-primary btn-file">'+
                            '<i class="fa fa-upload">'+
                            '</i> &nbsp;Upload <input name="filename" type="file" class="pdefile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="left: 16px;width: 90px;">'+
                        '</div>'+
                    '</form>'+
                '</div>'+
                '<div class="col-xs-9" style="margin-top: -25px;">Students TCC and Preferred Date of Exam</div>'+*/
                '</div>'+
                '<div style="margin-top: 20px;">'+
	                '<a href="batchwiseattendance?batchId='+baid+'" target="_blank">Click Here' +
	                '</a> to download "Student attendance" for this batch' +
                '</div>'+
                '<div id="batchStudTcc" style="margin-left: 200px;margin-top:-20px;">'+
	                '<a href="batchwisestudtcc?batchId='+baid+'" target="_blank">Click Here' +
	                '</a> to download "Students TCC and Preferred Date of Exam" for this batch' +
                '</div>'+
                '</div>' +
                    '<div  id="stdlist" class="row">' +
                        '<div class="col-xs-12">' +
                            '<div class="panel">' +
                                '<div class="panel-body">' +
                                    '<div class="table-responsive">' +
                                        '<table id="studentlisttable" class="table table-hover">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<th>URN</th>' +
                                                    '<th>Name</th>' +
                                                    '<th>Email</th>' +
                                                    '<th>Phone</th>' +
                                                    '<th>Branch Code</th>' +
                                                    '<th>Registered Date</th>' +
                                                    '<th>TCC Status</th>' +
                                                    '<th>Preferred Date Of Exam</th>' +
                                                    '<th>Attendance</th>' +
                                                    '<th><input type="checkbox" class="revStdAll-check"><button type="button" class="removeStudent btn btn-danger btn-circle"><i class="fa fa-trash-o"></i></button></th>' +
                                                '</tr>' +
                                                '<tr>' +
                                                    '<td class="errorClss errorStdCheck" colspan="10"><b>Select Atleast One Student </b></td>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '</tbody>' +
                                        '</table>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );
        
        jQuery("#studentlisttable").dataTable({
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bSort " : false,
            "bServerSide" : true,
            "sAjaxSource" : "paginliststudentbybatchid?batchId="+baid+"&moduleId="+mid,
            "bSort" : false,
             "columnDefs": [{
                 'targets': -3,
                 'searchable':false,
                 'orderable':false,
                 'render': function (data, type, full, meta){
                	 pdeStatus=full.pdeCheck;
                 		if(full.tccStatus == 1){
 	                    	return 'URN Expired';
                     	} else if(full.tccStatus == 4) {
                     		if(full.pde==null){
 	                    		return '<input size="8" type="text" name="batch-validfrom" class="form-control pdeDatePic" readonly="true" style="cursor:pointer ;background-color: white;"><button id="submitPerDateToJoinTable" type="button" class=" btn btn-success btn-circle" style="margin-top:-60px;margin-left:150px"> <i class="fa fa-floppy-o" aria-hidden="true" /> </button>';
 	                    	}else {
 	                    		return '<input size="8" type="text" name="batch-validfrom" class="form-control pdeDatePic" value='+full.pde+' readonly="true" style="cursor:pointer; background-color: white;"><button id="submitPerDateToJoinTable" type="button" class=" btn btn-success btn-circle" style="margin-top:-60px;margin-left:150px"> <i class="fa fa-floppy-o" aria-hidden="true" /> </button>';
 	                    	}
                     	}else {
                     		return 'N/A';
                     	}
                 	
                 	
                 }
            },{
                 'targets': -4,
                 'searchable':false,
                 'orderable':false,
                 "visible": status,
                 'render': function (data, type, full, meta){
                	 tccStatus=full.tccCheck;
                 		if(full.tccStatus==1){
                     		return 'URN Expired';
                     	}else if(full.tccStatus==4){
                     		/*preDateArray=full.uniId;*/
                     		return 'YES';
                     	}else {
                     		return 'NO';
                     	}
                     
                 }
             },{
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                    	  return '<input  id="check_'+full.uniId+'"  type=checkbox class="trainercheck" value='+full.uniId+'>';
                    }
                },{
                    'targets': -2,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<button class="attendancelist"><a><i class="fa fa-bullseye"></i> View </a></button>';
                    }
                }],
            "aoColumns" : [
                           { "mData": "employeeId" },
                           { "mData": "name" },
                           { "mData": "email" },
                           { "mData": "phoneNum" },
                           { "mData": "branchCode" },
                           { "mData": "createdDate" },
                           { "mData": "" },
                           { "mData": "" },
                           { "mData": "" },
                           { "mData": "" }
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                //    $(nRow).attr('id', data.uniId); // or whatever you choose to set as the id
                    $(nRow).attr('id', 'emp_row_' + data.uniId); 
                    if(tccStatus == 0){
                    	$("#studentlisttable").DataTable().column(-4).visible(false);
                    }else{
                    	$("#studentlisttable").DataTable().column(-4).visible(true);
                    }
                    
                    if( pdeStatus == 0){
                    	$("#studentlisttable").DataTable().column(-3).visible(false);
                    }else{
                    	$("#studentlisttable").DataTable().column(-3).visible(true);
                    }
                    
                },
                "rowCallback": function( row, data ) {
                	selected=[];
                	$(".revStdAll-check").prop('checked',false);
                    if ( $.inArray('emp_row_' + data.uniId, selected) !== -1 ) {
                        $(row).addClass('row_selected');
                        
                        $(row).children().children("#check_"+data.uniId).prop('checked',true);
                    }
                   /* if(pdeStatus == 0){
                    	$("#studentlisttable").DataTable().column(-4).visible(false);
                    }
                    
                    if(tccStatus == 0){
                    	$("#studentlisttable").DataTable().column(-3).visible(false);
                    }*/
                    
                }
            });
        
        
        $('#studentlisttable tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
     
            if ( index === -1 ) {
                selected.push( id );
                $("#check_"+id.split("_")[2]).prop('checked',true);
            } else {
                selected.splice( index, 1 );
                $("#check_"+id.split("_")[2]).prop('checked',false);
            }
     
            $(this).toggleClass('row_selected');
        } );
     },
        
    /*--``` GENERATING AND CREATING ROW FOR TABLE OF STUDNETS ACCORDING TO THERE BATCH ```*/
        /*generateStudentList: function(data) {
            for(var i=0; i< data.length; i++){
                studentsIn.append("<tr id='studenturn"+data[i].uniId+"'><td>"+data[i].employeeId+"</td><td>"+data[i].name+"</td><td>"+data[i].email+"</td><td>"+data[i].branchCode+"</td><td>"+data[i].createdDate+"</td><td><button class='attendancelist'><a><i class='fa fa-bullseye'></i> View </a></button></td><td><input type='checkbox' class='revStd-check' value="+data[i].uniId+"></td></tr>");
            }
            $('#studentlisttable').dataTable({
                "ordering": false
            }).yadcf([
                {column_number : 3}
            ]);
        },*/
    
    /*---  LOAD NEW VIEW TO SHOW ATTENDANCE CALL ROUTES WITH STUDENT URN ID  ---*/
        showAttendence: function(e) {
            var studid = e.currentTarget.parentElement.parentElement.id.slice(8);
            landingroutes.navigate('batch/'+baid+'/batchstudentattendence/'+mid+'/'+studid, {trigger: true});
        },
    
    /*ATTENDENCE TABLE OF BATCH */
        batchStudentAttenTable: function(stid) {
            Backbone.$("#tablesbatchs").empty().append(
                '<div class="tab-pane fade in active">' +
                    '<div id="stuntatt" class="row">' +
                        '<div class="col-xs-12">' +
                            '<div class="panel">' +
                                '<div class="panel-body">' +
                                    'Name: <span class="studeName"></span> &nbsp;&nbsp; URN: <span class="studId"></span>' +
                                '</div>' +
                                '<div class="panel-body">' +
                                    '<div class="table-responsive">' +
                                        '<table id="batchstudentattendance" class="table table-hover">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<td><b>Date</b></td>' +
                                                    '<td><b>Slot</b></td>' +
                                                    '<td><b>Attendance</b></td>' +
                                                    '<td><b>Remarks</b></td>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '</tbody>' +
                                        '</table>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );
        
        /* JSON CALL FOR STUDENT LIST IN BATCH*/
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"getstuattbybatch?batchId="+baid+"&studentId="+stid,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    batchAttendenceFlag = true;
                    batchStuAten = Backbone.$("#batchstudentattendance").children("tbody").empty();
                    $(".studeName").text(data.name);
                    $(".studId").text(data.urnId);
                    batchdetails.generateBatchStudentAtten(data.attendents);
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
     /*--``` GENERATING AND CREATING ROW FOR TABLE OF STUDNETS ACCORDING TO THERE BATCH ```*/
        generateBatchStudentAtten: function(data) {
            var paDat, prDat, prSlt, paSlt;
            
            for(var i=0; i< data.length; i++){
                batchStuAten.append('<tr><td>'+data[i].date+'</td><td>'+data[i].slotDesc+'</td><td>'+data[i].status+'</td><td>'+data[i].remarks+'</td></tr>');
            }
            $("#batchstudentattendance").dataTable({
                "ordering": false
            });
        },
        
    /*** MODAL TO DISPLAY THE LIST OF TRAINERS TO ADD TO EXISTING LIST ***/
        addNewTrainerToBatch: function() {
        	selected=[];
            $("#addtobatch").modal('show');
            $("#addtobatch").modal({ keyboard: false });
            json = [];
            $($("#addtobatch div div div")[0]).children(".modal-title").text("Add trainer");
            $("#addContentToBatch").empty().append(
                '<table id="addtobatchlist" class="table table-hover display">' +
                    '<thead>' +
                        '<tr>' +
                            '<th> Trainer ID </th>' +
                            '<th> Name </th>' +
                            '<th> E-mail ID </th>' +
                            '<th> Select </th>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                '</table>'
            );
            studentFlag = false;
            jQuery("#addtobatchlist").dataTable({
          
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            //
            "sAjaxSource" : "pagintrainerlist?moduleId="+mid,
            "sServerMethod": "POST",
            "ordering": false,
            "bSort" : false,
             "columnDefs": [
                {
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                        return '<input  id="check_'+full.uniId+'"  type=checkbox class="trainercheck" value='+full.uniId+'>';
                    }
                }],
            "aoColumns" : [
                           { "mData": "employeeId" },
                           { "mData": "name" },
                           { "mData": "email" },
                           { "mData": "" }
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                //    $(nRow).attr('id', data.uniId); // or whatever you choose to set as the id
                    $(nRow).attr('id', 'emp_row_' + data.uniId); 
                },
                "rowCallback": function( row, data ) {
                    if ( $.inArray('emp_row_' + data.uniId, selected) !== -1 ) {
                        $(row).addClass('row_selected');
                        
                        $(row).children().children("#check_"+data.uniId).prop('checked',true);
                    }
                }
            });
            
        	$('#addtobatchlist tbody').on('click', 'tr', function () {
                var id = this.id;
                var index = $.inArray(id, selected);
         
                if ( index === -1 ) {
                    selected.push( id );
                    $("#check_"+id.split("_")[2]).prop('checked',true);
                } else {
                    selected.splice( index, 1 );
                    $("#check_"+id.split("_")[2]).prop('checked',false);
                }
         
                $(this).toggleClass('row_selected');
            } );
            
            
            
        /*#^ CALL JSON FOR LOADING TRAINER LIST ^#*/
            /*$("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"trainerlist?moduleId="+mid,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    listofTrainers = Backbone.$("#addtobatchlist").children("tbody").empty();
                    studentFlag = false;
                    batchdetails.generateTrainerSelect(data);
                    $("#addtobatchlist").dataTable({
                        "ordering": false
                    });
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
        
       /* generateTrainerSelect: function(data) {
            for(var i=0; i< data.length; i++){
                listofTrainers.append(batchdetails.createTrainerRow(data[i]));
            }
        },*/
        
    /**** GENERATE ROW FOR TRAINER LIST ON MODAL ****/
       /* createTrainerRow: function(rowObj) {
            try{
                var trElement = "<tr id='"+rowObj.uniId+"'><td>"+rowObj.employeeId+"</td><td>"+rowObj.name+"</td><td>"+rowObj.email+"</td><td><input type=checkbox class='trainercheck' value='"+rowObj.uniId+"'></td>";
                return trElement+"</tr>";
            }catch(e){}
        },*/

        
    /*** MODAL TO DISPLAY THE LIST OF STUDENTS TO ADD TO EXISTING LIST ***/
        addNewStudentToBatch: function() {
            $("#addtobatch").modal('show');
            $("#addtobatch").modal({ keyboard: false });
            selected=[];
            json = [];
            $($("#addtobatch div div div")[0]).children(".modal-title").text("Add student");
            $("#addContentToBatch").empty().append(
                '<table id="addtobatchlist" class="table table-hover display">' +
                    '<thead>' +
                        '<tr>' +
                            '<th> URN No </th>' +
                            '<th> Name </th>' +
                            '<th> Branch Code </th>' +
                            '<th> Register Date </th>' +
                            '<th> <input type=checkbox class="selectAllStudent"></th>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                '</table>'
            );
            studentFlag = true;
            jQuery("#addtobatchlist").dataTable({
             
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" : "paginstudentlist?moduleId="+mid,
            "bSort" : false,
            "sServerMethod": "POST",
            "bSort" : false,

             "columnDefs": [
                {
                    'targets': -1,
                    'searchable':false,
                    'orderable':false,
                    'render': function (data, type, full, meta){
                    	return '<input  id="check_'+full.uniId+'"  type=checkbox class="studentcheck" value='+full.uniId+'>';
                    }
                }],
            "aoColumns" : [
                           { "mData": "employeeId" },
                           { "mData": "name" },
                           { "mData": "branchCode" },
                           { "mData": "createdDate" },
                           { "mData": "" }
            ],
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                //    $(nRow).attr('id', data.uniId); // or whatever you choose to set as the id
                    $(nRow).attr('id', 'emp_row_' + data.uniId); 
                },
                "rowCallback": function( row, data ) {
                	$(".selectAllStudent").prop('checked',false);
                	/*selected=[];*/
                    if ( $.inArray('emp_row_' + data.uniId, selected) !== -1 ) {
                        $(row).addClass('row_selected');
                        
                        $(row).children().children("#check_"+data.uniId).prop('checked',true);
                    }
                }
            });
            
            
            $('#addtobatchlist tbody').on('click', 'tr', function () {
                var id = this.id;
                var index = $.inArray(id, selected);
         
                if ( index === -1 ) {
                    selected.push( id );
                    $("#check_"+id.split("_")[2]).prop('checked',true);
                } else {
                    selected.splice( index, 1 );
                    $("#check_"+id.split("_")[2]).prop('checked',false);
                }
         
                $(this).toggleClass('row_selected');
            } );
            
        /*#^ CALL JSON FOR LOADING STUDENT LIST ^#*/
            /*$("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"studentlist?moduleId="+mid,
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    studentFlag = true;
                    batchdetails.generateStudentSelect(data);                
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
        
    /*** GENERATING A TABLE FOR STUDENTS FOR SELECTING IN ORDER TO ADD FOR A BATCH ***/
       /* generateStudentSelect: function(data) {
            for(var i=0; i< data.length; i++){
                listofStudents.append(batchdetails.createStudentRow(data[i]));
            }
            $('#addtobatchlist').dataTable({
                "ordering": false
            }).yadcf([
                {column_number : 2},
                {column_number : 3}
            ]);
        },*/
        
    /**** GENERATE ROW FOR STUDENT LIST ON MODAL ****/
       /* createStudentRow: function(rowObj) {
            try{
                var trElement = "<tr id='"+rowObj.uniId+"'><td>"+rowObj.employeeId+"</td><td>"+rowObj.name+"</td><td>"+rowObj.branchCode+"</td><td>"+rowObj.createdDate+"</td><td><input type=checkbox class='studentcheck' value='"+rowObj.uniId+"'></td>";
                return trElement+"</tr>";
            }catch(e){}
        },*/
        
    /*** SELECTED TRAINER DETAILS ***/
        addTrainerToBatch: function(e) {
            var seltrid = e.currentTarget.parentElement.parentElement.id;
            if(e.currentTarget.checked){
                try{
                    json.push({"uniId":e.currentTarget.parentElement.parentElement.id});
                }catch(e){}
            } else {
                try{
                    this.removeTri(e.currentTarget.parentElement.parentElement.id);
                }catch(e){}  
            }
        },

    /*** SELECTED STUDENTS DETAILS ***/
        addStudentToBatch: function(e) {
            var selstid = e.currentTarget.parentElement.parentElement.id;
            if(e.currentTarget.checked){
                try{
                    json.push({"uniId":e.currentTarget.parentElement.parentElement.id});
                }catch(e){} 
            } else {
                try{
                    this.removeA(e.currentTarget.parentElement.parentElement.id);
                }catch(e){}  
            }
        },
    
    /*** SENDING STUDENTS & TRAINER DETAILS TO ADD A BATCH ***/
        addUserToBatch: function(e) {
            $(".errorClss").hide();
            json = [];
            var urlv;
                if(studentFlag){
                	//selected[0].split("_")[2]
                	
                	
                	for(var i=0;i<selected.length;i++){
                		
                		json.push({"uniId": selected[i].split("_")[2]});
                	}
                	
                  /*  $('.studentcheck:checked').each(function () {
                        json.push({"uniId":$(this).val()})
                    });*/
                    if($(".selectAllStudent").is(':checked')){
                        urlv="addstudenttobatch?batchId="+baid+"&addAll=2";
                    } else {
                        urlv="addstudenttobatch?batchId="+baid+"&addAll=2";
                    }
                } else{
                	for(var i=0;i<selected.length;i++){
                		
                		json.push({"uniId": selected[i].split("_")[2]});
                	}
                    /*$('.trainercheck:checked').each(function () {
                        json.push({"uniId":$(this).val()})
                    });*/
                    urlv="addtrainertobatch?batchId="+baid;
                }
            if(json.length > 0){
                $("#pageloading").show();
                Backbone.ajax({
                    url: urlv,
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                    	
                        if(data == 1) {
                        	selected=[];
                            reloadErrorFlag = true;
                            $("#addtobatch").modal('hide');
                            $("#addtobatch").on('hidden.bs.modal', function(){
                                if(reloadErrorFlag){
                                    batchdetails.reloadBatchDeailsPage();
                                }
                            });
                        } else if(data == 2){
                            $(".errorUserSubmit").show();
                        } else {
                            alert("Contact IT team");
                        }
                    },
                    error:function(res,ioArgs){
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                    complete: function() {
                        $("#pageloading").hide();
                    }
                });
            } else {
                $(".errorUserSelect").show();
            }
        },
    
    /* PRPBLEM IN MODAL */
        reloadBatchDeailsPage:function(e) {
            reloadErrorFlag = false;
            if(studentFlag){
                batchdetails.loadStudentTable();
            } else{
                batchdetails.loadTrainerTable();
            }
        },
        
    /*** REMOVING A TRAINER FOR BATCH ***/
        removeClickedTrainer: function(e) {
            $(".errorTrainerRemove").remove();
            swal({
                    title: "Do you want to remove \""+$($(e.currentTarget.parentElement.parentElement).children()[1]).text()+"\" from this batch",
                    showCancelButton: true,
                    confirmButtonColor: "#F01E1E",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No"
                },
                 function(isConfirm){
                if(isConfirm){
                    var tid = e.currentTarget.parentElement.parentElement.id.slice(5);
                    $("#pageloading").show();
                    Backbone.ajax({
                        dataType:"json",
                        url:"removetrainerbatch?uniId="+tid+"&batchId="+baid,
                        data:"",
                        type: "POST",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader(header, token);
                        },
                        success: function(data){
                            if(data == 1){
                                batchdetails.loadTrainerTable();
                            } else if(data == 2){
                                $(e.currentTarget.parentElement).append('<div class="errorClss errorTrainerRemove"><br>Failed Try Again</div>');
                                $(".errorTrainerRemove").show();
                            } else{
                                alert("Contact IT team");
                            }
                        },
                        error: function (res, ioArgs) {
                            
                            if (res.status === 440) {
                                window.location.reload();
                            }
                        },
                        complete: function() {
                            $("#pageloading").hide();
                        }
                    });
                }
            });
        },
        
    /*** REMOVING A STUDENT FOR BATCH ***/
        removeClickedStudent: function(e) {
            $(".errorClss").hide();
            $(".errorStudentRemove").remove();
            swal({
                    title: "Remove Student from this batch",
                    showCancelButton: true,
                    confirmButtonColor: "#F01E1E",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No"
                },
                 function(isConfirm){
                if(isConfirm){
                    var useUnid = [];
                    var remAll;
                    if($('.revStdAll-check').is(':checked')){
                        remAll = 2;
                        for (var i = 0; i < selected.length; i++) {
                           // if ($(userChecked[i]).is(':checked')) { 
                                useUnid.push(selected[i].split("_")[2]);
                            //}
                        }
                    } else {
                        remAll = 2;
                        for (var i = 0; i < selected.length; i++) {
                            // if ($(userChecked[i]).is(':checked')) { 
                                 useUnid.push(selected[i].split("_")[2]);
                             //}
                         }
                    }
                    if($('.revStd-check').is(':checked') || useUnid.length >0){
                        $("#pageloading").show();
                        Backbone.ajax({
                            dataType:"json",
                            url:"removestudentbatch?removeAll="+remAll+"&uniId="+useUnid+"&batchId="+baid,
                            data:"",
                            type: "POST",
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader(header, token);
                            },
                            success: function(data){
                            	selected=[];
                                if(data == 1){
                                    batchdetails.loadStudentTable();
                                } else if(data == 2){
                                    $(e.currentTarget.parentElement).append('<div class="errorClss errorStudentRemove"><br>Failed Try Again</div>');
                                    $(".errorStudentRemove").show();
                                } else{
                                    alert("Contact IT team");
                                }
                            },
                            error: function (res, ioArgs) {

                                if (res.status === 440) {
                                    window.location.reload();
                                }
                            },
                            complete: function() {
                                $("#pageloading").hide();
                            }
                        });
                    } else {
                        $(".errorStdCheck").show();
                    }
                }
            });
        },
        
        removeTri: function(v) {  
            for(var i=0;i<json.length;i++){
                if(json[i].uniId == v){
                    json.splice(i, 1);
                }
            }
        },
        
        removeA: function(v) {  
            for(var i=0;i<json.length;i++){
                if(json[i].uniId == v){
                    json.splice(i, 1);
                }
            }
        },
        addPreDate: function(e) {
            $(".errorClss").hide();
            json = [];
            var preDate;
            var j = 0;
            /*for(var i = 0; i < preDateArray.length; i++ ) {*/
            preDateArray=e.currentTarget.parentElement.parentElement.id.split("_");
              var d = $("#emp_row_"+preDateArray[2]+"").children().children(".pdeDatePic").val();
              if(!!d){
                preDate = preDateArray[2]+"_"+d;
                j++;
              }
           /* }*/
            //var uniIdAndPreDAte = [];
              var urlv = "studentpdelistupdate";
             /* for(var i=0;i<preDate.length / 2;i++){*/
                //uniIdAndPreDAte =  preDate[i].split("_");
              json.push({"uniId":preDate.split("_")[0], "pde": preDate.split("_")[1] });
           /* }*/
              if(json.length > 0){
                $("#pageloading").show();
                Backbone.ajax({
                      url: urlv,
                      data: JSON.stringify(json),
                      type: "POST",
                      beforeSend: function(xhr) {
                          xhr.setRequestHeader(header, token);
                          xhr.setRequestHeader("Accept", "application/json");
                          xhr.setRequestHeader("Content-Type", "application/json");
                      },
                      complete: function() {
                          $("#pageloading").hide();
                          sweetAlert("Preferred Date Of Exam Submited Successfully ");
                      }
                  });
                
              }
          },
        
        studentPdeListUpload:function(e){
            $("#pageloading").show();
            $(".errorClss").hide();
            var urlv = "studentpdelistupload?"+csrf_url+"="+token;
            $(e.currentTarget).parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".pdefile").parent();
                    $(".pdefile").remove();
                    $(pf).append('<input name="filename" type="file" class="pdefile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    if(data=="1"){
                        sweetAlert("Students Preferred Date of Exam Uploaded Successfully");
                    } else if(data=="2"){
                    	sweetAlert("Uploading Failed. Try Again...!");
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    $(".pdefile").val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
            
        },
        /* DATE PICKER CALLING*/
        datePdeValidation: function(e) {
            var startDate = new Date(parseInt($($(e.currentTarget.parentElement.parentElement).children()[5]).text(), 10));
            //var endDate = new Date(startDate.setMonth(startDate.getMonth() + 11));

            $($($(e.currentTarget.parentElement.parentElement).children()[7]).children()).datepicker({
                dateFormat: "yy-mm-dd",
                minDate: new Date(),
                maxDate: "+11M"
            });
            
        }

        
    });
    
    
})();
