/*** Variacle for participant ***/
var paricipantindex, participantview, testview, conclusionview, correctAns;
var ansArray = [];
var queArray = [];
var asTogal;
(function () {
    
    window.participant_test = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    /*** Routes for Participant details and test ***/
    participant_test.Routes.testnav = testroutes;
    
    /*** Initializing View for Participant details and test ***/
    participant_test.Views.index = controller;
    
    /*** Object Creation Participant details and test ***/
    paricipantindex = new participant_test.Views.index();
    $(document.body).append(paricipantindex.el);
    
    /*** Object for Routes ***/
    participantroutes = new participant_test.Routes.testnav();
    Backbone.history.start();
})();