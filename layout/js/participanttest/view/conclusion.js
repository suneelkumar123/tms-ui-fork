(function () {
    
    var conQueAry = [];
    window.participant_conclusion = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    var quesansElement;
    participant_conclusion.Views.conclusion = Backbone.View.extend({
        
        events: {
            'click #compare_back':'loadCompareAns'
        },
        
        initialize: function () {
            _.bindAll(this, 'render');
        },
        
        render: function () {
            this.$el.html(
                '<div class="row">' +
                    '<div class="col-xs-6 col-xs-offset-3">' +
                        '<h3 class="page-header">You have completed the exam. Thank you.! </h3><br>' +
                        '<div class="panel-body">' +
                            '<div class="row">' +
                                '<div class="col-xs-12">' +
                                    '<div class="list-group">' +
                                        '<div class="list-group-item" style="background-color: #FCFCFC"><b><u>Results</u></b></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Total Number of Questions <span class="no_question pull-right text-muted small"></span></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Number of Questions Answered <span class="ans_question pull-right text-muted small"></span></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Maximum score <span class="tot_mark pull-right text-muted small"></span></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Number of Correct Answer <span class="you_mark pull-right text-muted small"></span></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Number of questions skipped <span class="skip_question pull-right text-muted small"></span></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Percentage Obtained<span class="Per_centage pull-right text-muted small"></span></div>' +
                                    '</div>' +
                                    '<button id="compare_back" type="submit" class="btn btn-primary btn-block">Compare Answer</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>' +
                '<!-- /.row -->'
            );
            this.loadFinalResult();
        },
        
        loadFinalResult: function(){
            var noque = 0;
            var ansque = 0;
            var skipqu = 0;
            var youper = 0;
            noque = queArray.length;
            for( var i=0; i<ansArray.length; i++){
                if( ansArray[i].answer != ""){
                    ansque++;
                }
            } 
            skipqu = noque - ansque;
            youper = (correctAns/noque)*100;
            $(".no_question").text(noque);
            $(".ans_question").text(ansque);
            $(".tot_mark").text(noque);
            $(".you_mark").text(correctAns);
            $(".skip_question").text(skipqu);
            $(".Per_centage").text(""+youper+" %");
        },
        
        loadCompareAns: function () {
            participantroutes.navigate('compare/ANSWERS', {trigger: true});
        },
        
        compareQueAnsDisplay: function() {
            
            this.$el.html(
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<h1 class="page-header">Answer Compare <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>'+
                            '<div class="panel-body" style="max-height: 425px; overflow-y: scroll; padding: 25px;">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="panel-group" id="test_questions"></div>' +
                                    '</div>' +
                                '</div>' +
                                '<!-- /.row (nested) -->' +
                            '</div>' +
                            '<!-- /.panel-body -->' +
                        '</div>' +
                        '<!-- /.panel -->' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>'
            );
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "topictestquestion",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("please login again");
                    } else {
                        quesansElement = Backbone.$("#test_questions").empty();
                        conclusionview.generateCompareQuesAns(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    
        generateCompareQuesAns:function(data){
            conQueAry = [];
            for(var i=0; i< data.length; i++){
                var que = data[i].qid;
                var ansar = data[i].ans;
                conQueAry.push({question:que, answer:ansar});
                quesansElement.append(
                '<div class="row" id=queanid'+data[i].qid+'>' +
                    '<div class="page-header"><b>'+(i+1)+'. &nbsp;&nbsp;</b><b>'+data[i].question+'?</b></div>' +
                    '<div class="col-xs-6">' +
                        '<div>' +
                            '<span class="ansopt1">1)&nbsp;&nbsp; '+data[i].opt1+'</span>' +
                        '</div>' +
                        '<div>' +
                            '<span class="ansopt3">3)&nbsp;&nbsp; '+data[i].opt3+'</span>' +
                        '</div>' + 
                        '<div>' +
                            '<span id="Opl5'+i+'" class="ansopt5">5)&nbsp;&nbsp; '+data[i].opt5+'</span>' +
                        '</div>' + 
                    '</div>' +
                    '<div class="col-xs-6">' +
                        '<div>' +
                            '<span class="ansopt2">2)&nbsp;&nbsp; '+data[i].opt2+'</span>' +
                        '</div>' +
                        '<div>' +
                            '<span class="ansopt4">4)&nbsp;&nbsp; '+data[i].opt4+'</span>' +
                        '</div>' +
                    '</div>' +
                '</div>'
                );
                
                if(data[i].opt5 == null){
                    $("#Opl5"+i+"").hide();
                } else if(data[i].opt5 == 0){
                    $("#Opl5"+i+"").hide();
                }
                
                for(var j=0; j< ansArray.length; j++){
                    if(ansArray[j].questionId == data[i].qid){
                        if(ansArray[j].answer == "opt1"){
                            $("#queanid"+data[i].qid+"> div:nth-child(2) > div:nth-child(1) > span").css("color", "#ff0000");
                        } else if(ansArray[j].answer == "opt2"){
                            $("#queanid"+data[i].qid+"> div:nth-child(3) > div:nth-child(1) > span").css("color", "#ff0000");
                        } else if(ansArray[j].answer == "opt3"){
                            $("#queanid"+data[i].qid+"> div:nth-child(2) > div:nth-child(2) > span").css("color", "#ff0000");
                        } else if(ansArray[j].answer == "opt4"){
                            $("#queanid"+data[i].qid+"> div:nth-child(3) > div:nth-child(2) > span").css("color", "#ff0000");
                        } else if(ansArray[j].answer == "opt5"){
                            $("#queanid"+data[i].qid+"> div:nth-child(2) > div:nth-child(3) > span").css("color", "#ff0000");
                        } else {
                            $("#queanid"+data[i].qid+"> div:nth-child(3)").append('<span class="ansskip" style="color:orange">Skipped</span>');
                        } 
                    }
                }
                
                if(data[i].ans == "opt1"){
                    $("#queanid"+data[i].qid+"> div:nth-child(2) > div:nth-child(1) > span").css("color", "#00ff62");
                } else if(data[i].ans == "opt2"){
                    $("#queanid"+data[i].qid+"> div:nth-child(3) > div:nth-child(1) > span").css("color", "#00ff62");
                } else if(data[i].ans == "opt3"){
                    $("#queanid"+data[i].qid+"> div:nth-child(2) > div:nth-child(2) > span").css("color", "#00ff62");
                } else if(data[i].ans == "opt4"){
                    $("#queanid"+data[i].qid+"> div:nth-child(3) > div:nth-child(2) > span").css("color", "#00ff62");
                } else if(data[i].ans == "opt5"){
                    $("#queanid"+data[i].qid+"> div:nth-child(2) > div:nth-child(3) > span").css("color", "#00ff62");
                }
            }
            
        },
    });
})();