(function () {
    var lanId, topId;
    var modArray = [];
    window.participant_details = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    participant_details.Views.participants = Backbone.View.extend({
        
        events: {
            'click #mok_test': 'loadMockTestStart',
            'click #top_test': 'loadTopicTestStart',
            'click #mock_test': 'mockTestStart',
            'click #topic_test': 'topicTestStart',
            'click .checkmoclng': 'lanSelMocTest',
            'click .checklng': 'languageSelectedForTest',
            'click .partymodcls': 'loadDetailsOfParticipant',
            'click .checktpc': 'topicSelectedForTest',
            'click .pre-res': 'previewResult'
        },
        
        initialize: function () {
            _.bindAll(this, 'render');
        },
        
        render: function() {
            $("#pageloading").show();
            this.$el.empty().append(
			'<div class="row selectView" >' +
                    '<div class="col-xs-6 col-xs-offset-3"  style="margin-top: 15px;">' +
                        '<div class="panel-body">' +
							'<h3 class="page-header">Welcome to test portal</h3><br>' +
                            '<div id="partymodule" class="list-group" style="font-size: 18px;"></div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
			);
            
            Backbone.ajax({
                dataType: "json",
                url: "participantinformation?moduleId=",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    $("#pageloading").show();
                    if(data == 0) {
                        sweetAlert("please login again");
                    } else {
                        var noMa = data.module;
                        if(noMa.length == 0){
                            $("#partymodule").empty().append(
										'<div class="list-group-item" style="background-color: #FCFCFC"><b>No Module is assigned to this user please contact your trainer</b></div>'
									);
                            $(".selectView").show();
                        }
                        if(noMa.length == 1){
                            participantview.singleModuleLoadDetails(noMa[0].moduleId, noMa[0].shortDesc);
                            $("#partybatchid").text(noMa[0].batchId);
                        } else {
                            for(var i=0; i<noMa.length; i++){
								if(i == 0){
									$("#partymodule").empty().append(
										'<div class="list-group-item" style="background-color: #FCFCFC"><b><u>Select Your Module</u></b></div>'
									);
								}
                                modArray.push({"modulId":noMa[i].moduleId,"mName":noMa[i].shortDesc})
                                $("#partymodule").append(
									'<div class="col-lg-12 list-group-item">' +
										'<a id='+noMa[i].moduleId+' class="partymodcls" value='+noMa[i].moduleId+'>Click Here</a> to Proceed as '+ noMa[i].shortDesc+' module participant' +
									'</div>'
								);
                            }
                            $(".selectView").show();
                            $("#pageloading").hide();
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                }
            });
        },
        
        singleModuleLoadDetails: function(mid, mname){
            /*var mjson;
            var midjson;
            mjson={"key":"moduleName","value":"TRAINING MANAGEMENT SYSTEM"};
            midjson={"key":"reloadMid","value":mid}
            participantview.sendPartyDataToBackEnd( mjson );
            participantview.sendMidBackEnd( midjson );*/
            //$(".logo-tms").text(mname);
            participantroutes.navigate('modules/'+mid+'/'+mname, {trigger: true});
        },
        
        loadDetailsOfParticipant: function(e) {
            var modId = e.currentTarget.id;
            var mjson;
            var mname;
            var midjson;
            for( var i = 0; i< modArray.length; i++){
                if( modId == modArray[i].modulId){
                    mname = modArray[i].mName;
                }
            }
            /*mjson={"key":"moduleName","value":"TRAINING MANAGEMENT SYSTEM"};
            participantview.sendPartyDataToBackEnd( mjson );
            midjson={"key":"reloadMid","value":modId}
            participantview.sendMidBackEnd( midjson );*/
            //$(".logo-tms").text(mname);
            participantroutes.navigate('modules/'+modId+'/'+mname, {trigger: true});
        },
        
        
        sendPartyDataToBackEnd: function( backenddate ){
            $("#pageloading").show();
            Backbone.ajax({
                url: "addvariable",
                data: JSON.stringify(backenddate),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("Loging from first problem in storing data");
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        detailsOfParticipant: function (mid, mname) {
            
            this.$el.html(
                '<div class="row">' +
                    '<h1 class="page-header"> Basic Information of participant <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="configmoduletable" class="table table-hover" style="text-align:left;">' +
                                        '<tbody>' +
                                            '<tr>' +
                                                '<td class="col-xs-2"><b>Name:&nbsp; </b></td>' +
                                                '<td>' +
                                                    '<span id="partyname"></span>' +
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><b>Phone No:&nbsp;</b></td>' +
                                                '<td>' +
                                                    '<span id="partyphone"></span>' +
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><b>Branch code:&nbsp;</b></td>' +
                                                '<td>' +
                                                    '<span id="partybranch"></span>' +
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><b>Mail Id:&nbsp;</b></td>' +
                                                '<td>' +
                                                    '<span id="partyemail"></span>' +
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><b>Batch Name:&nbsp;</b></td>' +
                                                '<td>' +
                                                    '<span id="partybatchid"></span>' +
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><b>Mock Test Attempts:&nbsp;</b></td>' +
                                                /*'<td>' +
                                                    '<span id="mc-att"></span>&nbsp;' +
                                                '</td>' +*/
                                                '<td>' +
                                                    '<button class="btn btn-success pull-left pre-res" align="center"> <i class="fa fa-file-text-o fa-fw"></i> Previous Results </button>' +
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><b>Study Material:&nbsp;</b></td>' +
                                                '<td>' +
                                                    '<span id="study_materials"></span>' +
                                                '</td>' +
                                            '</tr>' +
                                            /*'<tr>' +
                                                '<td><b>Exam Details:&nbsp;</b></td>' +
                                                '<td>' +
                                                    '<span id="partyexamdetails"></span>' +
                                                '</td>' +
                                            '</tr>' +*/
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                            '<!-- /.panel-body -->' +
                            '<div class="panel-footer">' +
                                '<div class="row">' +
                                    '<div class="col-xs-6">' +
                                        '<strong>If the above details are correct, continue for test</strong>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="errorClss error-Test" style="font-size: 20px; text-align: center;"><br>Test is not configured </div>' +
                                    '<div class="col-xs-6">' +
                                        '<div style="font-size: 20px"><br><a id="mok_test">Click Here</a> to take \"Mock\" test</div>' +
                                        '<div class="errorClss errormock-Test" style="font-size: 20px"><br> </div>' +
                                    '</div>' +
                                    '<div class="col-xs-6">' +
                                        '<div style="font-size: 20px"><br><a id="top_test">Click Here</a> to take \"Practice\" test</div>' +
                                        '<div class="errorClss errorPractice-Test" style="font-size: 20px"><br></div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<!-- /.panel -->' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>' +
                '<!-- /.row -->' +
                '<div class="modal fade" id="pre-result" tabindex="-1" role="dialog" aria-labelledby="pre-result" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                    '<div class="modal-dialog" style="width: 80%;">' +
                        '<div class="modal-content">' +
                            '<div class="modal-header">' +
                                '<h1 class="page-header">Score in respective assessments </h1>' +
                            '</div>' +
                            '<div class="modal-body">' +
                                '<div class="panel-body">' +
                                    '<div class="table-responsive">' +
                                        '<table id="assesmentScore" class="table table-hover">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<td><b>Attempted</b> (<span class="att-mkc"> </span>)</td>' +
                                                    '<td><b>Total Question</b></td>' +
                                                    '<td><b>Answered</b></td>' +
                                                    '<td><b>Un Answered</b></td>' +
                                                    '<td><b>Total Score</b></td>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '</tbody>' +
                                        '</table>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="modal-footer">' +
                                '<button class="btn btn-success" data-dismiss="modal"> Ok </button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            ); 
            
            var backenddate = {"key":"reloadMid","value":mid}
            Backbone.ajax({
                url: "addvariable",
                data: JSON.stringify(backenddate),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0) {
                        sweetAlert("Loging from first problem in storing data");
                    }
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    participantview.displayPraticipantDetails(mid);
                }
            });
        },
        
        displayPraticipantDetails: function(mid){
            $("#pageloading").show();
            var mjson;
            Backbone.ajax({
                dataType: "json",
                url: "participantinformation?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("please login again");
                    } else {
                        var noMo = data.module;
                        for(var i = 0; i < noMo.length; i++){
                            if( mid == noMo[i].moduleId){
                                $("#partybatchid").text(noMo[i].batchId);
                            }
                        }
                        $("#partyname").text(data.name);
                        $("#partyphone").text(data.phone);
                        $("#partybranch").text(data.branchcode);
                        //$("#partydob").text(data.dob);
                        $("#partyemail").text(data.emailId);
                        //$("#batchtrainer").text(data.trainerName);
                        //$("#partyexamdetails").text(data.examDetails);
                        mjson = {"key":"participantName","value":data.name};
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    participantview.partyNameSave( mjson );
                }
            });
        },
        
        previewResult: function() {
            $("#pre-result").modal('show');
            Backbone.$("#assesmentScore").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"getstudentmockstatus",
                data:"",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data, textStatus, jqXHR){
                    if(data == 0){
                        Backbone.$("#assesmentScore").children("tbody").append(
                                '<tr>' +
                                    '<td colspan="4"> No Data Found </td>' +
                                '</tr>'
                            );
                    } else {
                    $(".att-mkc").text(data[0].mkc);
                        for( var i=0; i< data.length; i++ ){
                            Backbone.$("#assesmentScore").children("tbody").append(
                                '<tr>' +
                                    '<td>'+data[i].attemptedCount+'</td>' +
                                    '<td>'+data[i].totalQuesCount+'</td>' +
                                    '<td>'+(data[i].totalQuesCount - data[i].unAnsCount)+'</td>' +
                                    '<td>'+data[i].unAnsCount+'</td>' +
                                    '<td>'+data[i].totalCorrectAnsCount+'</td>' +
                                '</tr>'
                            );
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
        partyNameSave: function(backendname) {
            $("#pageloading").show();
            Backbone.ajax({
                url: "addvariable",
                data: JSON.stringify(backendname),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("Problem on savaing Name of Participant");
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    participantview.loadStudyMaterial();
                }
            });
        },
        
        
        loadPracticeTest: function() {
            $(".errorClss").hide();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "ispractenabled",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    var temp = data.toString().split("");
                    if(data == "00") {
                        $("#top_test").parent().hide();
                        $("#mok_test").parent().hide();
                        $(".error-Test").show();
                    } else if (data == "11") {
                        $("#top_test").parent().show();
                        $("#mok_test").parent().show();
                    } else if(temp[0] == 1) {
                        $(".errorPractice-Test").show();
                        $("#mok_test").parent().show();
                        $("#top_test").parent().hide();
                    } else if(temp[1] == 1) {
                        $(".errormock-Test").show();
                        $("#top_test").parent().show();
                        $("#mok_test").parent().hide();
                    } else {
                        $("#top_test").parent().hide();
                        $("#mok_test").parent().hide();
                        sweetAlert(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        loadStudyMaterial: function(){
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "studymatrial",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        
                    } else {
                        $("#study_materials").empty();
                        for( var i = 0; i< data.length; i++){
                            $("#study_materials").append('<a href="getstudymatrial?unid='+data[i].unid+'" target="_blank">'+data[i].filename+'</a><br>');
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    participantview.loadPracticeTest();
                }
            });
        },
        
        loadMockTestStart: function(){
            participantroutes.navigate('test/confirmmocktest', {trigger: true});
        },
        
        loadTopicTestStart: function() {
            participantroutes.navigate('test/confirmtopictest', {trigger: true});
        },
        
        confirmMockDetails: function() {
            this.$el.html(
                '<div class="row">' +
                    '<h1 class="page-header"> Module Test <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-4">' +
                                        '<div id="selectlanguage" class="list-group">' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="col-xs-4">' +
                                        '<div class="row" style="font-size: 17px; margin:20%"><a id="mock_test">Click Here</a> to Test</div>' +
                                        
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<!-- /.panel -->' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>' +
                '<!-- /.row -->'
            );
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "mok-languages",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("Languages not present for selection");
                    } else {
                        $("#selectlanguage").empty();
                        for(var i=0; i<data.length; i++){
                            if(i == 0){
                                $("#selectlanguage").append(
                                    '<div class="list-group-item" style="background-color: #FCFCFC"><b><u>Select Language</u></b></div>'
                                );
                            }
                            $("#selectlanguage").append(
                                '<div class="list-group-item">&nbsp;&nbsp;'+data[i].shortDesc+'' +
                                    '<label class="pull-right text-muted small module-name">' +
                                        '<input type="radio" class="checkmoclng" name="select1" value='+data[i].langId+'>' +
                                    '</label>' +
                                '</div>'
                            );
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        mockTestStart: function(){
            if($(".checkmoclng").is(':checked')){
                var radios = $(".checkmoclng");
                var lncode;
                for (var i = 0, length = radios.length; i < length; i++) {
                    if (radios[i].checked) { 
                        lncode = radios[i].value;
                    }
                }
                $("#pageloading").show();
                Backbone.ajax({
                    url: "mok-selectlanguage?langCode="+lncode,
                    data: "",
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (data, textStatus, jqXHR) {
                        
                        if(data == 0) {
                            sweetAlert("Loging from first problem in storing data");
                        } else if(data == 1) {
                            participantroutes.navigate('prepare/mtbeginsingle', {trigger: true});
                        } else if(data == 2) {
                            participantroutes.navigate('prepare/mtbeginmultiple', {trigger: true});
                        }  else if(data == 3) {
                            swal({
                                title: "Maximum attempts completed. ",
                                confirmButtonColor: "#F01E1E",
                                confirmButtonText: "Ok"
                            },
                                 function(){
                                participantview.loadMockTestStart();
                            });
                        } else {
                            sweetAlert("Your selection not been submitted");
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            } else {
                sweetAlert("Please select language before taking test")
            }
        },
        
        /* TOPIC TEST  */
        
        confirmTopicDetails: function() {
            this.$el.html(
                '<div class="row">' +
                    '<h1 class="page-header">Practice \Test<div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-4">' +
                                        '<div id="selectlanguage" class="list-group">' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="col-xs-4">' +
                                        '<div id="selecttopic" class="list-group">' +
                                        '</div>' +
                                        
                                    '</div>' +
                                    '<div class="col-xs-4">' +
                                        '<div class="row" style="font-size: 17px; margin:20%;"><a id="topic_test">Click Here</a> to Test</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<!-- /.panel-body -->' +
                            '<div class="panel-footer">' +
                                '<div class="row">' +
                                    '<div class="col-xs-4"></div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<!-- /.panel -->' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>' +
                '<!-- /.row -->'
            );
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "langaugeforstudent",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("Languages not present for selection");
                    } else {
                        $("#selectlanguage").empty();
                        for(var i=0; i<data.length; i++){
                            if(i == 0){
                                $("#selectlanguage").append(
                                    '<div class="list-group-item" style="background-color: #FCFCFC"><b><u>Select Language</u></b></div>'
                                );
                            }
                            $("#selectlanguage").append(
                                '<div class="list-group-item">&nbsp;&nbsp;'+data[i].shortDesc+'' +
                                    '<label class="pull-right text-muted small module-name">' +
                                        '<input type="radio" class="checklng" name="select1" value='+data[i].langId+'>' +
                                    '</label>' +
                                '</div>'
                            );
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
            
           
        },
        
        languageSelectedForTest: function(e) {
            lanId = e.currentTarget.value;
            
            $("#selecttopic").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "topicforstudent?langCode="+lanId,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("Languages not present for selection");
                    } else {
                       
                        for(var i=0; i<data.length; i++){
                            if(i == 0){
                                $("#selecttopic").append(
                                    '<div class="list-group-item" style="background-color: #FCFCFC"><b><u>Select Topic</u></b></div>'
                                );
                            }
                            $("#selecttopic").append(
                                '<div class="list-group-item">&nbsp;&nbsp;'+data[i].shortDesc+'' +
                                    '<label class="pull-right text-muted small module-name">' +
                                        '<input type="radio" class="checktpc" name="select2" value='+data[i].topicId+'>' +
                                    '</label>' +
                                '</div>'
                            );
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        topicSelectedForTest: function(e) {
             if($(".checktpc").is(':checked')){
                var radios = $(".checktpc");
                var lncode;
                for (var i = 0, length = radios.length; i < length; i++) {
                    if (radios[i].checked) { 
                        topId = radios[i].value;
                    }
                }
             }
        },
        
        topicTestStart: function(){
            if($(".checklng").is(':checked') && $(".checktpc").is(':checked')){
                var json;
                json={"langId":lanId,"topicId":topId};
                $("#pageloading").show();
                Backbone.ajax({
                    url: "beforeteststart",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (data, textStatus, jqXHR) {
                        
                        if(data == 0) {
                            sweetAlert("Loging from first problem in storing data");
                        } else if(data == 1) {
                            participantroutes.navigate('prepare/tpsingle', {trigger: true});
                        } else if(data == 2) {
                            participantroutes.navigate('prepare/tpmultiple', {trigger: true});
                        } else {
                            sweetAlert("Your selection not been submitted");
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                    complete: function() {
                        $("#pageloading").hide();
                    }
                });
            } else {
                if(!($(".checklng").is(':checked') || $(".checktpc").is(':checked'))){
                    sweetAlert("Please select language and topic before taking test")
                } else if(!($(".checklng").is(':checked'))){
                    sweetAlert("Please select language also before taking test")
                } else if(!($(".checktpc").is(':checked'))){
                    sweetAlert("Please select topic also before taking test")
                }
            }
        },
    });
})();